// Includes
#include "CaloUtils/CaloMomentum.h"
#include "CaloProtoElectronMonitor.h"

namespace {
  /// hack to allow for tools with non-const interfaces...
  template <typename IFace>
  IFace* fixup(const ToolHandle<IFace>& iface) { return &const_cast<IFace&>(*iface); }
}
//-----------------------------------------------------------------------------
// Implementation file for class : CaloProtoElectronMonitor
//
// 2009-12-11 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloProtoElectronMonitor )


//==============================================================================
// Standard constructor, initializes variables
//==============================================================================

CaloProtoElectronMonitor::CaloProtoElectronMonitor( const std::string& name,
                                                    ISvcLocator* pSvcLocator)
: Consumer( name , pSvcLocator,
    KeyValue{ "Input", LHCb::ProtoParticleLocation::Charged }
)
{
  declareProperty("ExtrapolatorType"  , m_extrapolator ) ;

  // Protected vars in parent
  m_massFilterMax = 100;
  m_multMax = 100;
  m_massMin = 0;
  m_massMax = 5000;
  m_massBin = 500;
}

//==============================================================================
// Initialization
//==============================================================================

StatusCode CaloProtoElectronMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Initialize" << endmsg;

  if( !m_tracks.empty() )info() << "Will only look at track type(s) = " << m_tracks.value() << endmsg;
  else
    info() << "Will look at any track type" << endmsg;

  // Get, retrieve, configure tools
  if(!( m_caloElectron.retrieve() && m_extrapolator.retrieve() )){
    error() << "Unable to retrive one of the ToolHandles" << endmsg;
    return StatusCode::FAILURE;
  }

  // histograms
  hBook1(  "1", "# of CaloElectron protoP   " + inputLocation(),  m_multMin   ,    m_multMax   , m_multBin  );
  hBook1(  "2", "CaloElectron protoP Energy   " + inputLocation(),  m_energyMin ,    m_energyMax , m_energyBin );
  hBook1(  "3", "CaloElectron protoP Pt       " + inputLocation(),  m_etMin     ,    m_etMax     , m_etBin);
  hBook2(  "4", "CaloElectron protoP barycenter position x vs y   "
           + inputLocation(),  m_xMin, m_xMax, m_xBin, m_yMin, m_yMax, m_yBin);
  hBook2(  "5", "Energy-weighted CaloElectron protoP  barycenter position x vs y "
           + inputLocation(),m_xMin,m_xMax, m_xBin, m_yMin, m_yMax, m_yBin);
  hBook1(  "6", "e/p  " + inputLocation(),  m_eOpMin ,    m_eOpMax , m_eOpBin );
  hBook1(  "7", "Eprs  " + inputLocation(),  0. , 300. , m_energyBin );
  if( m_pairing ){
    hBook1(  "8", "m(track pair)  " + inputLocation()  ,  m_massMin   , m_massMax  ,  m_massBin );
    hBook1(  "9", "m(clust pair)  " + inputLocation()  ,  m_massMin   , m_massMax  ,  m_massBin );
    hBook1(  "10", "e/p for M(ee) < " + Gaudi::Utils::toString(m_massFilterMax)
             + inputLocation(),  m_eOpMin ,    m_eOpMax , m_eOpBin );
  }
  return StatusCode::SUCCESS;
}

//==============================================================================
// Main execution
//==============================================================================

bool CaloProtoElectronMonitor::valid_track(const LHCb::ProtoParticle* proto) const {
  // Return false if the proto's track fails to meet requested track type
  if(m_tracks.empty()) return true;
  const auto ptype = (int) proto->track()->type();
  auto valid = [ptype](const auto trtype){return ptype==trtype;};
  return std::any_of( std::begin(m_tracks), std::end(m_tracks), valid );
}

void CaloProtoElectronMonitor::operator()(const Input& protos) const {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Execute" << endmsg;
  if ( !produceHistos() ) return;

  initCounters();

  // Loop over protoparticles
  for( auto p = protos.begin(); protos.end() != p ; ++p ){
    const auto proto = *p;

    // Abort if track type mismatched
    if( !valid_track(proto) ) continue;

    // Abort if electron casting fail
    if( !fixup(m_caloElectron)->set(proto)) continue;

    // Abort if null hypo
    const auto hypo = fixup(m_caloElectron)->electron();
    if ( hypo == nullptr ) continue;

    // Abort if energy too low
    LHCb::CaloMomentum momentum( hypo );
    const double e    = momentum.e();
    const double et   = momentum.pt();
    if( e < m_eFilter ) continue;
    if( et < m_etFilter ) continue;
    const double ePrs = proto->info(LHCb::ProtoParticle::additionalInfo::CaloPrsE, 0.);
    if( m_prsCut > 0 && ePrs < m_prsCut ) continue;

    // Retrieve the seed id
    auto id = LHCb::CaloCellID();
    if ( hypo->clusters().size() > 0 ){
      SmartRef<LHCb::CaloCluster> cluster = *(hypo->clusters().begin());
      id = cluster->seed();
    }
    double eOp = fixup(m_caloElectron)->eOverP();

    // Fill histograms
    count(id);
    hFill1(id, "2", e  );
    hFill1(id, "3", et );
    const auto position = hypo->position();
    if( position != nullptr ){
      hFill2(id, "4", position->x(),position->y() );
      hFill2(id, "5", position->x(),position->y() , e);
    }
    hFill1(id, "6", eOp );
    hFill1(id, "7", ePrs );

    // perform electron pairing
    if( !m_pairing ) continue;

    // For electron paring, need an extrapolator tool before continuing
    if( m_extrapolator == 0 ){
      Warning("No extrapolator defined").ignore();
      continue;
    }

    // Loop over second electron
    for( auto pp = p+1 ; protos.end() != pp ; ++pp ){
      const auto proto2 = *pp;

      // Abort if track type mismatched
      if( !valid_track(proto2) ) continue;

      // Abort if fail hypo, or duplicate
      if( !fixup(m_caloElectron)->set(proto2) ) continue;;
      const auto hypo2 = fixup(m_caloElectron)->electron();
      if ( hypo2 == nullptr ) continue;
      if( hypo == hypo2 ) continue;

      // filtering proto2
      LHCb::CaloMomentum momentum2( hypo2 );
      const double e2 = momentum2.e();
      const double et2 = momentum2.pt();
      if(e2 < m_eFilter) continue;
      if(et2 < m_etFilter) continue;
      const double ePrs2 = proto2->info(LHCb::ProtoParticle::additionalInfo::CaloPrsE, 0.);
      if( m_prsCut > 0 &&  ePrs2 < m_prsCut )continue;

      // Combine hypo
      LHCb::CaloMomentum momentumSum( hypo );
      momentumSum.addCaloPosition( hypo2 );
      const double caloM = momentumSum.mass();

      // Validate tracks
      const auto t1 = proto->track();
      const auto t2 = proto2->track();
      if( t1 == nullptr || t2 == nullptr ) continue;
      if( t1->charge()*t2->charge() != -1 ) continue;
      auto st1 = t1->firstState();
      auto st2 = t2->firstState();


      // bool accept2 = m_tracks.empty() ? true : false;
      // for(std::vector<int>::iterator itr = m_tracks.begin();m_tracks.end() != itr;++itr){
      //   if( (int)t2->type() == *itr)accept2=true;
      // }
      // if( !accept2 )continue;

      // Propagation
      StatusCode sc = m_extrapolator->propagate(st1, 0.);
      if(sc.isFailure()) Warning("Propagation 1 failed").ignore();
      sc = m_extrapolator->propagate(st2, 0.);
      if(sc.isFailure()) Warning("Propagation 2 failed").ignore();

      // Finally, fill the histos
      const auto p1 = st1.momentum();
      const auto p2 = st2.momentum();
      double m2 = p1.R()*p2.R();
      m2 -= p1.X()*p2.X();
      m2 -= p1.Y()*p2.Y();
      m2 -= p1.Z()*p2.Z();
      m2 *= 2;
      const double m = (m2>0) ? sqrt(m2) : 0;
      hFill1(id, "8", m );
      hFill1(id, "9", caloM );
      if( m2 < m_massFilterMax ){
        hFill1(id, "10", eOp );
        const double eOp2 = fixup(m_caloElectron)->eOverP();
        hFill1(id, "10", eOp2 );
      }
    }
  }
  fillCounters("1");
  return;
}
//=============================================================================
