#ifndef CALOPIDS_CALOCHI22ID_H
#define CALOPIDS_CALOCHI22ID_H 1

// Include files
#include "Relations/Relation1D.h"
#include "Relations/IRelationWeighted.h"
#include "Relations/IRelationWeighted2D.h"
#include "Event/Track.h"
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/CaloAlgUtils.h"

// ============================================================================
/** @class CaloChi22ID CaloChi22ID.h
 *
 *
 *  @author Ivan BELYAEV
 *  @date   2006-06-18
 */
// ============================================================================

class CaloChi22ID : public CaloTrackAlg {
 public:
  /// algorithm execution
  StatusCode execute() override;

  CaloChi22ID(const std::string& name, ISvcLocator* pSvc);

 protected:
  /// perform the actual job
  template <class TABLEI, class TABLEO>
  inline StatusCode doTheJob(const TABLEI* input, TABLEO* output) const;

 private:
  /// perform the actual job
  template <class TABLEI, class TABLEO>
  inline StatusCode doTheJob(const LHCb::Track::Container* tracks,
                             const TABLEI* input, TABLEO* output) const;

 protected:
  typedef std::vector<std::string> Inputs;

  Gaudi::Property<Inputs>      m_tracks { this, "Tracks", { }  , "input tracks"};
  Gaudi::Property<std::string> m_input  { this, "Input" , ""   , "input relation table"};
  Gaudi::Property<std::string> m_output { this, "Output", ""   , "output relation table"};
  Gaudi::Property<float>       m_large  { this, "CutOff", 10000, "large value"};
};

// ============================================================================
/// perfromthe actual job for container of tracks
// ============================================================================

template <class TABLEI, class TABLEO>
inline StatusCode CaloChi22ID::doTheJob(const LHCb::Track::Container* tracks,
                                        const TABLEI* input,
                                        TABLEO* output) const {
  Assert(nullptr != tracks, "Invalid container of input tracks");

  for (const auto track : *tracks) {
    if (!use(track)) {
      continue;
    }
    auto links = input->relations(track);
    // fill the relation table
    const float chi2  = links.empty() ? m_large.value() : links.front().weight() ;
    output -> i_push ( track , chi2 ) ;                      // NB!: i_push
  }
  return StatusCode::SUCCESS;
}

// ============================================================================
/// perfrom the actual job for all containers
// ============================================================================

template <class TABLEI, class TABLEO>
inline StatusCode CaloChi22ID::doTheJob(const TABLEI* input,
                                        TABLEO* output) const {
  Assert(!m_tracks.empty(), "No input tracks are specified!");
  // loop over the container
  for (const auto& i : m_tracks) {
    const auto tracks = get<LHCb::Track::Container>(i);
    if (!tracks) continue;
    StatusCode sc = doTheJob(tracks, input, output);
    if (sc.isFailure()) return sc;
  }
  /// MANDATORY: i_sort after i_push
  output->i_sort();  // NB: i_push
  //
  return StatusCode::SUCCESS;
}

// ============================================================================
#endif  // CALOCHI22ID_H
// ============================================================================
