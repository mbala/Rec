#ifndef CALOPIDS_CALOTRACK2IDALG_H
#define CALOPIDS_CALOTRACK2IDALG_H 1

// Include files
#include "Event/Track.h"
#include "CaloInterfaces/ICaloTrackIdEval.h"
#include "CaloUtils/Calo2Track.h"
#include "CaloTrackAlg.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "ToVector.h"

// ============================================================================
/** @class CaloTrack2IDAlg CaloTrack2IDAlg.h
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-06-15
 */

class CaloTrack2IDAlg : public CaloTrackAlg {
 public:
  StatusCode initialize() override;
  StatusCode execute() override;

  CaloTrack2IDAlg(const std::string& name, ISvcLocator* pSvc);

 protected:
  typedef std::vector<std::string> Inputs;


  Gaudi::Property<Inputs> m_inputs
    {this, "Inputs", {}, "input tracks"};

  Gaudi::Property<std::string> m_output
    {this, "Output", "", "output data"};

  Gaudi::Property<std::string> m_filter
    {this, "Filter", "", "filter"};

 private:
  // tool to be used for evaluation
  ToolHandle<ICaloTrackIdEval> m_tool{"<NOT DEFINED>", this};
};

// ============================================================================
#endif  // CALOTRACK2IDALG_H
// ============================================================================
