// Include files
#include "CaloChi22ID.h"
#include "ToVector.h"

// ============================================================================
/** @class EcalChi22ID EcalChi22ID.cpp
 *  The preconfigured instance of class CaloChi22ID
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================

class EcalChi22ID final : public CaloChi22ID {
 public:
  EcalChi22ID(const std::string& name, ISvcLocator* pSvc)
      : CaloChi22ID(name, pSvc) {
    using LHCb::CaloAlgUtils::CaloIdLocation;
    _setProperty("Input", CaloIdLocation("ElectronMatch", context()));
    _setProperty("Output", CaloIdLocation("EcalChi2", context()));
    // @todo it must be in agrement with "Threshold" for ElectonMatchAlg
    _setProperty("CutOff", "10000");
    // track types:
    _setProperty("AcceptedType", Gaudi::Utils::toString<int>(
                                     LHCb::Track::Types::Long, LHCb::Track::Types::Ttrack,
                                     LHCb::Track::Types::Downstream));
  };
};

// ============================================================================
DECLARE_COMPONENT( EcalChi22ID )

// ============================================================================
