// ============================================================================
#include "CaloSelectClusterWithSpd.h"
// ============================================================================
/** @class CaloSelectChargedClusterWithSpd
 *  Simple seleclton of newural clusters based on Spd information
 *  @author Olivier Deschamps
 *  @author Vanya BELYAEV
 */
// ============================================================================

class CaloSelectChargedClusterWithSpd : public CaloSelectClusterWithSpd
{
public:
  // ==========================================================================
  bool select ( const LHCb::CaloCluster* cluster ) const override
  { return (*this) ( cluster ) ; }
  // ==========================================================================
  bool operator()( const LHCb::CaloCluster* cluster ) const override
  {
    if ( 0 == cluster )
    {
      Warning ( "CaloCluster* points to NULL, return false" );
      return false ;                                                  // RETURN
    }
    //
    bool sel = cut() < n_hit ( *cluster ) ;
    if(counterStat->isVerbose())counter("selected clusters") += (int) sel;
    return sel;
  }
  // ==========================================================================
  /// constructor
  using CaloSelectClusterWithSpd::CaloSelectClusterWithSpd;
  // ==========================================================================
};
// ============================================================================
DECLARE_COMPONENT( CaloSelectChargedClusterWithSpd )
// ============================================================================
