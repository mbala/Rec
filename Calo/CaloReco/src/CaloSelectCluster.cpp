#include "GaudiKernel/SystemOfUnits.h"
#include "CaloUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "CaloSelectCluster.h"

DECLARE_COMPONENT( CaloSelectCluster )

// ============================================================================
CaloSelectCluster::CaloSelectCluster
( const std::string& type, 
  const std::string& name,
  const IInterface*  parent)
  : GaudiTool (type ,name ,parent) 
{
  declareInterface<ICaloClusterSelector> (this);
}
// ============================================================================

StatusCode CaloSelectCluster::initialize (){  
  StatusCode sc = GaudiTool::initialize () ;
  return sc;
}

bool CaloSelectCluster::select( const LHCb::CaloCluster* cluster ) const
{ 
  return (*this) (cluster); 
}

bool CaloSelectCluster::operator()( const LHCb::CaloCluster* cluster) const 
{
  if ( 0 == cluster ) { Warning ( "CaloCluster* points to NULL!" ).ignore() ; return false ; }

  double e = cluster->e();
  LHCb::CaloMomentum moment = LHCb::CaloMomentum(cluster);
  double et = moment.momentum().Pt();
  int m = cluster->entries().size();

  if( UNLIKELY(msgLevel( MSG::DEBUG) ))debug() << "Cluster has " << m << " entries " 
                                      << " for a total energy of " << e <<  "(Et = " << et << ")" << endmsg;

  bool isSelected =  (e>m_cut) && (m < m_mult) && (et > m_etCut) && (m > m_multMin);
  if (isSelected ) { 
    ++m_counter;
  }
  return isSelected;
}
