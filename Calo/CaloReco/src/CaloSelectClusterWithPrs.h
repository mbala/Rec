// ============================================================================
#ifndef CALORECO_CALOSELECTCLUSTERWITHPRS_H
#define CALORECO_CALOSELECTCLUSTERWITHPRS_H 1
// ============================================================================
#include <string>
#include "GaudiAlg/GaudiTool.h"
#include "CaloInterfaces/ICaloClusterSelector.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICounterLevel.h"
// ============================================================================

class CaloSelectClusterWithPrs :
  public virtual ICaloClusterSelector ,
  public          GaudiTool
{
public:

  bool select( const LHCb::CaloCluster* cluster ) const override;
  bool operator()( const LHCb::CaloCluster* cluster ) const override;
  StatusCode initialize() override;

  CaloSelectClusterWithPrs( const std::string& type   ,
                            const std::string& name   ,
                            const IInterface*  parent );

private:
  Gaudi::Property<float> m_cut  {this, "MinEnergy", -10. *Gaudi::Units::MeV};
  Gaudi::Property<float> m_mult {this, "MinMultiplicity", 0.};
  ICaloHypo2Calo* m_toPrs = nullptr;
  Gaudi::Property<std::string> m_det {this, "Detector", "Ecal"};
  ICounterLevel* counterStat = nullptr;
};
#endif // CALORECO_CALOSELECTCLUSTERWITHPRS_H
