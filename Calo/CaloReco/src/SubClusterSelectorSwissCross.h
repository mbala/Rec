#ifndef CALORECO_SUBCLUSTERSELECTORSwissCross_H
#define CALORECO_SUBCLUSTERSELECTORSwissCross_H 1
// Include files
// CaloTools
#include "SubClusterSelectorBase.h"
// CaloUtils
#include "CaloUtils/CellSwissCross.h"


class SubClusterSelectorSwissCross  : public SubClusterSelectorBase{
public:
  StatusCode initialize() override;

  StatusCode tag        ( LHCb::CaloCluster* cluster)const override;
  StatusCode untag      ( LHCb::CaloCluster* cluster ) const override;

  SubClusterSelectorSwissCross( const std::string& type   ,
                         const std::string& name   ,
                         const IInterface*  parent );

private:

  CellSwissCross        m_matrix;

};

#endif
