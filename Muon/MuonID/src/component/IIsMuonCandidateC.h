#ifndef IISMUONCANDIDATEC_H 
#define IISMUONCANDIDATEC_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"


/** @class IIsMuonCandidateC IIsMuonCandidateC.h
 *  
 *
 *  @author Jose Angel Hernando Morata
 *  @author Xabier Cid Vidal
 *  @date   2009-03-11
 */
struct IIsMuonCandidateC : extend_interfaces<IAlgTool>{

  // Return the interface ID
  DeclareInterfaceID( IIsMuonCandidateC, 2, 0 );
  
  virtual bool IsMuonCandidate(const LHCb::Track& muTrack)=0;
  virtual bool IsMuonCandidate(const std::vector<LHCb::LHCbID>& LHCbIDs,const double& mom)=0;

};
#endif // IISMUONCANDIDATEC_H
