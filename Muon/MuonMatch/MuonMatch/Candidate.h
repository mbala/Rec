// $Id: $
#ifndef CANDIDATE_H
#define CANDIDATE_H 1

// STL
#include <vector>
#include <algorithm>
#include <optional>


// LHCb
#include "Event/Track.h"
#include "Event/State.h"

// from MuonID
#include "MuonID/CommonMuonHit.h"

struct Candidate;
typedef std::vector<Candidate> Candidates;

/** @class CandidateFitAtt Candidate.h
 *  MuonMatch candidate fit attributes.
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

struct CandidateFitAtt final
{

  /// Slope
  double slope;

  /// Momentum
  double p;

  /// Chi2 per degrees of freedom
  double chi2ndof;
};

/** @class CandidateFitResult Candidate.h
 *  MuonMatch candidate fit result.
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

struct CandidateFitResult final
{

  /// a0
  double a0;

  /// Slope
  double slope;

  /// Chi2
  double chi2;

  /// nDoF
  size_t ndof;
};

/** @class Candidate Candidate.h
 *  MuonMatch candidate.
 *
 *  @author Roel Aaij
 *  @date   2010-12-02
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

struct Candidate final
{

  /// Standard constructor
  Candidate( const LHCb::Track* tr, const CommonMuonHits &muonhits,
	     LHCb::State::Location stateLocation )
    : track(tr)
    {

      for ( const auto& el : muonhits )
	hits.emplace_back(el);

      state = track->stateAt(stateLocation);
      if ( !state )
	state = &(track->closestState(5000));
    }

  /// Constructor given a track
  Candidate( const LHCb::Track* tr )
    : Candidate( tr, CommonMuonHits(), LHCb::State::Location::EndVelo )
    {
      hits.reserve( 5 );
    }

  /// Constructor given a track and the state location
  Candidate( const LHCb::Track* tr, LHCb::State::Location stateLocation )
    : Candidate( tr, CommonMuonHits(), stateLocation )
    {
      hits.reserve( 5 );
    }

  /// Calculate the projection in the magnet for the "x" axis
  void xStraight( const double& z, double& x, double& errX ) const
  {
    double dz = z - state->z();
    x    = state->x() + dz*state->tx();
    errX = dz*sqrt(state->errTx2());
  }

  /// Calculate the projection in the magnet for the "y" axis
  void yStraight( const double& z, double& y, double& errY ) const
  {
    double dz = z - state->z();
    y    = state->y() + dz*state->ty();
    errY = dz*sqrt(state->errTy2());
  }

  /// Return the squared tangent in "x"
  double tx2() const
  {
    return state->tx()*state->tx();
  }

  /// Return the squared tangent in "y"
  double ty2() const
  {
    return state->ty()*state->ty();
  }

  /// Return the sine of the angle of the track with respect to "z"
  double sinTrack() const
  {
    return sqrt(1. - 1./(1. + tx2() + ty2()));
  }

  /// Store the track
  const LHCb::Track* track;

  /// Store the state
  const LHCb::State* state;

  /// Store the fit result attributes
  std::optional<CandidateFitAtt> fitatt;

  /// List of muon hits
  CommonMuonHits hits;
};

#endif // CANDIDATE_H
