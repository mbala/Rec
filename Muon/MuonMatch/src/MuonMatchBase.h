#ifndef MUONMATCH_BASE_H
#define MUONMATCH_BASE_H 1

// STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiAlg/Transformer.h"

// from LHCb
#include "Kernel/ILHCbMagnetSvc.h"

// from MuonDet
#include "MuonDet/DeMuonDetector.h"

// from MuonID
#include "MuonID/ICommonMuonHitManager.h"
#include "MuonID/CommonMuonStation.h"

// from MuonMatch
#include "MuonMatch/Candidate.h"

extern const std::array<unsigned int,4> order;

/** @class MuonMatchBase MuonMatchBase.h
 *  Base class for the MuonMatch tools
 *
 *  @author Miguel Ramos Pernas
 *  @date   2017-10-05
 */

class MuonMatchBase :
  public Gaudi::Functional::Transformer<LHCb::Tracks(const LHCb::Tracks&),
  Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

 public:
  
  MuonMatchBase( const std::string& name, ISvcLocator* pSvcLocator );

  /// Add hits to the given candidate
  virtual void addHits( Candidate& seed ) const = 0;
  
  /// Create candidates from the seed track and seed station. The actual vector of candidates must be taken using the "extractSeeds" member function.
  virtual Candidates findSeeds( const LHCb::Track& seed, const unsigned int seedStation ) const = 0;

  /// Fit the given candidate
  virtual void fitCandidate( Candidate& seed ) const = 0;

  /// Initialize the instance
  virtual StatusCode initialize() override;

  /// Match the given tracks to the seed
  virtual void match( const LHCb::Track& seed, LHCb::Track::Vector& tracks ) const = 0;

  /// Wrap the "findSeeds" member function so it adds the hits, and produces the histograms
  Candidates extractSeeds( const LHCb::Track& seed ) const;
  
  /// Gaudi-functional method
  LHCb::Tracks operator() ( const LHCb::Tracks& seeds ) const override;

  /// Get the momentum given the slope in "x"
  inline double momentum( const double dtx ) const
  {
    return m_kickScale/std::fabs(dtx) + m_kickOffset;
  }

  /// Calculate the slope using the minimum momentum
  inline double dtx() const
  {
    return m_kickScale/(m_minMomentum - m_kickOffset);
  }

  /** Calculate the maximum tangent value.
   *  Use sum rule for tan and approximate "tan(s)" with "s" to calculate
   *  window in x.
   */
  double tanMax( const Candidate &c ) const
  {
    double s = this->dtx();
    double t = c.state->tx();

    return (t + s)/(1 - t*s);
  }

  /** Calculate the minimum tangent value.
   *  Use sum rule for tan and approximate "tan(s)" with "s" to calculate
   *  window in x.
   */
  double tanMin( const Candidate& c ) const
  {
    double s = this->dtx();
    double t = c.state->tx();
    
    return (t - s)/(1 + t*s);
  }

 protected:

  /// Services
  ILHCbMagnetSvc* m_fieldSvc = nullptr;

  /// Tools
  ICommonMuonHitManager* m_hitManager = nullptr;
  
  Gaudi::Property<double> m_kickOffset {this, "KickOffset", 175*Gaudi::Units::MeV};
  Gaudi::Property<double> m_kickScale  {this, "KickScale",  1255*Gaudi::Units::MeV};
  Gaudi::Property<double> m_minMomentum {this, "MinMomentum", 6*Gaudi::Units::GeV};

 private:
  
  /// Timing tool
  ISequencerTimerTool* m_timerTool = nullptr;

  /// Counter for timing tool
  int m_toolTime = 0;
  
  /// Measure timing of algorithm
  Gaudi::Property<bool> m_doTiming {this, "DoTiming", false};

};

#endif // MUONMATCH_TRANSFORMER_H
