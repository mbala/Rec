################################################################################
# Package: MuonTrackAlign
################################################################################
gaudi_subdir(MuonTrackAlign v1r10p2)

gaudi_depends_on_subdirs(Det/MuonDet
                         Event/LinkerEvent
                         Event/MCEvent
                         Event/TrackEvent
                         Tr/TrackInterfaces
                         GaudiAlg
                         Muon/MuonDAQ)

find_package(AIDA)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(MuonTrackAlign
                 src/*.cpp
                 INCLUDE_DIRS AIDA Muon/MuonDAQ Tr/TrackInterfaces
                 LINK_LIBRARIES MuonDetLib LinkerEvent MCEvent TrackEvent GaudiAlgLib)

