#ifndef COMPONENT_MUONCOMBREC_H
#define COMPONENT_MUONCOMBREC_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MuonInterfaces/IMuonTrackRec.h"            // Interface
#include "MuonInterfaces/MuonTrack.h"
#include "MuonInterfaces/MuonNeuron.h"

// forward declarations
class DeMuonDetector;
class IMuonHitDecode;
struct IMuonPadRec;
struct IMuonClusterRec;
class ISequencerTimerTool;
struct IMuonTrackMomRec;
/** @class MuonCombRec MuonCombRec.h component/MuonCombRec.h
 *
 *
 *  @author Giovanni Passaleva
 *  @date   2009-10-07
 */
class MuonCombRec final : public extends<GaudiTool,IMuonTrackRec, IIncidentListener> {
public:
  /// Standard constructor
  MuonCombRec( const std::string& type,
                const std::string& name,
                const IInterface* parent);

  // from GaudiTool
  StatusCode initialize() override;
  // from IIncidentListener
  void handle ( const Incident& incident ) override;

  // implementation of interface methods
  const std::vector<MuonHit>* trackhits() const override {
    if(!m_recDone) const_cast<MuonCombRec*>(this)->muonTrackFind();//@FIXME
    return m_trackhits;
  }

  const std::vector<MuonTrack>& tracks() const override {
    if(!m_recDone) const_cast<MuonCombRec*>(this)->muonTrackFind(); //@FIXME
    return m_tracks;
  }
  inline const std::vector<MuonNeuron*>* useneurons() override {
    return nullptr;
  }
  inline const std::vector<MuonNeuron*>* allneurons() override {
    return nullptr;
  }
  inline bool recOK() override {return (m_recDone && m_recOK);}
  inline bool tooManyHits() override {return false;}
  inline bool clusteringOn() override {return (m_clusterToolName != "MuonFakeClustering");}
  inline void setZref(double Zref) override { MuonTrackRec::Zref=Zref; }
  void setPhysicsTiming(bool PhysTiming) override { MuonTrackRec::PhysTiming = PhysTiming;}
  void setAssumeCosmics(bool AssumeCosmics) override {
    MuonTrackRec::IsCosmic = AssumeCosmics;
    if (AssumeCosmics) MuonTrackRec::IsPhysics = false;
  }
  void setAssumePhysics(bool AssumePhysics) override {
    MuonTrackRec::IsPhysics = AssumePhysics;
    if(AssumePhysics) MuonTrackRec::IsCosmic = false;
  }
  void setSeedStation(int seedS) override
  { m_seedStation = seedS;}
  void setSkipStation(int skipS) override
  {
    if( skipS >= 0) {
      m_recDone = false; // reset the recDone flag to allow a new track reconstruction
      m_cloneKiller = false; // do not kill clones
      m_strongCloneKiller = false;  // do not kill clones
      m_skipStation = skipS; // set the station to be skipped
      if(m_skipStation == 4) { // special treatment for M5
        setSeedStation(3);
      }
    } else {
      m_skipStation = skipS; // set the station to be skipped
      m_recDone = false; // reset the recDone flag to allow a new track reconstruction
      m_cloneKiller = m_optCloneKiller;
      m_strongCloneKiller = m_optStrongCloneKiller;
      if( m_strongCloneKiller ) m_cloneKiller = false; // don't run both

      // In case the seed station is M5 the clone killing does not work
      // FIXME: Make the clone killing more flexible
      if( m_seedStation != M5 ) {
        m_cloneKiller = false;
        m_strongCloneKiller = false;
      }
    }
  }
  StatusCode copyToLHCbTracks() override;

private:

  // station mnemonics
  enum{M1=0, // M1
       M2,   // M2
       M3,   // M3
       M4,   // M4
       M5    // M5
         };
  // region mnemonics
  enum{R1=0, // R1
       R2,   // R2
       R3,   // R3
       R4    // R4
         };

  IMuonHitDecode* m_decTool = nullptr;
  IMuonPadRec* m_padTool = nullptr;
  IMuonClusterRec* m_clusterTool = nullptr;
  IMuonTrackMomRec* m_momentumTool = nullptr;
  DeMuonDetector* m_muonDetector = nullptr;

  bool m_recDone;
  bool m_recOK;
  bool m_sortDone = false; // MuonHit-s sorted

  Gaudi::Property<std::vector<float>> m_xFOIs {this, "xFOIs", {
  // R1     R2     R3     R4
     100.0F,200.0F,300.0F,400.0F, // M1
     100.0F,200.0F,300.0F,400.0F, // M2
     100.0F,200.0F,300.0F,400.0F, // M3
     400.0F,400.0F,400.0F,400.0F, // M4
  }};
  Gaudi::Property<std::vector<float>> m_yFOIs {this, "yFOIs", {
  // R1     R2    R3     R4
     30.0F, 60.0F,100.0F,150.0F, // M1
     60.0F,120.0F,180.0F,240.0F, // M2
     60.0F,120.0F,240.0F,480.0F, // M3
     60.0F,120.0F,240.0F,480.0F, // M4
  }};

  double m_zStations[5];

  const std::vector< MuonHit >* m_trackhits = nullptr;
  std::vector< MuonTrack > m_tracks;
  std::array< std::vector<const MuonHit*>, 4*5 > m_sortedHits;

  void clear();

  int m_stationOffset; // offset into lookup tables (0 in Run1/2, 1 in upgrade)
  int m_nStation; // number of stations (5 in Run1/2, 4 in upgrade)
  int m_nRegion;

  // -- timing
  ISequencerTimerTool* m_timer = nullptr;
  int m_timeTotal;
  int m_timeLoadHits;
  int m_timeClusters;
  int m_timeMuon;
  int m_timeBuildLogicalPads;
  int m_timeCloneKilling;
  int m_timeFitting;

  // enable timers
  Gaudi::Property<bool> m_measureTime {this, "MeasureTime", false};

  // enable clone finding and killing
  bool m_cloneKiller;
  Gaudi::Property<bool> m_optCloneKiller
    {this, "CloneKiller", true,
    "enable clone finding and killing"};

  // enable strong clone finding and killing
  bool m_strongCloneKiller;
  Gaudi::Property<bool> m_optStrongCloneKiller
    {this, "StrongCloneKiller", true,
    "enable strong clone finding and killing"};

  // station to be skipped (e.g. for eff. study): 0-4;
  // -1 = keep all stns (default)
  int m_skipStation;
  Gaudi::Property<int> m_optSkipStation
    {this, "SkipStation", -1,
    "station to be skipped (e.g. for eff. study): 0-4; -1 = keep all stns (default)"};

  Gaudi::Property<bool> m_physicsTiming
    {this, "PhysicsTiming", true,
    "if true, we assume that timing is the final one (accounting for TOF from primary vx)"};

  Gaudi::Property<bool> m_assumeCosmics
    {this, "AssumeCosmics", false,
    "if true (default) we assume that tracks are of cosmic origin (can be backward)"};

  Gaudi::Property<bool> m_assumePhysics
    {this, "AssumePhysics", true,
    "if true we assume that tracks have the 'right' direction (pz>0)"};

  Gaudi::Property<std::string> m_decToolName
    {this, "DecodingTool", "MuonHitDecode"
    "name of decoding tool (MuonHitDecode for offline, MuonMonHitDecode for online monitoring)"};

  Gaudi::Property<std::string> m_padToolName
    {this, "PadRecTool", "MuonPadRec",
    "name of pad rec tool (MuonPadRec only option so far)"};

  Gaudi::Property<std::string> m_clusterToolName
    {this, "ClusterTool", "MuonFakeClustering",
    "name of clustering tool"};

  /// cross talk
  Gaudi::Property<bool> m_XTalk {this, "AddXTalk", true};

  /// station used for seed
  int m_seedStation;
  Gaudi::Property<int> m_optSeedStation {this, "SeedStation", 4, "default seed: M5"};

  Gaudi::Property<std::string> m_trackOutputLoc
    {this, "TracksOutputLocation", "Rec/Muon/Track",
    "LHCb tracks output location in TES"};

  /// main steering reconstruction routine
  StatusCode muonTrackFind();

  /// find matching muon hits in other stations
  StatusCode findMatching( const double x, const double y,
                           const int lookInStation, const int lookInRegion,
                           std::vector<const MuonHit*> &candidates );

  StatusCode sortMuonHits();
  StatusCode muonSearch();
  StatusCode cloneKiller();
  StatusCode strongCloneKiller();
  StatusCode trackFit();

};
#endif // COMPONENT_MUONCOMBREC_H
