#ifndef PRGECFILTER_H 
#define PRGECFILTER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/FilterPredicate.h"
#include "Event/RawEvent.h"

/** @class PrGECFilter PrGECFilter.h
 *  \brief give decision concerning GEC
 */
class PrGECFilter : public Gaudi::Functional::FilterPredicate<bool(const LHCb::RawEvent&)> {

 public:
  /// Standard constructor
  PrGECFilter(const std::string& name, ISvcLocator* pSvcLocator);

  /// Algorithm execution
  bool operator()(const LHCb::RawEvent&) const override;

 private:

  Gaudi::Property<unsigned int> m_nFTUTClusters{this, "NumberFTUTClusters", 0 };
  Gaudi::Property<unsigned int> m_clusterMaxWidth{ this, "ClusterMaxWidth", 4, "Maximal cluster width"};

};
#endif // PRGECFILTER_H

