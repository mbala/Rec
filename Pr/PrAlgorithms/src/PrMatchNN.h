#ifndef PRMATCHNN_H
#define PRMATCHNN_H 1

// Include files
// from Gaudi
#include "GaudiAlg/Transformer.h"

#include "GaudiKernel/IRegistry.h"
#include "Event/Track.h"

#include "PrKernel/IPrDebugMatchTool.h"
#include "IPrAddUTHitsTool.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"

#include "weights/TMVAClassification_MLPMatching.class.C"
/** @class PrMatchNN PrMatchNN.h
 *  Match Velo and Seed tracks
 *
 *  @author Michel De Cian (migration to Upgrade)
 *  @date 2013-11-15
 *
 *  @author Olivier Callot
 *  @date   2007-02-07
 */

class PrMatchNN : public Gaudi::Functional::Transformer<LHCb::Tracks( const std::vector<LHCb::Track>&, const LHCb::Tracks& )>
{
public:
  /// Standard constructor
  PrMatchNN( const std::string& name, ISvcLocator* pSvcLocator );

  /// initialization
  StatusCode initialize() override;

  //  main method
  LHCb::Tracks operator()( const std::vector<LHCb::Track>&, const LHCb::Tracks& ) const override;

  /** @class MatchCandidate PrMatchNN.h
   *
   * Match candidate for PrMatcNNh algorithm
   *
   * @author Manuel Schiller
   * @date 2012-01-31
   * 	code cleanups
   *
   * @author Olivier Callot
   * @date   2007-02-07
   * 	initial implementation
   */
  class MatchCandidate
  {
  public:
    MatchCandidate( const LHCb::Track* vTr, const LHCb::Track* sTr, float dist )
        : m_vTr( vTr ), m_sTr( sTr ), m_dist( dist )
    {
    }

    const LHCb::Track* vTr() const { return m_vTr; }
    const LHCb::Track* sTr() const { return m_sTr; }
    float              dist() const { return m_dist; }

  private:
    const LHCb::Track* m_vTr;
    const LHCb::Track* m_sTr;
    float              m_dist;
  };

private:
  /// calculate matching chi^2
  float getChi2Match( const LHCb::State& vState, const LHCb::State& sState,
                      std::array<float, 6>&                         mLPReaderInput ) const;

  /// merge velo and seed segment to output track
  void makeTrack( const LHCb::Track& velo, const LHCb::Track& seed, LHCb::Track& output, float chi2 ) const;


  Gaudi::Property<std::string> m_addUTHitsToolName{this, "AddUTHitsToolName", "PrAddUTHitsTool"};
  Gaudi::Property<std::string> m_fastMomentumToolName{this, "FastMomentumToolName", "FastMomentumEstimate"};
  Gaudi::Property<std::string> m_matchDebugToolName{this, "MatchDebugToolName", ""};
  Gaudi::Property<std::vector<double>> m_zMagParams{
      this, "ZMagnetParams", {5287.6, -7.98878, 317.683, 0.0119379, -1418.42}};
  Gaudi::Property<std::vector<double>> m_momParams{
      this, "MomentumParams", {1.24386, 0.613179, -0.176015, 0.399949, 1.64664, -9.67158}};
  Gaudi::Property<std::vector<double>> m_bendYParams{this, "bendParamYParams", {-347.801, -42663.6}};
  // -- Parameters for matching in y-coordinate
  Gaudi::Property<float> m_zMatchY{this, "zMatchY", 10000. * Gaudi::Units::mm};
  // -- Tolerances
  Gaudi::Property<float> m_dxTol{this, "dxTol", 8. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dxTolSlope{this, "dxTolSlope", 80. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dyTol{this, "dyTol", 6. * Gaudi::Units::mm};
  Gaudi::Property<float> m_dyTolSlope{this, "dyTolSlope", 300. * Gaudi::Units::mm};
  Gaudi::Property<float> m_fastYTol{this, "FastYTol", 250.};
  // -- The main cut values
  Gaudi::Property<float> m_maxChi2{this, "MaxMatchChi2", 15.0};
  Gaudi::Property<float> m_minNN{this, "MinMatchNN", 0.25};
  Gaudi::Property<float> m_maxdDist{this, "MaxdDist", 0.10};
  Gaudi::Property<bool> m_addUT{this, "AddUTHits", true};

  IClassifierReader*      m_MLPReader        = nullptr;
  IPrAddUTHitsTool*       m_addUTHitsTool    = nullptr;
  IPrDebugMatchTool*      m_matchDebugTool   = nullptr;
  ITrackMomentumEstimate* m_fastMomentumTool = nullptr;

  typedef std::pair<const LHCb::Track*, const LHCb::State*> TrackStatePair;
  typedef std::vector<TrackStatePair> TrackStatePairs;
};

#endif // PRMATCH_H
