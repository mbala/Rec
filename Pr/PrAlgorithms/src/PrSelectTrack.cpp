#include "GaudiAlg/Transformer.h"
#include "Event/Track.h"
#include <vector>
#include "boost/algorithm/string/join.hpp"
#include "boost/format.hpp"
#include "LoKi/ITrackFunctorFactory.h"
#include "LoKi/BasicFunctors.h"
#include "LoKi/TrackTypes.h"

namespace Pr
{
  class SelectTracks final : public Gaudi::Functional::Transformer<LHCb::Track::Selection( const LHCb::Track::Range& )>
  {
  public:

     SelectTracks( const std::string& name, ISvcLocator* pSvcLocator );

     LHCb::Track::Selection operator()(const LHCb::Track::Range& in) const override {
        return { in.begin(), in.end(), std::cref(m_predicate) };
     }

     StatusCode initialize() override {
        auto sc = Transformer::initialize(); decode(); return sc;
     }

  private:
    void decode();

    LoKi::TrackTypes::TrCut m_predicate = LoKi::BasicFunctors<const LHCb::Track*>::BooleanConstant( false );
//        BinomialCounter counterPass;

    ToolHandle<LoKi::ITrackFunctorFactory>    m_factory  { this, "Factory","LoKi::Hybrid::TrackFunctorFactory/TrackFunctorFactory:PUBLIC" };
    Gaudi::Property<std::vector<std::string>> m_preambulo{ this, "Preambulo" };
    Gaudi::Property<std::string>              m_code     { this, "Code" };

    bool m_code_updated = false;
    bool m_preambulo_updated = false;

  };

  SelectTracks::SelectTracks( const std::string& name, ISvcLocator* pSvcLocator )
  : Transformer(name, pSvcLocator, KeyValue{"Input", ""}, KeyValue{"Output", ""})
  {
      m_code.declareUpdateHandler( [this](auto&) {
          this->m_code_updated = true;
          if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
          if ( !this->m_preambulo_updated ) return;
          this->decode();
      } );
      m_preambulo.declareUpdateHandler( [this](auto&) {
          m_preambulo_updated = true;
          if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
          if ( !this->m_code_updated ) return;
          this->decode();
      } );
  }

  void SelectTracks::decode() {
      if (msgLevel(MSG::DEBUG)) debug() << "decoding " << m_code.value() << endmsg;
      m_factory.retrieve();
      StatusCode sc = m_factory->get( m_code.value(), m_predicate, boost::algorithm::join( m_preambulo.value(), "\n" ) );
      if ( sc.isFailure() ) throw GaudiException{"Failure to decode", "SelectTracks", StatusCode::FAILURE };
      m_factory.release();
      m_code_updated = false;
      m_preambulo_updated = false;
      if (msgLevel(MSG::DEBUG)) debug() << "decoded " << m_code.value() << endmsg;
  }

  DECLARE_COMPONENT( SelectTracks )
}
