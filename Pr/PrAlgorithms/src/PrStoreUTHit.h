#ifndef PRSTOREUTHIT_H
#define PRSTOREUTHIT_H 1

#include "GaudiAlg/Transformer.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHitHandler.h"
#include "STDet/DeSTDetector.h"
#include "UTDAQ/UTReadoutTool.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Kernel/STClusterWord.h"
#include "Kernel/STDecoder.h"
#include "Event/STTELL1BoardErrorBank.h"

class PrStoreUTHit : public Gaudi::Functional::Transformer<UT::HitHandler(const LHCb::RawEvent&)> {
public:
  PrStoreUTHit( const std::string& name, ISvcLocator* pSvcLocator );
  virtual StatusCode initialize() override;

  UT::HitHandler operator()(const LHCb::RawEvent&) const override;

private:
  std::vector<unsigned int> missingInAction(LHCb::span<const LHCb::RawBank*> banks) const;
  unsigned int pcnVote(LHCb::span<const LHCb::RawBank*> banks) const;
  StatusCode decodeBanks(const LHCb::RawEvent& rawEvt,
                         UT::HitHandler& fCont) const;
  std::unique_ptr<LHCb::STTELL1BoardErrorBanks> decodeErrors(const LHCb::RawEvent& raw) const;
  bool canBeRecovered(const LHCb::STTELL1BoardErrorBank* bank,
                      const STClusterWord& word,
                      const unsigned int pcn) const;
  bool checkDataIntegrity(STDecoder& decoder,
                          const STTell1Board* aBoard,
                          const unsigned int bankSize,
                          const STDAQ::version& bankVersion) const;

private:
  DeUTDetector *m_utDet;

  ToolHandle<IUTReadoutTool> m_readoutTool { this, "ReadoutTool", "UTReadoutTool" };

  mutable Gaudi::Accumulators::SummingCounter<> m_lostBanks{ this, "lost Banks" };
  mutable Gaudi::Accumulators::Counter<> m_noBanksFound{ this, "no banks found" };
  mutable Gaudi::Accumulators::SummingCounter<> m_skippedBanks{ this, "skipped Banks" };
  mutable Gaudi::Accumulators::Counter<> m_validBanks{ this, "# valid banks" };
  mutable Gaudi::Accumulators::Counter<> m_validSourceID{ this, "# valid source ID" };

  Gaudi::Property<bool> m_skipErrors { this,  "skipBanksWithErrors",  false };
  Gaudi::Property<bool> m_recoverMode { this,  "recoverMode",  true };

};
#endif // PRSTOREUTHIT_H
