#ifndef PRFTINFO_H 
#define PRFTINFO_H 1

#include <string>
#include <array>
#include "Kernel/STLExtensions.h"

/** Constant information of the detector
 *  @author Sebastien Ponce
 *  @date   2016-09-30
 */

namespace PrFTInfo {

  enum Numbers { NFTZones    = 24 , 
                 NFTXLayers  = 6 ,
                 NFTUVLayers = 6 };

  unsigned int nbZones();

  const std::string FTHitsLocation = "FT/FTHits";
  const std::string FTCondLocation = "Conditions/FT";
  const std::string FTZonesLocation = "Conditions/FT/FTZones";

  //layer structure of the FT det
  constexpr auto xZonesUpper = LHCb::make_array( 1 , 7 , 9 , 15 , 17 , 23 );
  constexpr auto xZonesLower = LHCb::make_array( 0 , 6 , 8 , 14 , 16 , 22 );

  constexpr auto uvZonesUpper = LHCb::make_array( 3 , 5 , 11, 13 , 19 , 21 );
  constexpr auto uvZonesLower = LHCb::make_array( 2 , 4 , 10, 12 , 18 , 20 );

  constexpr auto stereoZones = LHCb::make_array( 2, 3, 4, 5,
                                                 10, 11, 12, 13,
                                                 18, 19, 20, 21 );
}

#endif // PRFTINFO_H
