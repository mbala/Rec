#ifndef PRVELOUTTRACK_H
#define PRVELOUTTRACK_H 1

#include "Kernel/LHCbID.h"
#include "boost/container/static_vector.hpp"

// Include files
/** @class PrVeloUTTrack PrVeloUTTrack.h
 *  Small class to hold output of VeloUT.
 *
 *  @author Michel De Cian
 *  @date   2018-02-08
 */

struct PrVeloUTTrack final{
  
  using LHCbIDs = boost::container::static_vector<LHCb::LHCbID, 8>;

  float qOverP;
  LHCbIDs UTIDs;
  const LHCb::Track* veloTr;
  
};

using PrVeloUTTracks = std::vector<PrVeloUTTrack>;


#endif // PRVUTTRACK_H

