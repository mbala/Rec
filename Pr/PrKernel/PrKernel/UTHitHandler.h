#ifndef PRKERNEL_UTHITHANDLER_H
#define PRKERNEL_UTHITHANDLER_H 1

// Include files
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "GaudiKernel/Range.h"
#include "PrKernel/UTHitInfo.h"
#include "PrKernel/UTHit.h"
#include <boost/container/small_vector.hpp>

/**
 *  UTHitHandler contains the hits in the UT detector and the accessor to them
 */
namespace UT
{
  class HitHandler
  {
  public:

    using HitVector = boost::container::small_vector<UT::Hit,10>;
    using HitsInRegion = std::array<HitVector,98>;
    using HitsInLayer = std::array<HitsInRegion,3>;
    using HitsInStation = std::array<HitsInLayer,2>;
    using HitsInUT = std::array<HitsInStation,2>;

    // Method to add Hit in the container
    void AddHit( const DeSTSector* aSector,
                 unsigned int station,
                 unsigned int layer,
                 unsigned int region,
                 unsigned int sector,
                 unsigned int strip,
                 double fracStrip,
                 LHCb::STChannelID chanID,
                 unsigned int size, bool highThreshold )
    {
      double dxDy;
      double dzDy;
      double xAtYEq0;
      double zAtYEq0;
      double yBegin;
      double yEnd;
      //--- this method allow to set the values
      aSector->trajectory( strip, fracStrip/4, dxDy, dzDy, xAtYEq0, zAtYEq0, yBegin, yEnd );
      float cos   = aSector->cosAngle();
      float error = aSector->pitch() / std::sqrt( 12.0 );

      if ( dzDy != 0 ) {
        throw GaudiException("dzDy is not zero", "UTHitHandler", StatusCode::FAILURE);
      }

      (*m_allhits)[station-1][layer-1][region-1][sector-1].emplace_back( chanID, size, highThreshold, dxDy, xAtYEq0, zAtYEq0, yBegin, yEnd, cos, error, strip, fracStrip/4 );
      ++m_nbHits;

    }

    const HitVector& hits( unsigned int station, unsigned int layer, unsigned int region, unsigned int sector ) const {
      return (*m_allhits)[station-1][layer-1][region-1][sector-1];
    }

    unsigned int nbHits() const {
      return m_nbHits;
    }

  private:
    // using a pointer here so that object is efficiently movable.
    // This is a plus as it will be moved to the TES
    std::unique_ptr<HitsInUT> m_allhits{new HitsInUT};
    // keep number of hits
    unsigned int m_nbHits{0};
  };
}
#endif // PRKERNEL_UTHITHANDLER_H
