
// from Gaudi
#include "PrTrackConverter.h"

DECLARE_COMPONENT( PrTrackConverter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrTrackConverter::PrTrackConverter( const std::string& name,
				    ISvcLocator* pSvcLocator ):
Transformer( name, pSvcLocator, 
	     KeyValue{"InputTracksLocation", ""},
	     KeyValue{"OutKeyedTrackLocation", ""}){}

//=============================================================================
// Main execution
//=============================================================================
LHCb::Tracks PrTrackConverter::operator()( const std::vector<LHCb::Track>& inputTracks) const{
  // Loop over the Tracks
  LHCb::Tracks OutputTracks; 
  for( auto& Source_Track  : inputTracks ){
    LHCb::Track* tr = new LHCb::Track;
    //Copy the track content in the new container
    tr->copy( Source_Track );
    OutputTracks.insert( tr );
  } // End loop over Tracks
  return OutputTracks;
}
