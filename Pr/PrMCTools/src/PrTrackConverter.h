#ifndef PRTRACKCONVERTER_H 
#define PRTRACKCONVERTER_H 1

// Include files

// from Gaudi
#include "GaudiAlg/Transformer.h"
#include  "Event/Track.h"
/** @class TrackConverter
 *
 *  This algorithm is a dummy std::vector<LHCb::Track> to keyed container converter, to allow the truth matching to work for the upgrade
 *
 *  @author Renato Quagliani
 *  @date   25-01-2018
 */

class PrTrackConverter : public Gaudi::Functional::Transformer<LHCb::Tracks(const std::vector<LHCb::Track>& )> {
  
 public:
  
  PrTrackConverter( const std::string& name, ISvcLocator* pSvcLocator );
  LHCb::Tracks operator()( const std::vector<LHCb::Track> &inputTracks ) const override;
  
};
#endif // PRTRACKCONVERTER_H
