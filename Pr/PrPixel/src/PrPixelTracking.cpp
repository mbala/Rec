// LHCb
#include "Event/Track.h"
#include "Event/StateParameters.h"
#include "Kernel/VPConstants.h"
// Local
#include "PrPixelTracking.h"
#include <range/v3/algorithm.hpp>
#include <range/v3/view.hpp>
#include "fastAtan2.h"

DECLARE_COMPONENT(PrPixelTracking)

namespace{
  using namespace ranges;
}

namespace VPConf{
  /* Those are the string matching of configuration you can use and it change the behaviour of Pairs creations
            "Default"             : default pixel tracking
            "OnlyForward"         : search only for dr/dz >0 tracks with hits having z> MinZ_ForwardTracks 
            "OnlyBackward"        : search only for dr/dz <0 tracks with hits having z< MaxZ_ForwardTracks 
            "ForwardThenBackward" : Do the OnlyForward  first and OnlyBackward after
            "BackwardThenForward" : Do the OnlyBackward first and OnlyForward after
            "whateverelse"        : No tracking
  */
  namespace {
    static const auto configuration = LHCb::make_array(
          std::make_pair( ConfAlgo::DefaultAlgo,         "Default"),            
          std::make_pair( ConfAlgo::OnlyForward,         "OnlyForward"),        
          std::make_pair( ConfAlgo::OnlyBackward,        "OnlyBackward"),       
          std::make_pair( ConfAlgo::ForwardThenBackward, "ForwardThenBackward"),
          std::make_pair( ConfAlgo::BackwardThenForward, "BackwardThenForward") 
          );
  }
  std::string toString(const ConfAlgo& scheme) {
    auto i = std::find_if( begin(configuration), end(configuration),
                             [&](const std::pair<ConfAlgo,const char*>& p)
                             { return p.first == scheme; } );
    if (i==end(configuration)) { throw std::range_error( "Invalid VPConf::ConfAlgo" ); return "<<<INVALID>>>"; }
      return i->second;
  }

  StatusCode parse(ConfAlgo& result, const std::string& input ) {
    auto i = std::find_if( begin(configuration), end(configuration),
                             [&](const std::pair<ConfAlgo,const char*>& p)
                             { return p.second == input; } );
    if (i==end(configuration)) return StatusCode::FAILURE;
      result = i->first;
      return StatusCode::SUCCESS;
  }
}


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrPixelTracking::PrPixelTracking(const std::string &name,
                                 ISvcLocator *pSvcLocator) :
Transformer(name , pSvcLocator,
            {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light},
             KeyValue{"ClusterOffsets", LHCb::VPClusterLocation::Offsets}},
            KeyValue{"OutputTracksName", LHCb::TrackLocation::Velo}) {
  // use the square of the scatter to avoid calls to sqrt()
  m_maxScatter.declareUpdateHandler([this](Property&) {
      m_maxScatterSq = std::pow( m_maxScatter.value(), 2 );
      }).useUpdateHandler();
  // Note that we need to explicitly call `useUpdateHandler` to make sure the derived value is consistent
}


//=============================================================================
// Initialization
//=============================================================================
StatusCode PrPixelTracking::initialize() {

  StatusCode sc = Transformer::initialize();
  if (sc.isFailure()) return sc;

  // Get detector element.
  m_vp = getDet<DeVP>(DeVPLocation::Default);
  // Make sure we precompute z positions of the modules
  registerCondition(m_vp->sensors().front()->geometry(),
                    &PrPixelTracking::recomputeModuleZPositions);


  // Setup the module-skip mask
  // if( msgLevel(MSG::DEBUG)){
  //   info() << "Modules to skip size: " << m_modulesToSkip.size() << endmsg;
  // }
  for ( unsigned int moduleNumber : m_modulesToSkip ) {
    // if( msgLevel(MSG::DEBUG)){
    //   info() << "Skipping module " << moduleNumber << endmsg;
    // }
    m_modulesToSkipMask.set(moduleNumber);
    m_modulesToSkipForPairsMask.set(moduleNumber);
  }
  for ( unsigned int moduleNumber : m_modulesToSkipForPairs ) {
    m_modulesToSkipForPairsMask.set(moduleNumber);
  }
  // always set Histo top dir if we are debugging histos
#ifdef DEBUG_HISTO
    setHistoTopDir("VP/");
#endif

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
std::vector<LHCb::Track> PrPixelTracking::operator()(const std::vector<LHCb::VPLightCluster>& clusters,
                                                     const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets) const {
  // Search for tracks by finding a pair of hits, then extrapolating.
  std::vector<LHCb::Track> outputTracks;
  outputTracks.reserve(800);

  //---- Get for each cluster the phi value, clusters have been already sorted in VPClus algorithm
  std::vector<float> phi_hits;
  phi_hits.resize(clusters.size() );
  getPhi( clusters, offsets, phi_hits);

  searchByPair(clusters, offsets, outputTracks , phi_hits);

  // counter("NbVeloTracksProduced")+= outputTracks.size();
  // counter("NbClustersProduced")+= clusters.size();
  
  return outputTracks;
}

//============================================================================
// Get phi values of the hits from the clusters
//============================================================================

void PrPixelTracking::getPhi( const std::vector<LHCb::VPLightCluster> & clusters,
                                            const std::array<unsigned, VeloInfo::Numbers::NOffsets> & offsets,
                                            std::vector<float> & phivec) const{
  float phi;
  bool swap;
  for( size_t moduleID = 0; moduleID< VeloInfo::NModules; ++moduleID){
    swap = moduleID%2 == 0;
    for( size_t hit = offsets[moduleID]; hit< offsets[moduleID+1]; ++hit){
      phi = atan2_approximation1( clusters[hit].y(), clusters[hit].x() );
      phivec[hit] =phi + ( phi<0.f && swap ? 360.f : 0.f);
    }
    // if(msgLevel(MSG::DEBUG)){
    //   bool sortedbyphi = std::is_sorted( phivec.begin() + offsets[moduleID], phivec.begin() +offsets[moduleID+1]);
    //   if( !sortedbyphi){
    // 	warning()<<"Wrong sorting in phi for module ID: "<<moduleID<<" MUST BE FIXED"<<endmsg;
    //   }
    // }
  }
  return;
}

//=============================================================================
// Extend track towards smaller/larger Modules depending on algo configuration
// Control flow in extendTrack depends on this small method
//=============================================================================
template< Conf configuration>
inline void PrPixelTracking::updatenextstationsearch( int & next, int value)const{
  if( configuration == Conf::Default){
    next = next-value;
  }
  if( configuration == Conf::Forward){
    next = next-value;
  }
  if( configuration == Conf::Backward){
    next = next +value;
  }
}



//============================================================================
// Rebuild the geometry (in case something changes in the Velo during the run)
//============================================================================
StatusCode PrPixelTracking::recomputeModuleZPositions() {
  // Note that we are taking the position of the first sensor for that module
  // This is what was done in the original PrPixelHitManager but is not
  // necessary correct.
  m_firstModule = 999;
  m_lastModule = 0;
  for (auto sensor : m_vp->sensors()) {
    // Get the number of the module this sensor is on.
    const unsigned int number = sensor->module();
    // 4 sensors per module, we want the average position
    m_moduleZPositions[number] += sensor->z()/4.0;
    if (m_firstModule > number) m_firstModule = number;
    if (m_lastModule < number) m_lastModule = number;
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Extend track towards smaller z,
// on both sides of the detector as soon as one hit is missed.
//=============================================================================
template< Conf configuration>
void PrPixelTracking::extendTrack( const std::vector<LHCb::VPLightCluster>& clusters,
                                   const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                   boost::container::static_vector<size_t,35>& hitbuffer,
                                   int lastmoduleID,
                                   const std::vector<float>& phi_hits,
                                   std::vector<unsigned char>& clusterIsUsed) const {
  // Initially scan every second module (stay on the same side).
  int step = -2;
  // Start two modules behind the last one.

  if( configuration == Conf::Backward){
    // info()<<"Backward changing next value"<<endmsg;
    step = +2;
    //i.e. next = h2->module() + 2 if backward tracks search, move forward in z
  }

  int next;
  // Count modules without hits found.
  // lastmoduleID keeps track of the last index to the module closest in z to the
  // one used to seed the bestHit call
  unsigned foundHits = 2;
  unsigned int missed_consecutive = 0;
  unsigned int missed_OnTrack = 0;
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"Extending track"<<endmsg;
  // }
  while( lastmoduleID >=2 &&
         missed_consecutive <= m_maxMissedConsecutive &&
         missed_OnTrack     <= m_maxMissedOnTrack){
    //always try first same side w.r.t last hit moduleID added on track
    next = lastmoduleID + step;
    if( configuration == Conf::Forward){
      if( m_moduleZPositions[next] < m_ForwardTracks_minZ){
        break;
      }
    }
    if( configuration == Conf::Backward){
      if(  m_moduleZPositions[next] > m_BackwardTracks_maxZ ){
        break;
      }
    }
    if( m_modulesToSkipMask[ next] ){
      lastmoduleID+=step;
      continue;
    }
    // if( msgLevel(MSG::DEBUG)){
    //   info()<<"Best hit same side on next = "<<next <<" lastModuleID = "<<lastmoduleID<<endmsg;
    // }
    size_t h3 = bestHitSameSide( clusters, offsets, next, foundHits, hitbuffer , phi_hits, clusterIsUsed);
    if( h3 < badhit){
      // if( msgLevel(MSG::DEBUG)){
      //  info()<<"besthit same side call hit found"<<endmsg;
      // }
      ++foundHits;
      hitbuffer.push_back(h3);
      missed_consecutive = 0;
      lastmoduleID = next;
      continue;
    } else {
      //you have not find a hit in the same side module, try the other side,
      //subtract (add) 1 to next for forward (backward).
      updatenextstationsearch<configuration>(next, +1);
      if( m_modulesToSkipMask[next]){
        //protect if next you try is in the list to skip
        lastmoduleID+=step;
        continue;
        }
      // if( msgLevel(MSG::DEBUG)){
      //   info()<<"Best hit change side on next = "<<next <<" lastModuleID = "<<lastmoduleID<<endmsg;
      // }
      h3 = bestHitChangeSide(clusters, offsets, next, foundHits, hitbuffer , phi_hits, lastmoduleID, clusterIsUsed);
      // if( msgLevel(MSG::DEBUG)){
      //   info()<<"besthit change side call done"<<endmsg;
      // }
      if( h3 < badhit){
        // if( msgLevel(MSG::DEBUG)){
        //   info()<<"besthit change side call hit found"<<endmsg;
        // }
            ++foundHits;
          hitbuffer.push_back(h3);
        missed_consecutive = 0;
        lastmoduleID = next;
        continue;
        } else {
        //increase counters of missing moudules (+1 means you missed both left and right)
        missed_OnTrack ++ ;
        missed_consecutive++ ;
        //if you were trying to extend a doublet and you have not found anything at first trial;
        //break here, too large jump otherwise and very poor precision in extrapolation
        if( m_earlykill3hittracks && foundHits == 2 ) break;
        updatenextstationsearch<configuration>( lastmoduleID, 2);
        continue;
        }
      }
    }
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"Exit extending track , hitfound "<<hitbuffer.size()<<endmsg;
  // }
    }
//=========================================================================
//  Search starting with a pair of consecutive modules.
//=========================================================================
void PrPixelTracking::searchByPair(const std::vector<LHCb::VPLightCluster>& clusters,
                                   const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                   std::vector<LHCb::Track>& outputTracks,
                                   const std::vector<float>& phi_hits ) const
{

  // cache 3-hit tracks to only fit if the hits are really unused after the pattern reco is done
  std::vector<size_t> ThreeHitVec;
  ThreeHitVec.reserve(300);
  // vector storing whether hits are used. 0 means unused, other value is used
  std::vector<unsigned char> clusterIsUsed(clusters.size(), 0);
  // allocate a buffer used for temporary storage of the tracks we are building
  // it is allocated here and passed to doPairSearch in order to reuse the
  // memory and avoid new allocation for each track
  boost::container::static_vector<size_t,35> hitbuffer{};
  // allocate a PrPixelTrack object that will be reused for all tracks, again
  // optimizing memory allocations
  PrPixelTrack FitTrack{35};
  
  // Get the range of modules to start search on,
  // starting with the one at largest Z.
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"searchByPari send command"<<endmsg;
  // }
  switch( m_ConfAlgo){
  case VPConf::ConfAlgo::DefaultAlgo:
    //do default pattern reco
    doPairSearch<Conf::Default>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    break;
  case VPConf::ConfAlgo::OnlyForward:
    //do onlyforward pattern reco
    doPairSearch<Conf::Forward>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    break;
  case VPConf::ConfAlgo::OnlyBackward: 
    //do onlybackward pattern reco
    doPairSearch<Conf::Backward>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    break;
  case VPConf::ConfAlgo::ForwardThenBackward:
    //do forward pattern reco then backward
    doPairSearch<Conf::Forward>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    doPairSearch<Conf::Backward>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    break;
  case VPConf::ConfAlgo::BackwardThenForward: 
    //do backward pattern reco then backward
    doPairSearch<Conf::Backward>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    doPairSearch<Conf::Forward>( clusters, clusterIsUsed, offsets, outputTracks, ThreeHitVec, FitTrack, hitbuffer ,phi_hits);
    break;
  default: 
    throw("impossible algorithm configuration"); 
    break;
  }
  
  //info() << "starting 3 hit loop " << ThreeHitVec.size()/3 << endmsg;
  // if( msgLevel(MSG::DEBUG)){
  //   if( m_hardFlagging && ThreeHitVec.size()!=0){
  //     info()<<"Something goes wrong: ThreeHitVec never filled if m_hardFlagging is ON"<<endmsg;
  //   }
  // }
  for(size_t i = 0; i < ThreeHitVec.size(); i+=3){
    const size_t h0 = ThreeHitVec[i];
    const size_t h1 = ThreeHitVec[i + 1];
    const size_t h2 = ThreeHitVec[i + 2];
    if( clusterIsUsed[h0] || clusterIsUsed[h1] || clusterIsUsed[h2] ) continue;
    FitTrack.fill(clusters, h0, h1, h2);
    FitTrack.fit();
    if ( FitTrack.chi2() > m_maxChi2Short.value()) continue;
    hitbuffer.clear();
    for(size_t j = 0; j < 3; ++j) hitbuffer.push_back(ThreeHitVec[i + j]);
    //for 3 hit vector ariseing from backward and forward,
    makeLHCbTracks<Conf::Default>(FitTrack, hitbuffer, clusters, outputTracks);
  }
}


template<Conf configuration>
void PrPixelTracking::doPairSearch( const std::vector<LHCb::VPLightCluster>& clusters,
                                    std::vector<unsigned char>& clusterIsUsed,
                                    const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                    std::vector<LHCb::Track>& outputTracks,
                                    std::vector<size_t>& ThreeHitVec,
                                    PrPixelTrack& FitTrack,
                                    boost::container::static_vector<size_t,35>& hitbuffer,
                                    const std::vector<float>& phi_hits) const{
  
  unsigned int lastModule  = m_lastModule;
  unsigned int firstModule = m_firstModule;
  
  if( configuration == Conf::Default){
    firstModule = firstModule +4;
  }
  if( configuration == Conf::Forward){
    firstModule = firstModule +4;	
  }
  if( configuration == Conf::Backward){
    lastModule = lastModule - 4 ;
  }

  int startmodule = lastModule;
  int endmodule   = firstModule;
      
  int step   = -2;
  int factor =  1;
  
  if( configuration == Conf::Backward){
    step   = +2;
    factor = -1;
    std::swap( startmodule, endmodule);
  }

  unsigned int  loopsens = 0;

  for (int sens0 = startmodule; factor*endmodule <= factor*sens0; ){
    // Pick-up the "paired" module one station backwards
    const int sens1 = sens0 + step ;
    const int sens2 = sens1 + step ;
    if ( m_modulesToSkipForPairsMask[sens0] ||
          m_modulesToSkipForPairsMask[sens1] ||
          m_modulesToSkipForPairsMask[sens2] ){
      sens0 -= factor;
      // if( msgLevel(MSG::DEBUG)){
      //   debug()<<"Skipping "<<sens0<<":"<<sens1<<" module pairs for doublet creation"<<endmsg;
      // }
      continue;
    }

    const float z1 = m_moduleZPositions[sens1];
    //If you do forward  velo track search [Conf::Forward],
    //don't even consider of building pairs with second module at less than z<m_ForwardTracks_minZ [mm]
    //If you do backward velo track search [Conf::Backward],
    //don't even consider of building pairs with second module at more than z>m_BackwardTracks_maxZ [mm]
    if( configuration == Conf::Forward){
      if( z1 < m_ForwardTracks_minZ ){
        // if( msgLevel(MSG::DEBUG)){
        //   info()<<"Forward velo track search break sens1 z limit"<<endmsg;
        // }
    	break;
      }
    }
    if( configuration == Conf::Backward){ 
      if( z1 > m_BackwardTracks_maxZ ){
        // if( msgLevel(MSG::DEBUG)){
        //   info()<<"Backward velo track search break sens1 z limit"<<endmsg;
        // }
    	break;
      }
    }

    const float z0 = m_moduleZPositions[sens0];
    const float dz = z0 - z1;
    const float invdz = 1./dz;

    // Calculate the search window from the slope limits.
    const float dxMax = m_maxXSlope * fabs(dz);
    const float dyMax = m_maxYSlope * fabs(dz);
    // Loop over hits in the first module (larger Z) in the pair.
    auto firstHit1 = offsets[sens1];
    auto pastLastHit1 = offsets[sens1+1];
    //empty sens1, no pair to build
    // if( msgLevel(MSG::DEBUG)){
    //   info() << "Sensors for pair building: " << sens0 << " " << sens1 << endmsg;
    // }
    float phi0, phi_min, phi_max , phi1;
    for (size_t hit0 = offsets[sens0]; hit0 < offsets[sens0+1]; ++hit0) {
      if (clusterIsUsed[hit0]) continue;
      phi0 = phi_hits[hit0];
      phi_min = phi0-m_PhiPairs.value();
      phi_max = phi0+m_PhiPairs.value();
      //Hits are exposed sorted by phi value now.
      if( m_earlykill3hittracks){
        size_t nbhits_next        =  std::upper_bound( phi_hits.begin()+offsets[sens1],
                                                       phi_hits.begin()+offsets[sens1+1],
                                                       phi_max)
                                    -std::lower_bound( phi_hits.begin()+offsets[sens1],
                                                       phi_hits.begin()+offsets[sens1+1],
                                                       phi_min);
    
        size_t nbhits_next_next   = std::upper_bound( phi_hits.begin()+offsets[sens1+step],
                                                      phi_hits.begin()+offsets[sens1+step+1],
                                                      phi_max)
                                    -std::lower_bound( phi_hits.begin()+offsets[sens1+step],
                                                       phi_hits.begin()+offsets[sens1+step+1],
                                                       phi_min);
        if( !(nbhits_next >0 && nbhits_next_next>0 )) continue;
      }
      // size_t nbhits_next = std::upper_b
      // Skip hits already assigned to tracks.
      //info() << "hit0: " << clusters[hit0].x()<< " " << clusters[hit0].y()<< " " << clusterIsUsed[hit0] << endmsg;
      const float x0 = clusters[hit0].x();
      const float y0 = clusters[hit0].y();
      // Loop over hits in the second module (smaller Z) in the pair.
      for (size_t hit1 = firstHit1; hit1 < pastLastHit1; ++hit1) {
        //killin of doublets order : used hits skip, phi search windows, dr/dz, x, y
        // Skip hits already assigned to tracks.
        phi1 = phi_hits[hit1];
        if( phi1 < phi_min){
          //caching hit to start in module1 for next pair using next starting hit0 in module0
	  firstHit1 = hit1 + 1;
	  continue;
	}
        if( phi1 > phi_max) break;
	if (clusterIsUsed[hit1]) continue;
        // Stop search when above the Phi limit.
        const float x1 = clusters[hit1].x();
        const float y1 = clusters[hit1].y();
	
        //Apply dr/dz cut
	if( configuration == Conf::Forward){
          // if( msgLevel(MSG::DEBUG)){
	  // info()<<"Continuing forward -----"<<endmsg;
          // }
          if( x1*( x0-x1)*invdz + y1*( y0-y1)*invdz <0. ) {
            continue;
          }
        }
        if( configuration == Conf::Backward){
          // if( msgLevel(MSG::DEBUG)){
	  // info()<<"Continuing backward -----"<<endmsg;
          // }
          if(x1*( x0-x1)*invdz + y1*( y0-y1)*invdz >0.) {
            continue;
          }
        }
        // Skip hits outside the X-pos. limit.
        if( fabs(x1-x0) > dxMax) continue;
        if( fabs(y1-y0) > dyMax) continue;
        //info() << "hit1: " << clusters[hit1].x()<< " " << clusters[hit1].y()<< " " << clusterIsUsed[hit1] << endmsg;
	// Make a seed track out of these two hits.
	// Extend the seed track towards smaller Z.
        // if(msgLevel(MSG::DEBUG)){
        //   info()<<"push back in hit buffer hit 0  indx = "<<hit0<<"  and hit 1 at indx = "<<hit1<<endmsg; 
        // }
        hitbuffer.clear();
        //Order of push back matters in bestHit call later in extendTrack.
        //Last hit in hitbuffer here must be the one closest in z to where you propagate the seeding doublet
	hitbuffer.push_back(hit0);
	hitbuffer.push_back(hit1);
        // if( msgLevel(MSG::DEBUG)){
        //   info()<<"hitbuffer size = "<<hitbuffer.size();
        // }
	// info() << "extendTrack" << endmsg;
      	extendTrack<configuration>(clusters, offsets, hitbuffer, sens1, phi_hits, clusterIsUsed);
	if ( hitbuffer.size() < 3 ) {
          continue;
        }
      	if ( hitbuffer.size() == 3 && !m_hardFlagging) {
          //No hard flagging, fill the 3 hit vector
	  ThreeHitVec.insert(ThreeHitVec.end(), hitbuffer.begin(), hitbuffer.begin()+3);
	  continue;
	}


	unsigned unUsed = 0;
	for ( size_t hit : hitbuffer )
	  if ( !clusterIsUsed[hit] ) ++unUsed;
      	if ( unUsed < hitbuffer.size() * m_fractionUnused.value() ) {
          continue;
      	}
        
        //3 hit track and hard flagging.
        if( hitbuffer.size() == 3 && m_hardFlagging){
          if( unUsed != 3){
            continue;
          }else{
            //fill the 3 hit tracks as well, flag hits if passing chi2 cut
            FitTrack.fill( clusters, hitbuffer);
            FitTrack.fit();
            if( FitTrack.chi2() > m_maxChi2Short.value()){
          continue;
	}
	    for( size_t hit: hitbuffer){
	      clusterIsUsed[hit] =1;
	    }
            makeLHCbTracks<configuration>(FitTrack, hitbuffer, clusters, outputTracks);
            break;
          }
        }
	//for (const auto idx : hitbuffer) IsUsedVec.set( idx );
	FitTrack.fill(clusters, clusterIsUsed, hitbuffer);
	FitTrack.fit();
	makeLHCbTracks<configuration>(FitTrack, hitbuffer, clusters, outputTracks);
        // if( msgLevel(MSG::DEBUG)){
        //   info() << "=== Store track nhits " << hitbuffer.size() << endmsg;
        // }
	break;
      } // hit1
    } //hit0

    if( m_skiploopsens){
      if( loopsens == 1){
        if( sens0 >= 44 ){
          sens0-=factor;
          continue;
        }else{
          sens0 -= 3*factor;
          loopsens=0;
          continue;
        }
      }else{
    sens0 -= factor;
        loopsens++;
        continue;
      }
    }
    sens0 -= factor;
  } // sensor loop
}

//=========================================================================
// Convert the local tracks to LHCb tracks
//=========================================================================
template< Conf configuration>
void PrPixelTracking::makeLHCbTracks( PrPixelTrack& track,
                                      const boost::container::static_vector<size_t,35>& hitbuffer,
                                      const std::vector<LHCb::VPLightCluster>& clusters,
                                      std::vector<LHCb::Track>& outputTracks ) const
{
  // Create a new LHCb track.
  // XXXX To be improve when C++17 is used : emplace_back will return newTrack directly
  outputTracks.emplace_back((int)outputTracks.size());
  LHCb::Track& newTrack = outputTracks.back();
  newTrack.states().reserve(6);
  newTrack.setType(LHCb::Track::Velo);
  newTrack.setHistory(LHCb::Track::PatFastVelo);
  newTrack.setPatRecStatus(LHCb::Track::PatRecIDs);
  {
    //In Forward approach ids already reversed-sorted if lhcbID is increasing for increasing moduleID (direct order for backward tracks)
    std::vector<LHCb::LHCbID> ids;
    ids.reserve( hitbuffer.size());
    if( configuration == Conf::Forward){
      //tracks are created by large z to small z, lhcbID ordered with increasing z, reverse iteration on hit buffer.
      for( unsigned i = hitbuffer.size() ; i--!=0; ){
	ids.push_back( clusters[ hitbuffer[i] ].channelID());
      }
    }
    if( configuration == Conf::Backward){
      //tracks are created by small  to larger z, lhcbID ordered with decreasing z
      for( size_t hit: hitbuffer){
	ids.push_back( clusters[hit].channelID());
      }
    }
    if( configuration == Conf::Default){
    for ( size_t hit : hitbuffer ){
      ids.push_back(clusters[hit].channelID());
    }
    std::sort(ids.begin(), ids.end());
    }
    // if( msgLevel(MSG::DEBUG)){
    //   if( !std::is_sorted( ids.begin(), ids.end()) ){
    // 	std::sort( ids.begin(), ids.end());
    // 	info()<<"Not sorted! in configuration = "<<configuration<<endmsg;
    //   }
    // }
    newTrack.setSortedLhcbIDs(std::move(ids));
  }

  // Decide if this is a forward or backward track.
  // Calculate z where the track passes closest to the beam.
  const float zBeam = track.zBeam();
  // Define backward as z closest to beam downstream of hits.
  const bool backward = zBeam > track.hitsZ().front();
  newTrack.setFlag(LHCb::Track::Backward, backward);

  // Get the state at zBeam from the straight line fit.
  LHCb::State state;
  state.setLocation(LHCb::State::ClosestToBeam);
  state.setState(track.state(zBeam));
  state.setCovariance(track.covariance(zBeam));

  // Parameters for kalmanfit scattering. calibrated on MC, shamelessly
  // hardcoded:
  const float tx = state.tx();
  const float ty = state.ty();
  const float scat2 = 1e-8 + 7e-6 * (tx * tx + ty * ty);

  // The logic is a bit messy in the following, so I hope we got all cases
  // right
  if (m_stateClosestToBeamKalmanFit ||
      m_addStateFirstLastMeasurementKalmanFit) {
    // Run a K-filter with scattering to improve IP resolution
    LHCb::State upstreamstate;
    track.fitKalman(upstreamstate, backward ? 1 : -1, scat2);
    // Add this state as state at first measurement if requested
    if (m_addStateFirstLastMeasurementKalmanFit) {
      upstreamstate.setLocation(LHCb::State::FirstMeasurement);
      newTrack.addToStates(upstreamstate);
    }
    // Transport the state to the closestToBeam position
    if (m_stateClosestToBeamKalmanFit) {
      upstreamstate.setLocation(LHCb::State::ClosestToBeam);
      upstreamstate.linearTransportTo(zBeam);
      newTrack.addToStates(upstreamstate);
    }
  }
  if (!m_stateClosestToBeamKalmanFit) {
    newTrack.addToStates(state);
  }

  // Set state at last measurement, if requested
  if ((!backward && m_stateEndVeloKalmanFit) ||
      m_addStateFirstLastMeasurementKalmanFit) {
    LHCb::State downstreamstate;
    track.fitKalman(downstreamstate, backward ? -1 : +1, scat2);
    if (m_addStateFirstLastMeasurementKalmanFit) {
      downstreamstate.setLocation(LHCb::State::LastMeasurement);
      newTrack.addToStates(downstreamstate);
    }
    if (m_stateEndVeloKalmanFit) {
      state = downstreamstate;
    }
  }

  // Add state at end of velo
  if (!backward) {
    state.setLocation(LHCb::State::EndVelo);
    state.linearTransportTo(StateParameters::ZEndVelo);
    newTrack.addToStates(state);
  }

  // Set the chi2/dof
  newTrack.setNDoF(2*hitbuffer.size() - 4);
  newTrack.setChi2PerDoF(track.chi2());
}

 inline float maxPhiInModule( unsigned moduleID,  const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,const std::vector<float>& phi_hits ){
    return phi_hits[offsets[moduleID+1]-1];
 }

 inline float minPhiInModule( unsigned moduleID,  const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,const std::vector<float>& phi_hits ){
    return phi_hits[offsets[moduleID]];
 }

//=========================================================================
// Add hits from the specified module to the track
//=========================================================================
size_t PrPixelTracking::bestHitSameSide( const std::vector<LHCb::VPLightCluster>& clusters,
                                 const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                 size_t next,
                                 unsigned foundHits,
                                 boost::container::static_vector<size_t,35>& hitbuffer ,
                                 const std::vector<float>& phi_hits,
                                 std::vector<unsigned char>& clusterIsUsed) const
{
  //same side, same sorting, same ordering and values of range of phi values for hits
  unsigned int firstHit = offsets[next];
  unsigned int endHit = offsets[next+1];
  //empty module
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"bestHitSameSide inside: firstHit = "<<firstHit<<"   endHit "<< endHit<<endmsg;
  //   if( endHit < firstHit){ info()<<"severe errror in endHit/firstHit offsets!"<<endmsg;}
  // }
  if (endHit == firstHit) return badhit;
  //grep last hit added to track
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"accessing hit1 , found hits = "<<foundHits<<endmsg;
  // }
  const size_t hit1 = hitbuffer[foundHits-1];
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"accessing phi1 , phi hits size ="<<phi_hits.size()<<", hit 1 loaded = "<<hit1<<endmsg;
  // }
  const float phi1  = phi_hits[hit1];
  const float phi2_min = phi1 - m_PhiExtrap.value();
  const float phi2_max = phi1 + m_PhiExtrap.value();

  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"Checking phi on boundaries max"<<endmsg;
  // }
  
  if( maxPhiInModule(next, offsets, phi_hits) < phi2_min)  return badhit;

  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"Checking phi on boundaries min"<<endmsg;
  // }

  if( minPhiInModule(next, offsets, phi_hits) > phi2_max ) return badhit;


  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"Lower /upper bounds"<<endmsg;
  // }
  // Do a binary search through the hits.
  // Early kill of extrapolation if no hits withitn the phi window.
  size_t hit_start = std::lower_bound( phi_hits.begin()+ firstHit, 
                                       phi_hits.begin()+ endHit,
                                       phi2_min) - phi_hits.begin();

  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"hitstart from lower bound "<<hit_start<<endmsg;
  // }
  size_t hit_end = std::upper_bound(   phi_hits.begin()+ firstHit,
                                       phi_hits.begin()+ endHit,
                                       phi2_max) - phi_hits.begin();
  // if( msgLevel(MSG::DEBUG)){
  //   info()<<"hitend from upper bound "<<hit_end<<endmsg;
  // }
  if( hit_end - hit_start == 0) return badhit;
  const size_t hit0 = hitbuffer[foundHits - 2];

  const float x0 = clusters[hit0].x();
  const float z0 = clusters[hit0].z();
  const float x1 = clusters[hit1].x();
  const float z1 = clusters[hit1].z();

  //info() << "BestHit start: " << x0 << " " << clusters[hit0].y() << " " << z0 << " " << x1 << " " << clusters[hit1].y() << " " << z1 << endmsg;

  const float td = 1.0 / (z1 - z0);
  const float txn = (x1 - x0);
  const float tx = txn * td;

  // Extrapolate to the z-position of the module
  // const float module_z = m_moduleZPositions[next];

  // If the first hit is already below this limit we can stop here
  const float y0 = clusters[hit0].y();
  const float y1 = clusters[hit1].y();
  const float tyn = (y1 - y0);
  const float ty = tyn * td;

#ifdef DEBUG_HISTO
  unsigned int nFound = 0;
#endif
  float bestScatter = m_maxScatter;
  size_t bestHit = badhit;
  for (size_t i = hit_start; i < hit_end; ++i) {
    if(clusterIsUsed[i]) continue;
    const float hit_x = clusters[i].x();
    const float hit_z = clusters[i].z();
    const float dz = hit_z - z0;
    const float xPred = x0 + tx * dz;
#ifdef DEBUG_HISTO
    plot((hit.x() - xPred) / m_extraTol, "HitExtraErrPerTol",
         "Hit X extrapolation error / tolerance", -4.0, +4.0, 400);
#endif
    const float dx = xPred - hit_x;
    // If x-position is above prediction +- tolerance, keep looking (hits sorted by phi)
    if (fabs(dx) > m_extraTol) continue;
    const float hit_y = clusters[i].y();
    const float yPred = y0 + ty * dz;
    const float dy = yPred - hit_y;
    // Skip hits outside the y-position tolerance.
    if (fabs(dy) > m_extraTol) continue;
    //info() <<  hit_x << endmsg;
    const float scatterDenom = 1.0 / (hit_z - z1);
    const float scatterNum = (dx * dx) + (dy * dy);
    const float scatter = scatterNum * scatterDenom * scatterDenom;
    if (scatter < bestScatter) {
      //info() <<  hit_x << endmsg;
      bestHit = i;
      bestScatter = scatter;
    }
#ifdef DEBUG_HISTO
    if (scatter < m_maxScatter) ++nFound;
    plot(sqrt(scatter), "HitScatter", "hit scatter [rad]", 0.0, 0.5, 500);
    plot2D(dx, dy, "Hit_dXdY",
           "Difference between hit and prediction in x and y [mm]", -1, 1, -1,
           1, 500, 500);
#endif
  }
#ifdef DEBUG_HISTO
  plot(nFound, "HitExtraCount",
       "Number of hits within the extrapolation window with chi2 within limits"
       0.0, 10.0, 10);
#endif
  //if ( bestHit < badhit ) info() << "at end of BestHit: " << clusters[bestHit].x() << endmsg;
  return bestHit;
}


size_t PrPixelTracking::bestHitChangeSide( const std::vector<LHCb::VPLightCluster>& clusters,
                                           const std::array<unsigned, VeloInfo::Numbers::NOffsets>& offsets,
                                           size_t next,
                                           unsigned foundHits,
                                           boost::container::static_vector<size_t,35>& hitbuffer ,
                                           const std::vector<float>& phi_hits,
                                           size_t moduleIDlastAdded,
                                           std::vector<unsigned char>& clusterIsUsed)const
{
  //same side, same sorting, same ordering and values of range of phi values for hits
  unsigned int firstHit = offsets[next];
  unsigned int endHit = offsets[next+1];
  //empty module
  if (endHit == firstHit) return badhit;
  //grep last hit added to track
  const size_t hit1 = hitbuffer[foundHits-1];
  float phi1  = phi_hits[hit1];
  // if( msgLevel(MSG::DEBUG)){
  //     if( next%2 != moduleIDlastAdded%2){
  //       info()<<"bestHitChangeSide should be called with next on different side of last hit"<<endmsg;
  //     };
  // }
  //apply offset value to phi in previous hit (sitting on a different side module of the one under inspection)
  //offset for phi value is needed because the phi-sorting is achieved internally to each module. 
  //"Dog eating his tail"
  float offset;
  if( moduleIDlastAdded%2 == 0){    
    if( phi1> 270.f){
      offset = -360.f;
    }else if( phi1 < 130.f){
      offset = 0.f;
    }else{
      return badhit;
    }
  }else{
    if( phi1< -50.f){
      offset = +360.f;
    }else if( phi1 > 90.f){
      offset = 0.f;
    }else{
      return badhit;
    }
  }
  phi1 = phi1+offset;
  const float phi2_min = phi1 - m_PhiExtrap.value();
  const float phi2_max = phi1 + m_PhiExtrap.value();

  //first hit, smallest phi in next station
  // if( maxPhiInModule(next, offsets, phi_hits) < phi2_min)  return badhit;
  // if( minPhiInModule(next, offsets, phi_hits) > phi2_max ) return badhit;


 
  // Do a binary search through the hits.
  // Early kill of extrapolation if no hits within the phi window
  size_t hit_start = std::lower_bound( phi_hits.begin()+ firstHit, 
                                       phi_hits.begin()+ endHit,
                                       phi2_min) - phi_hits.begin();

  //Starting doublet [prev and prev-prev hits] computed values.
  const size_t hit0 = hitbuffer[foundHits - 2];
  const float x0 = clusters[hit0].x();
  const float z0 = clusters[hit0].z();
  const float x1 = clusters[hit1].x();
  const float z1 = clusters[hit1].z();
  
  // if( msgLevel(MSG::DEBUG)){
  //   info() << "BestHit start: " << x0 << " " << clusters[hit0].y() << " " << z0 << " " << x1 << " " << clusters[hit1].y() << " " << z1 << endmsg;
  // }

  const float td = 1.0 / (z1 - z0);
  const float txn = (x1 - x0);
  const float tx = txn * td;

  // If the first hit is already below this limit we can stop here
  const float y0 = clusters[hit0].y();
  const float y1 = clusters[hit1].y();
  const float tyn = (y1 - y0);
  const float ty = tyn * td;

#ifdef DEBUG_HISTO
  unsigned int nFound = 0;
#endif

  float bestScatter = m_maxScatterSq;
  size_t bestHit = badhit;
  float phi2 ;
  for (size_t i = hit_start; i <offsets[next+1]; ++i) {
    if(clusterIsUsed[i]) continue;
    phi2 = phi_hits[i];
    if( phi2 > phi2_max) break;
    // if( msgLevel(MSG::DEBUG)){
    //   info()<<"phi2_max = "<<phi2_max<<" phi2_min ="<<phi2_min<<" currently processing "<<phi2<<endmsg;
    // }

    const float hit_x = clusters[i].x();
    const float hit_z = clusters[i].z();
    const float dz = hit_z - z0;
    const float xPred = x0 + tx * dz;

#ifdef DEBUG_HISTO
    plot((hit.x() - xPred) / m_extraTol, "HitExtraErrPerTol",
         "Hit X extrapolation error / tolerance", -4.0, +4.0, 400);
#endif
    const float dx = xPred - hit_x;
    // If x-position is above prediction +- tolerance, keep looking (hits sorted by phi)
    if (fabs(dx) > m_extraTol) continue;
    const float hit_y = clusters[i].y();
    const float yPred = y0 + ty * dz;
    const float dy = yPred - hit_y;
    // Skip hits outside the y-position tolerance.
    if (fabs(dy) > m_extraTol) continue;
    //info() <<  hit_x << endmsg;
    const float scatterDenom = 1.0 / (hit_z - z1);
    const float scatterNum = (dx * dx) + (dy * dy);
    const float scatter = scatterNum * scatterDenom * scatterDenom;
    if (scatter < bestScatter) {
      //info() <<  hit_x << endmsg;
      bestHit = i;
      bestScatter = scatter;
    }
#ifdef DEBUG_HISTO
    if (scatter < m_maxScatterSq) ++nFound;
    plot(sqrt(scatter), "HitScatter", "hit scatter [rad]", 0.0, 0.5, 500);
    plot2D(dx, dy, "Hit_dXdY",
           "Difference between hit and prediction in x and y [mm]", -1, 1, -1,
           1, 500, 500);
#endif
  }
#ifdef DEBUG_HISTO
  plot(nFound, "HitExtraCount",
       "Number of hits within the extrapolation window with chi2 within limits"
       0.0, 10.0, 10);
#endif
  //if ( bestHit < badhit ) info() << "at end of BestHit: " << clusters[bestHit].x() << endmsg;
  return bestHit;
}

//=========================================================================
// Print all hits on a track.
//=========================================================================
void PrPixelTracking::printTrack(PrPixelTrack& track) const {
  for (size_t hit=0; hit < track.size(); ++hit) {
    info() << format(" x%8.3f y%8.3f z%8.2f",
                     track.hitsX()[hit], track.hitsY()[hit], track.hitsZ()[hit]);//, track.hitsID()[hit].lhcbID());
    info() << endmsg;
  }
}
