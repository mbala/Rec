// Include files
#include "Kernel/STLExtensions.h"
#include <limits>
#include "STDet/DeUTSector.h"
#include <boost/container/small_vector.hpp>

// local
#include "PrVeloUT.h"
#include "LHCbMath/GeomFun.h"
//-----------------------------------------------------------------------------
// Implementation file for class : PrVeloUT
//
// 2007-05-08 : Mariusz Witek
// 2017-03-01: Christoph Hasse (adapt to future framework)
//-----------------------------------------------------------------------------

namespace {

  template < typename > constexpr bool false_v = false;
  bool rejectTrack(const LHCb::Track& track){
    return track.checkFlag( LHCb::Track::Flags::Backward )
      || track.checkFlag( LHCb::Track::Flags::Invalid );
  }


  // -- These things are all hardcopied from the PrTableForFunction
  // -- and PrUTMagnetTool
  // -- If the granularity or whatever changes, this will give wrong results

  int masterIndex(const int index1, const int index2, const int index3){
    return (index3*11 + index2)*31 + index1;
  }

  constexpr std::array<float,3> minValsBdl = { -0.3, -250.0, 0.0 };
  constexpr std::array<float,3> maxValsBdl = { 0.3, 250.0, 800.0 };
  constexpr std::array<float,3> deltaBdl   = { 0.02, 50.0, 80.0 };

  constexpr std::array<float,4> dxDyHelper = { 0.0, 1.0, -1.0, 0.0 };
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrVeloUT )
//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PrVeloUT::PrVeloUT(const std::string& name,
                   ISvcLocator* pSvcLocator) :
Transformer(name, pSvcLocator,
            {KeyValue{"InputTracksName", LHCb::TrackLocation::Velo},
#ifndef SMALL_OUTPUT
             KeyValue{"InputVerticesName", LHCb::RecVertexLocation::Velo3D},
#endif
             KeyValue{"UTHits", UT::Info::HitLocation}
            },
            KeyValue{"OutputTracksName", LHCb::TrackLocation::VeloTT}) {
}

/// Initialization
StatusCode PrVeloUT::initialize() {
  auto sc = Transformer::initialize();
  if (sc.isFailure()) return sc;  // error printed already by GaudiAlgorithm

  // m_zMidUT is a position of normalization plane which should to be close to z middle of UT ( +- 5 cm ).
  // Cached once in PrVeloUTTool at initialization. No need to update with small UT movement.
  m_zMidUT = m_PrUTMagnetTool->zMidUT();
  // zMidField and distToMomentum isproperly recalculated in PrUTMagnetTool when B field changes
  m_distToMomentum = m_PrUTMagnetTool->averageDist2mom();

  if (m_doTiming) {
    m_timerTool->increaseIndent();
    m_veloUTTime = m_timerTool->addTimer( "Internal VeloUT Tracking" );
    m_timerTool->decreaseIndent();
  }

  // Get detector element.
  m_utDet = getDet<DeSTDetector>(DeSTDetLocation::UT);
  // Make sure we precompute z positions/sizes of the layers/sectors
  registerCondition(m_utDet->geometry(), &PrVeloUT::recomputeGeometry);

  return StatusCode::SUCCESS;
}

StatusCode PrVeloUT::recomputeGeometry() {
  for(int iStation = 0; iStation < 2; ++iStation){
    for(int iLayer = 0; iLayer < 2; ++iLayer){
      // get layer
      unsigned int layerIndex = 2*iStation+iLayer;
      DeSTLayer* layer = m_utDet->layers()[layerIndex];
      // get the z position of the layer and store it
      m_layers[layerIndex].z = layer->sectors()[0]->sensors()[0]->plane().ProjectOntoPlane({0,0,0}).z();
      // get the layer size and sector sizes. Go through all sectors, do not rely on ordering
      float YFirstRow = std::numeric_limits<float>::max();
      float YLastRow = std::numeric_limits<float>::lowest();
      float smallestXLastCol = std::numeric_limits<float>::max();
      float smallestXFirstcol = std::numeric_limits<float>::max();
      float biggestXFirstCol = std::numeric_limits<float>::lowest();
      unsigned int biggestColumn = 0;
      unsigned int smallestColumn = 999;
      unsigned int topMostRow = 0;
      unsigned int bottomMostRow = 999;
      // First pass
      // deal with col/row numbers, we need a UTSector for that
      // Note that rows/cols cannot be relied on the middle columns/rows
      // as they are not anymore "rows/cols" but "number of sectors
      // in the column/row". So we use only first column and row
      for (const auto& sector : layer->sectors()) {
        DeUTSector* utSector = dynamic_cast<DeUTSector*>(sector);
        auto column = utSector->column();
        auto row = utSector->row();
        smallestColumn = std::min<float>(smallestColumn, column);
        if (utSector->column() == smallestColumn) {
          topMostRow = std::max<float>(topMostRow, row);
          bottomMostRow = std::min<float>(bottomMostRow, row);
        }
        if (utSector->row() == bottomMostRow) {
          biggestColumn = std::max<float>(biggestColumn, column);
        }
      }
      // Second pass
      // find x and y values in the corners to deduce the geometry of the layer
      for (const auto& sector : layer->sectors()) {
        // deal with x,y coordinates. Remember the corner coordinates
        DeUTSector* utSector = dynamic_cast<DeUTSector*>(sector);
        auto column = utSector->column();
        auto row = utSector->row();
        auto center = sector->geometry()->toGlobal(Gaudi::XYZPoint{0,0,0});
        if (column == smallestColumn) {
          if (row == bottomMostRow) {
            smallestXFirstcol = center.x();
            YFirstRow = center.y();
          } else if (row == topMostRow) {
            biggestXFirstCol = center.x();
            YLastRow = center.y();
          }
        }
        if (column == biggestColumn && row == bottomMostRow) {
          smallestXLastCol = center.x();
        }
        m_sectorsZ[iStation][iLayer][sector->elementID().detRegion()-1][sector->elementID().sector()-1] = center.z();
      }
      // gather all information into the corresponding LayerInfo object
      auto ncols = biggestColumn - smallestColumn + 1;
      auto nrows = topMostRow - bottomMostRow + 1;
      m_layers[layerIndex].nColsPerSide = ncols / 2;
      m_layers[layerIndex].nRowsPerSide = nrows / 2;
      m_layers[layerIndex].invHalfSectorYSize = 2*(nrows-1)/(YLastRow-YFirstRow);
      m_layers[layerIndex].invHalfSectorXSize = 2*(ncols-1)/(smallestXLastCol-smallestXFirstcol);
      m_layers[layerIndex].dxDy = (biggestXFirstCol-smallestXFirstcol)/(YLastRow-YFirstRow);
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
PrVeloUT::OutputContainer PrVeloUT::operator()(const std::vector<LHCb::Track>& inputTracks,
#ifndef SMALL_OUTPUT
                                               const std::vector<LHCb::RecVertex>& vertices,
#endif
                                               const UT::HitHandler& hh) const {

  if ( m_doTiming ) m_timerTool->start( m_veloUTTime );

  OutputContainer outputTracks;
  outputTracks.reserve(inputTracks.size());
  counter("#seeds") += inputTracks.size();

  const std::vector<float> fudgeFactors = m_PrUTMagnetTool->returnDxLayTable();
  const std::vector<float> bdlTable     = m_PrUTMagnetTool->returnBdlTable();

  std::array<UT::Mut::Hits,4> hitsInLayers;
  for( auto& it : hitsInLayers ) it.reserve(8); // check this number!

  auto has_any_small_ip = [&](const LHCb::Track& t) {
    return std::any_of( vertices.begin(), vertices.end(),
                        [&t,min_ip=m_minIP.value()](const auto& v) {
                            const auto& pos = v.position();
                            const auto& s = t.closestState(pos.Z());
                            auto line  = Gaudi::Math::Line{ s.position(), s.slopes() };
                            auto ipvec = Gaudi::Math::closestPoint(pos, line)-pos;
                            return ipvec.Mag2() < min_ip*min_ip;
                        } );
  };

  for(const LHCb::Track& veloTr: inputTracks) {

    if( rejectTrack( veloTr ) || ( m_doIPCut && has_any_small_ip(veloTr) )) continue;

    MiniState trState;
    if( !getState(veloTr, trState, outputTracks)) continue;

    for( auto& it : hitsInLayers ) it.clear();
    if( !getHits(hitsInLayers, hh, fudgeFactors, trState) ) continue;

    TrackHelper helper(trState, m_zKink, m_sigmaVeloSlope, m_maxPseudoChi2);

    //counter("formingClusters")++;
    if( !formClusters(hitsInLayers, helper) ){
      std::reverse(hitsInLayers.begin(),hitsInLayers.end());
      //counter("reversing")++;
      formClusters(hitsInLayers, helper);
      std::reverse(hitsInLayers.begin(),hitsInLayers.end());
    }

    if( helper.bestHits[0]){
      prepareOutputTrack(veloTr, helper, hitsInLayers, outputTracks, bdlTable);
    }

  }

  counter("#tracks") += outputTracks.size();
  if ( m_doTiming ) m_timerTool->stop( m_veloUTTime );
  return outputTracks;
}
//=============================================================================
// Get the state, do some cuts
//=============================================================================
template <typename Container>
bool PrVeloUT::getState(const LHCb::Track& iTr, MiniState& trState, Container& outputTracks) const {
  static_assert(    std::is_same_v<Container,std::vector<LHCb::Track>>
                 || std::is_same_v<Container,PrVeloUTTracks> );
  const LHCb::State* s = iTr.stateAt(LHCb::State::Location::EndVelo);
  const LHCb::State& state = s ? *s : (iTr.closestState(LHCb::State::Location::EndVelo));

  // -- reject tracks outside of acceptance or pointing to the beam pipe
  trState.tx = state.tx();
  trState.ty = state.ty();
  trState.x = state.x();
  trState.y = state.y();
  trState.z = state.z();

  const float xMidUT =  trState.x + trState.tx*(m_zMidUT-trState.z);
  const float yMidUT =  trState.y + trState.ty*(m_zMidUT-trState.z);

  if( xMidUT*xMidUT+yMidUT*yMidUT  < m_centralHoleSize*m_centralHoleSize ) return false;
  if( (std::abs(trState.tx) > m_maxXSlope) || (std::abs(trState.ty) > m_maxYSlope) ) return false;

  if(m_passTracks && std::abs(xMidUT) < m_passHoleSize && std::abs(yMidUT) < m_passHoleSize){
    if constexpr ( std::is_same_v<Container,std::vector<LHCb::Track>> ) {
        outputTracks.emplace_back(iTr);
        auto& outTr = outputTracks.back();
        outTr.addToAncestors( iTr );
    } else if constexpr ( std::is_same_v<Container,PrVeloUTTracks> ) {
        outputTracks.emplace_back();
        outputTracks.back().qOverP = 0;
        outputTracks.back().veloTr = iTr;
    } else {
        static_assert(false_v<Container>,"Container must either be vector<Track> or PrVeloUTTracks" );
    }
    return false;
  }

  return true;

}
//=============================================================================
// Find the hits
//=============================================================================
 bool PrVeloUT::getHits(std::array<UT::Mut::Hits,4>& hitsInLayers,
                        const UT::HitHandler& hh,
                        const std::vector<float>& fudgeFactors, MiniState& trState ) const {

  // -- This is hardcoded, so faster
  // -- If you ever change the Table in the magnet tool, this will be wrong
  const float absSlopeY = std::abs( trState.ty );
  const int index = (int)(absSlopeY*100 + 0.5);
  const std::array<float,4> normFact = { fudgeFactors[4*index], fudgeFactors[1 + 4*index], fudgeFactors[2 + 4*index], fudgeFactors[3 + 4*index] };

  // -- this 500 seems a little odd...
  const float invTheta = std::min(500.,1.0/std::sqrt(trState.tx*trState.tx+trState.ty*trState.ty));
  const float minMom   = std::max(m_minPT.value()*invTheta, m_minMomentum.value());
  const float xTol     = std::abs(1. / ( m_distToMomentum * minMom ));
  const float yTol     = m_yTol + m_yTolSlope * xTol;

  int nLayers = 0;
  boost::container::small_vector<std::pair<int, int>,9> sectors;

  for(int iStation = 0; iStation < 2; ++iStation){

    if( iStation == 1 && nLayers == 0 ){
      //counter("#NoHitsFound")++;
      return false;
    }

    for(int iLayer = 0; iLayer < 2; ++iLayer){
      if( iStation == 1 && iLayer == 1 && nLayers < 2 ) return false;

      const unsigned int layerIndex = 2*iStation + iLayer;
      const float z = m_layers[layerIndex].z;
      const float yAtZ   = trState.y + trState.ty*(z - trState.z);
      const float xLayer = trState.x + trState.tx*(z - trState.z);
      const float yLayer = yAtZ + yTol*m_layers[layerIndex].dxDy;
      const float normFactNum = normFact[layerIndex];
      const float invNormFact = 1.0/normFactNum;

      findSectors(layerIndex, xLayer, yLayer, xTol*invNormFact-std::abs(trState.tx)*m_intraLayerDist.value(),
                  m_yTol + m_yTolSlope * std::abs(xTol*invNormFact), sectors);

      const SectorsInLayerZ& sectorsZForLayer = m_sectorsZ[iStation][iLayer];
      std::pair<int, int> pp{-1, -1};
      for (auto& p : sectors) {
        // sectors can be duplicated in the list, but they are ordered
        if (p == pp) continue;
        pp = p;
        const UT::HitHandler::HitVector& hits = hh.hits( iStation+1, iLayer+1, p.first, p.second );
        if( UNLIKELY( hits.empty() ) ) continue;
        findHits(hits.begin(), hits.end(), sectorsZForLayer[p.first-1][p.second-1], trState, xTol*invNormFact, invNormFact, hitsInLayers[layerIndex]);
      }

      sectors.clear();
      nLayers += int(!hitsInLayers[2*iStation + iLayer].empty());
    }
  }

  return nLayers > 2;
}

//=========================================================================
// Form clusters
//=========================================================================
bool PrVeloUT::formClusters(const std::array<UT::Mut::Hits,4>& hitsInLayers, TrackHelper& helper) const {

  bool fourLayerSolution = false;

  for(const auto& hit0 : hitsInLayers[0]){

    const float xhitLayer0 = hit0.x;
    const float zhitLayer0 = hit0.z;

    // Loop over Second Layer
    for(const auto& hit2 : hitsInLayers[2]){

      const float xhitLayer2 = hit2.x;
      const float zhitLayer2 = hit2.z;

      const float tx = (xhitLayer2 - xhitLayer0)/(zhitLayer2 - zhitLayer0);

      if( std::abs(tx-helper.state.tx) > m_deltaTx2 ) continue;

      const UT::Mut::Hit* bestHit1 = nullptr;
      float hitTol = m_hitTol2;
      for( auto& hit1 : hitsInLayers[1]){

        const float xhitLayer1 = hit1.x;
        const float zhitLayer1 = hit1.z;

        const float xextrapLayer1 = xhitLayer0 + tx*(zhitLayer1-zhitLayer0);
        if(std::abs(xhitLayer1 - xextrapLayer1) < hitTol){
          hitTol = std::abs(xhitLayer1 - xextrapLayer1);
          bestHit1 = &hit1;
        }
      }

      if( fourLayerSolution && !bestHit1) continue;

      const UT::Mut::Hit* bestHit3 = nullptr;
      hitTol = m_hitTol2;
      for( auto& hit3 : hitsInLayers[3]){

        const float xhitLayer3 = hit3.x;
        const float zhitLayer3 = hit3.z;

        const float xextrapLayer3 = xhitLayer2 + tx*(zhitLayer3-zhitLayer2);

        if(std::abs(xhitLayer3 - xextrapLayer3) < hitTol){
          hitTol = std::abs(xhitLayer3 - xextrapLayer3);
          bestHit3 = &hit3;
        }
      }

      // -- All hits found
      if( bestHit1 && bestHit3 ){
        simpleFit( LHCb::make_array( &hit0, bestHit1, &hit2, bestHit3 ), helper);

        if(!fourLayerSolution && helper.bestHits[0]){
          fourLayerSolution = true;
        }
        continue;
      }

      // -- Nothing found in layer 3
      if( !fourLayerSolution && bestHit1 ){
        simpleFit( LHCb::make_array( &hit0, bestHit1, &hit2 ),  helper);
        continue;
      }
      // -- Noting found in layer 1
      if( !fourLayerSolution && bestHit3 ){
        simpleFit( LHCb::make_array( &hit0, bestHit3, &hit2 ), helper);
        continue;
      }
    }
  }

  return fourLayerSolution;
}
//=========================================================================
// Create the Velo-TU tracks
//=========================================================================
template <typename Container>
void PrVeloUT::prepareOutputTrack(const LHCb::Track& veloTrack,
                                  const TrackHelper& helper,
                                  const std::array<UT::Mut::Hits,4>& hitsInLayers,
                                  Container& outputTracks,
                                  const std::vector<float>& bdlTable) const {

  //== Handle states. copy Velo one, add TT.
  const float zOrigin = (std::fabs(helper.state.ty) > 0.001)
    ? helper.state.z - helper.state.y / helper.state.ty
    : helper.state.z - helper.state.x / helper.state.tx;

  //const float bdl1    = m_PrUTMagnetTool->bdlIntegral(helper.state.ty,zOrigin,helper.state.z);

  // -- These are calculations, copied and simplified from PrTableForFunction
  const std::array<float,3> var = { helper.state.ty, zOrigin, helper.state.z };

  const int index1 = std::max(0, std::min( 30, int((var[0] + 0.3)/0.6*30) ));
  const int index2 = std::max(0, std::min( 10, int((var[1] + 250)/500*10) ));
  const int index3 = std::max(0, std::min( 10, int( var[2]/800*10)        ));

  float bdl = bdlTable[masterIndex(index1, index2, index3)];

  const std::array<float,3> bdls = { bdlTable[masterIndex(index1+1, index2,index3)],
                                     bdlTable[masterIndex(index1,index2+1,index3)],
                                     bdlTable[masterIndex(index1,index2,index3+1)] };

  const std::array<float,3> boundaries = { -0.3f + float(index1)*deltaBdl[0],
                                           -250.0f + float(index2)*deltaBdl[1],
                                           0.0f + float(index3)*deltaBdl[2] };

  // -- This is an interpolation, to get a bit more precision
  float addBdlVal = 0.0;
  for(int i=0; i<3; ++i) {

    if( var[i] < minValsBdl[i] || var[i] > maxValsBdl[i] ) continue;

    const float dTab_dVar =  (bdls[i] - bdl) / deltaBdl[i];
    const float dVar = (var[i]-boundaries[i]);
    addBdlVal += dTab_dVar*dVar;
  }
  bdl += addBdlVal;
  // ----

  const float qpxz2p =-1*vdt::fast_isqrt(1.+helper.state.ty*helper.state.ty)/bdl*3.3356/Gaudi::Units::GeV;
  const float qop = (std::abs(bdl) < 1.e-8) ? 0.0 : helper.bestParams[0]*qpxz2p;

  // -- Don't make tracks that have grossly too low momentum
  // -- Beware of the momentum resolution!
  const float p  = 1.3*std::abs(1/qop);
  const float pt = p*std::sqrt(helper.state.tx*helper.state.tx + helper.state.ty*helper.state.ty);

  if( p < m_minMomentum || pt < m_minPT ) return;

  const float txUT = helper.bestParams[3];

if constexpr ( std::is_same_v<Container,std::vector<LHCb::Track>> ) {

  const float xUT  = helper.bestParams[2];
  outputTracks.emplace_back(veloTrack, veloTrack.key());
  auto& outTr = outputTracks.back();

  // Adding overlap hits
  for( const auto* hit : helper.bestHits){

    // -- only the last one can be a nullptr.
    if( !hit ) break;

    outTr.addToLhcbIDs( hit->HitPtr->lhcbID() );

    const float xhit = hit->x;
    const float zhit = hit->z;

    for( auto& ohit : hitsInLayers[hit->HitPtr->planeCode()]){
      const float zohit = ohit.z;
      if(zohit==zhit) continue;

      const float xohit = ohit.x;
      const float xextrap = xhit + txUT*(zhit-zohit);
      if( xohit-xextrap < -m_overlapTol) continue;
      if( xohit-xextrap > m_overlapTol) break;
      outTr.addToLhcbIDs( ohit.HitPtr->lhcbID() );
      // -- only one overlap hit
      //break;
    }
  }

  // set q/p in all of the existing states
  for(auto& state : outTr.states()) state.setQOverP(qop);

  const float yMidUT =  helper.state.y + helper.state.ty*(m_zMidUT-helper.state.z);

  //== Add a new state...
  LHCb::State temp;
  temp.setLocation( LHCb::State::Location::AtTT );
  temp.setState( xUT,
                 yMidUT,
                 m_zMidUT,
                 txUT,
                 helper.state.ty,
                 qop );


  outTr.addToStates( std::move(temp) );
  outTr.setType( LHCb::Track::Types::Upstream );
  outTr.setHistory( LHCb::Track::History::PatVeloTT );
  outTr.addToAncestors( veloTrack );
  outTr.setPatRecStatus( LHCb::Track::PatRecStatus::PatRecIDs );
  outTr.setChi2PerDoF( helper.bestParams[1]);

} else if constexpr ( std::is_same_v<Container,PrVeloUTTracks> ) {

  outputTracks.emplace_back();
  //outputTracks.back().UTIDs.reserve(8);

  // Adding overlap hits
  for( const auto* hit : helper.bestHits){

    // -- only the last one can be a nullptr.
    if( !hit ) break;

    outputTracks.back().UTIDs.push_back(hit->HitPtr->lhcbID());

    const float xhit = hit->x;
    const float zhit = hit->z;

    for( auto& ohit : hitsInLayers[hit->HitPtr->planeCode()]){
      const float zohit = ohit.z;
      if(zohit==zhit) continue;

      const float xohit = ohit.x;
      const float xextrap = xhit + txUT*(zhit-zohit);
      if( xohit-xextrap < -m_overlapTol) continue;
      if( xohit-xextrap > m_overlapTol) break;
      outputTracks.back().UTIDs.push_back(ohit.HitPtr->lhcbID());
      // -- only one overlap hit
      break;
    }
  }

  /*
  outTr.x = helper.state.x;
  outTr.y = helper.state.y;
  outTr.z = helper.state.z;
  outTr.tx = helper.state.tx;
  outTr.ty = helper.state.ty;
  */
  outputTracks.back().qOverP = qop;
  outputTracks.back().veloTr = veloTrack;


} else {
     // impossible
     static_assert(false_v<Container>,"wrong type");
}

}
