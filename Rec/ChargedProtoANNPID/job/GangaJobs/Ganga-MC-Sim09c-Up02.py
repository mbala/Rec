
def jobExists(name):
    from GangaCore.GPI import jobs
    js = jobs.select(name=name)
    return len(js) > 0

def dateString():
    import datetime
    now = datetime.datetime.now()
    return now.strftime("%d%m%Y")

datasets = [

    #{ "EvTypes" : ["11102202"],
    #  "simcond" : "Upgrade",
    #  "year"    : "Upgrade",
    #  "lhccond" : "Nu7.6-25ns",
    #  "en"      : "7000GeV",
    #  "pols"    : ["MagDown","MagUp"],
    #  #"trig"    : "Trig0x410700a1",
    #  "sim"     : ["Sim09c-Up02"],
    #  "reco"    : "Reco-Up01",
    #  #"turbo"   : "Turbo01a",
    #  #"strp"    : "Stripping23r1NoPrescalingFlagged",
    #  "pythias" : ["Pythia8"],
    #  "ftype"   : "LDST" 
    #  }

    { "EvTypes" : ["11102005","11104124","11114001","11124001","11144201","11296013",
                   "11874091","11874401","12103032","12143001","12163021","12873441",
                   "13104001","13112001","13144011","13154001","13264021","13264031",
                   "13512010","15204010","15336011","15874041","16166011","18112001",
                   "27163002","27165000","27373001","42122000","37103000","11102404",
                   "11114101","11140400","11142401","11144008","11144103","11160001",
                   "11574001","12103022","12165106","12203224","12245021","12103213",
                   "12875401","13102202","13104012","13134061","13774004","14177001",
                   "14195002","21163000","27165100","27165101","27175002","31113001",
                   "32113001","34112106","34112407","40114010","42112050","42311000",
                   "42321000","49000054","11102202","15102307","12265042","14103034"],
      "simcond" : "Upgrade",
      "year"    : "Upgrade",
      "lhccond" : "Nu7.6-25ns",
      "en"      : "7000GeV",
      "pols"    : ["MagDown","MagUp"],
      #"trig"    : "Trig0x410700a1",
      "sim"     : ["Sim09c-Up02"],
      "reco"    : "Reco-Up01",
      #"turbo"   : "Turbo01a",
      #"strp"    : "Stripping23r1NoPrescalingFlagged",
      "pythias" : ["Pythia8"],
      "ftype"   : "LDST" 
      }
    
        ]

myproxy = DiracProxy(group='lhcb_calibration')

# Project Directory
#projectDir = str( os.path.abspath(os.path.dirname(sys.argv[0])) ) 
#projectDir = projectDir.replace('/var/clus','')
#projectDir = projectDir.replace('/B2DKGammaNtuple/jobs/B2D0hD2KsPiPiDalitz/data','')
projectDir = "/usera/jonesc/LHCbCMake/Releases/DaVinciDev_v43r1"
print "Project Dir", projectDir

for dataset in datasets :

    EvTypes = dataset["EvTypes"]
    simcond = dataset["simcond"]
    year    = dataset["year"]
    lhccond = dataset["lhccond"]
    en      = dataset["en"]
    pols    = dataset["pols"]
    #trig    = dataset["trig"]
    sims    = dataset["sim"]
    reco    = dataset["reco"]
    #turbo   = dataset["turbo"]
    #strp    = dataset["strp"]
    pythias = dataset["pythias"]
    ftype   = dataset["ftype"]

    for EvType in EvTypes :
        for pythia in pythias :
            for polarity in pols :

                for sim in sims :

                    #jname = EvType+"-"+simcond+"-MC"+year+"-ANNPID-"+polarity+"-"+pythia+sim+trig+reco+turbo+strp
                    jname = EvType+"-"+simcond+"-MC"+year+"-ANNPID-"+polarity+"-"+pythia+sim+reco
                    if jobExists(jname+'*') :
                        
                        print "Job", jname, "already exists. Skipping."
                        
                    else:

                        #datapath = ("/MC/"+simcond+"/Beam"+en+"-"+year+"-"+polarity+"-"+lhccond+"-"+pythia+"/"+sim+"/"+trig+"/"+reco+"/"+turbo+"/"+strp+"/"+EvType+"/"+ftype).replace("//", "/")

                        

                        datapath = ("/MC/"+simcond+"/Beam"+en+"-"+year+"-"+polarity+"-"+lhccond+"-"+pythia+"/"+sim+"/"+reco+"/"+EvType+"/"+ftype).replace("//", "/")

                        datalfns = BKQuery( path = datapath, credential_requirements = myproxy ).getDataset()

                        import random
                        random.shuffle(datalfns.files)
                        
                        print "Extracted", len(datalfns), "LFNS for", datapath

                        if len(datalfns) > 0 :
                            
                            j = Job( application = GaudiExec() )
                            j.name = jname+"-"+dateString()
                            j.application.directory = projectDir
                            j.application.platform = 'x86_64-slc6-gcc62-opt'

                            # Main options
                            j.application.options = [ 'options-MC'+year+'.py' ]
                            
                            j.splitter = SplitByFiles ( filesPerJob = 4, maxFiles = 500 )
                            #j.splitter = SplitByFiles ( filesPerJob = 1, maxFiles = 5 )

                            j.splitter.credential_requirements = myproxy
                            
                            #rootfiles = [ LocalFile('ProtoPIDANN.MC.tuples.root') ]
                            rootfiles = [ 'ProtoPIDANN.MC.tuples.root' ]
                            
                            j.outputfiles = rootfiles
                            
                            j.inputdata = datalfns
                            
                            #j.do_auto_resubmit = True
                            
                            j.backend = Dirac( credential_requirements = myproxy )
                            
                            print "Submitting job", j.name
                            j.submit()
                            #queues.add( j.submit )
                            # j.remove()
