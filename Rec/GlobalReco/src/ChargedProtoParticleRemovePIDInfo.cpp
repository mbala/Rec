
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleRemovePIDInfo.cpp
 *
 * Implementation file for algorithm ChargedProtoParticleRemovePIDInfo
 *
 * @author Dmitry Golubkov
 * @date 13/03/2010
 */
//-----------------------------------------------------------------------------

// local
#include "ChargedProtoParticleRemovePIDInfo.h"

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleRemovePIDInfo )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ChargedProtoParticleRemovePIDInfo::
ChargedProtoParticleRemovePIDInfo( const std::string& name,
                                   ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{

  // context specific locations
  if ( context() == "HLT" || context() == "Hlt" )
  {
    m_protoPath  = LHCb::ProtoParticleLocation::HltCharged;
  }
  else
  {
    m_protoPath  = LHCb::ProtoParticleLocation::Charged;
  }

  // ProtoParticles
  declareProperty( "ProtoParticleLocation", m_protoPath );
  declareProperty( "RemoveRichPID", m_RemoveRichPID = true, "remove Rich PID info from the ProtoParticles"    );
  declareProperty( "RemoveMuonPID", m_RemoveMuonPID = true, "remove Muon PID info from the ProtoParticles"    );
  declareProperty( "RemoveCombPID", m_RemoveCombPID = true, "remove Combined PID info from the ProtoParticles");
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ChargedProtoParticleRemovePIDInfo::execute()
{
  // ProtoParticle container
  auto * protos = getIfExists<LHCb::ProtoParticles>(m_protoPath);
  if ( !protos )
  {
    return Warning( "No existing ProtoParticle container at " +  m_protoPath + " thus do nothing.",
                    StatusCode::SUCCESS );
  }

  // Loop over proto particles
  for ( auto * proto : *protos )
  {

    if ( msgLevel(MSG::VERBOSE) )
      verbose() << "Trying ProtoParticle " << proto->key() << endmsg;

    // Erase current RichPID information
    if ( m_RemoveRichPID ) proto->removeRichInfo();

    // Erase current MuonPID information
    if ( m_RemoveMuonPID ) proto->removeMuonInfo();

    // Erase current CombPID information
    if ( m_RemoveCombPID ) proto->removeCombinedInfo();

    // print full ProtoParticle content
    if ( msgLevel(MSG::VERBOSE) ) verbose() << *proto << endmsg;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
