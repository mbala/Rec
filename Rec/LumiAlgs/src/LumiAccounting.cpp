// Include files

// from Gaudi
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/GaudiException.h"

// event model
#include "Event/RawEvent.h"
#include "Event/ODIN.h"
#include "Event/HltLumiSummary.h"
#include "Event/LumiFSR.h"
#include "Event/LumiCounters.h"
#include "Event/LumiMethods.h"

// CondDB
#include "DetDesc/Condition.h"
#include "GaudiKernel/IDetDataSvc.h"

// local
#include "LumiAccounting.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LumiAccounting
//
// 2009-01-19 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LumiAccounting )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LumiAccounting::LumiAccounting( const std::string& name,
                                ISvcLocator* pSvcLocator)
  : Consumer ( name , pSvcLocator,
               { KeyValue{ "RawEventLocation"  ,  LHCb::RawEventLocation::Default },
                 KeyValue{ "InputDataContainer",  LHCb::HltLumiSummaryLocation::Default },
                 KeyValue{ "ODINLocation" , LHCb::ODINLocation::Default } } )
{
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode LumiAccounting::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // get the File Records service
  m_fileRecordSvc = service("FileRecordDataSvc", true);


  // prepare TDS for FSR
  m_lumiFSRs = new LHCb::LumiFSRs();
  put(m_fileRecordSvc.get(), m_lumiFSRs, m_FSRName);
  // create a new FSR and append to TDS
  m_lumiFSR = new LHCb::LumiFSR();
  m_lumiFSRs->insert(m_lumiFSR);

  // initialize calibration factors
  for ( int key = 0; key <= LHCb::LumiCounters::counterKey::Random; key++ ) {
    std::string counterName = LHCb::LumiCounters::counterKeyToString( key );
    m_calibThresholds.push_back(0);
  }

  // get the detectorDataSvc
  m_dds = detSvc();
  if (!m_dds) {
    error() << "No thresholds defined for lumi counters (no dds)" << endmsg ;
    m_statusThresholds = 0;         // no database
    return StatusCode::SUCCESS;
  }

  // register conditions for database acces
  sc = registerDB(); // must be executed first
  if ( sc.isFailure() ) {
    error() << "No thresholds defined for lumi counters (no registration)" << endmsg ;
    m_statusThresholds = 0;         // no database
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
void LumiAccounting::operator()(const LHCb::RawEvent& event,
                                const LHCb::HltLumiSummary& the_hltLumiSummary,
                                const LHCb::ODIN& odin ) const
{

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // registry from raw data - only correct if file catalogue used
  IOpaqueAddress* eAddr = event.registry()->address();
  // obtain the fileID
  if ( !eAddr ) {
    throw GaudiException( "Registry cannot be loaded from Event", "LumiAccounting::"+name(),StatusCode::SUCCESS);
  }
  std::string event_fname = eAddr->par()[0];
  if ( msgLevel(MSG::DEBUG) ) debug() << "RunInfo record from Event: " << event_fname << endmsg;

  // obtain the run number from ODIN
  unsigned int run = odin.runNumber();
  if ( msgLevel(MSG::DEBUG) ) debug() << "ODIN RunNumber: " << run << endmsg;

  // check if the file ID is new
  std::string fname = event_fname;
  {   // start a new scope to limit the lifetime of the mutex
      std::lock_guard<std::mutex> lock(m_files_mutex);
      m_files.insert(fname);
  }
  if ( msgLevel(MSG::DEBUG) ) debug() << "RunInfo record: " << fname << endmsg;
  // insert new fileID only
  if ( !m_lumiFSR->hasFileID(fname) ) m_lumiFSR->addFileID(fname);
  // insert new run only
  if ( !m_lumiFSR->hasRunNumber(run) ) m_lumiFSR->addRunNumber(run);
  // increment FSR with summary info
  for (const auto&  sum : the_hltLumiSummary.extraInfo()){
    // get the key and value of the input info
    int key = sum.first;
    int value = sum.second;
    // increment!
    m_lumiFSR->incrementInfo(key, value);
    // check if over threshold and increment with offset
    double threshold = m_calibThresholds[key];
    int binary = value > threshold ? 1 : 0 ;
    m_lumiFSR->incrementInfo(key + LHCb::LumiMethods::methodKey::PoissonOffset, binary);
  }
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode LumiAccounting::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  // some printout of FSRs
  if ( msgLevel(MSG::INFO) ) {
    info() << "number of files seen: " << m_files.size() << endmsg;
    // FSR - use the class method which prints it
    LHCb::LumiFSRs::iterator fsr;
    LHCb::LumiFSR sumFSR;
    for ( fsr = m_lumiFSRs->begin(); fsr != m_lumiFSRs->end(); fsr++ ) {
      // sum up the information
      sumFSR += *(*fsr);
      // print the individual FSR
      if ( msgLevel(MSG::VERBOSE) ) verbose() << "FSR: " << *(*fsr) << endmsg;
    }
    // print the integral
    info() << "INTEGRAL: " << sumFSR << endmsg;
  }

  // check if the FSRs can be retrieved from the TS
  if ( msgLevel(MSG::DEBUG) ) {
    LHCb::LumiFSRs* readFSRs = get<LHCb::LumiFSRs>(m_fileRecordSvc.get(), m_FSRName);
    LHCb::LumiFSRs::iterator fsr;
    for ( fsr = readFSRs->begin(); fsr != readFSRs->end(); fsr++ ) {
      // print the FSR just retrieved from TS
      debug() << "READ FSR: " << *(*fsr) << endmsg;

      // print also the contents using the builtin lookup tables
      LHCb::LumiFSR::ExtraInfo::iterator infoIter;
      LHCb::LumiFSR::ExtraInfo  fsrInfo = (*fsr)->extraInfo();
      for (infoIter = fsrInfo.begin(); infoIter != fsrInfo.end(); infoIter++) {
        // get the key and value of the input info
        int key = infoIter->first;
        LHCb::LumiFSR::ValuePair values = infoIter->second;
        int incr = values.first;
        longlong count = values.second;
        const std::string keyName = LHCb::LumiCounters::counterKeyToString(key);
        int keyInt = LHCb::LumiCounters::counterKeyToType(keyName);
        debug() << "SUM: key: " << key
                << " name: " << keyName << " KeyInt: " << keyInt
                << " increment: " << incr << " integral: " << count << endmsg;
      }
    }
  }
  return Consumer::finalize();  // must be called after all other actions
}


//=============================================================================
// DB access
//=============================================================================
StatusCode LumiAccounting::registerDB() {
  // register the DB conditions for the update maganer
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Register DB" << endmsg;

  // register thresholds
  if (this->existDet<Condition>("Conditions/Lumi/LHCb/ThresholdCalibration")) {
    registerCondition("Conditions/Lumi/LHCb/ThresholdCalibration",
                      m_condThresholds, &LumiAccounting::i_cacheThresholdData);
  }
  else {
    error() << "Conditions/Lumi/LHCb/ThresholdCalibration not found" << endmsg;
    m_statusThresholds = 0;        // no thresholds
    return StatusCode::SUCCESS;
  }
  return StatusCode::SUCCESS;
}

//=========================================================================
//  Extract data from relativeCalibration
//=========================================================================
StatusCode LumiAccounting::i_cacheThresholdData() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "callback ThresholdCalibration:" << endmsg;
  std::vector<double> cal = m_condThresholds->paramVect<double>("Thresholds");
  if ( cal.size() == m_calibThresholds.size() ) {
    m_calibThresholds = cal;
    return StatusCode::SUCCESS;
  }
  fatal() << "inconsistent number of parameters in RelativeCalibration:" << cal.size() << endmsg;
  m_statusThresholds = 0;        // no thresholds
  return StatusCode::SUCCESS;

}
