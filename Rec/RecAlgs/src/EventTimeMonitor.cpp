// Include files

// local
#include "EventTimeMonitor.h"
#include "AIDA/IHistogram1D.h"

namespace {
    //@FIXME: what about leapyears?
    static const std::array<unsigned int, 12> month_offsets = { 0,31,60,91,121,152,182,213,244,274,305,335 };
}

//-----------------------------------------------------------------------------
// Implementation file for class : EventTimeMonitor
//
// 2012-04-19 : Patrick Koppenburg
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EventTimeMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
EventTimeMonitor::EventTimeMonitor( const std::string& name,
                                    ISvcLocator* pSvcLocator)
  : Consumer( name , pSvcLocator,
              KeyValue{"Input", LHCb::ODINLocation::Default } )
{}
//=============================================================================
// Initialization
//=============================================================================
StatusCode EventTimeMonitor::initialize() {
  StatusCode sc = Consumer::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiHistoAlg

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  if ( produceHistos() ) {
    m_plotS  = book    ( m_histoS      ) ;
    m_plotH  = book    ( m_histoH      ) ;
    m_plotD  = book    ( m_histoD      ) ;
    m_plotY  = book    ( m_histoY      ) ;
  }
  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
void EventTimeMonitor::operator()(const LHCb::ODIN& odin) const {

  if ( produceHistos() ) {
      const Gaudi::Time gtime = odin.eventTime();
      m_plotY->fill(gtime.year(false));
      m_plotD->fill(month_offsets[gtime.month(false)]+gtime.day(false));
      m_plotH->fill(gtime.hour(false));
      m_plotS->fill(60*gtime.minute(false)+gtime.second(false)+gtime.nsecond()/1000000000.);
  }
}

//=============================================================================
