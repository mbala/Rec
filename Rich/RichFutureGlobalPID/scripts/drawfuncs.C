
void drawfuncs()
{

  auto c = new TCanvas("log(exp(x)-1)","c",1000,800);

  const double fmin = 0.001;
  const double fmax = 5.0;

  // the baseline using STD functions
  auto stdf = []( double*x, double * ) 
    {
      return std::log( std::exp( x[0] ) - 1.0 ); 
    };
  auto tf1 = new TF1( "log(exp(x)-1)", stdf, fmin, fmax, 1 );
  tf1->SetLineColor(kBlack);
  tf1->Draw();

  // approximation
  auto a1 = []( double * xp, double * ) 
    {
      // using log( e^x - 1 ) ~= log(x) + x/2 + x^2/24 - x^4/2880 + x^6/181440

      const float x = (float)xp[0];

      // The log part
      const union { float f; std::uint32_t i; } vx = { x };
      const union { std::uint32_t i; float f; } mx = { ( vx.i & 0x007FFFFF ) | 0x3f000000 };
      const auto y = float(vx.i) * 1.1920928955078125e-7f;
      auto z = 0.69314718f * ( y - 124.22551499f - 1.498030302f * mx.f 
                               - 1.72587999f / ( 0.3520887068f + mx.f ) );

      // the power series
      const float a( 1.0/24.0 );
      const float b( 0.5      );
      z += ( ( ( a * x ) + b ) * x ); // x and x^2 terms
      z -= std::pow(x,4)/2800.0;      // x^4 term
      z += std::pow(x,6)/181440.0;    // x^6 term
      
      // return
      return z;
    };
  auto tf2 = new TF1( "approx1", a1, fmin, fmax, 1 );
  tf2->SetLineColor(kRed);
  tf2->Draw("SAME");

  // faster approximation
  auto a2 = []( double * xp, double * ) 
    {
      // using log( e^x - 1 ) ~= log(x) + x/2 + x^2/24 - x^4/2880 + x^6/181440

      const float x = (float)xp[0];

      // The log part
      const union { float f; std::uint32_t i; } vx = { x };
      auto z = ( float(vx.i) * 8.2629582881927490e-8f ) - 87.989971088f;

      // the power series
      const float a( 1.0/24.0 );
      const float b( 0.5      );
      z += b*x;   // x term
      z += a*x*x; // x^2 term 
      
      // return
      return z;
    };
  auto tf3 = new TF1( "approx2", a2, fmin, fmax, 1 );
  tf3->SetLineColor(kBlue);
  tf3->Draw("SAME");

  c = new TCanvas("log(exp(x)-1) log10(diff)","c",1000,800);

  auto diff1 = [&stdf,&a1]( double * x, double * p ) { return std::log10( std::fabs( a1(x,p) - stdf(x,p) ) ); };
  auto diff2 = [&stdf,&a2]( double * x, double * p ) { return std::log10( std::fabs( a2(x,p) - stdf(x,p) ) ); };

  auto tdiff1 = new TF1( "log(exp(x)-1) | log10( fabs(approx-STD) )", diff1, fmin, fmax, 1 );
  auto tdiff2 = new TF1( "diff2",                                     diff2, fmin, fmax, 1 );

  tdiff1->SetLineColor(kRed);
  tdiff2->SetLineColor(kBlue);

  tdiff1->SetMinimum(-8);
  tdiff1->SetMaximum(0);

  tdiff1->Draw();
  tdiff2->Draw("SAME");

}
