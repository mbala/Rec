
// local
#include "RichGlobalPIDInitialisePIDInfo.h"

using namespace Rich::Future::Rec;
using namespace Rich::Future::Rec::GlobalPID;

//-----------------------------------------------------------------------------
// Implementation file for class : RichGlobalPIDInitialisePIDinfo
//
// 2016-10-25 : Chris Jones
//-----------------------------------------------------------------------------

InitialisePIDInfo::
InitialisePIDInfo( const std::string& name, ISvcLocator* pSvcLocator )
  : MultiTransformer( name, pSvcLocator,
                      { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks } },
                      { KeyValue{ "TrackPIDHyposLocation", TrackPIDHyposLocation::Default },
                        KeyValue{ "TrackDLLsLocation", TrackDLLsLocation::Default } } )
{ }

//-----------------------------------------------------------------------------

OutData
InitialisePIDInfo::operator()( const Summary::Track::Vector& gTracks ) const
{
  // make a default output containers with the correct size
  // and fill with default values
  return OutData( TrackPIDHypos( gTracks.size(), Rich::Pion ),
                  TrackDLLs::Vector( gTracks.size() ) );
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( InitialisePIDInfo )

//=============================================================================
