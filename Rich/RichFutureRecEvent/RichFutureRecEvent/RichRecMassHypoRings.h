
#pragma once

// STL
#include <array>

// Kernel
#include "Kernel/RichParticleIDType.h"
#include "Kernel/FastAllocVector.h"

// Event
#include "RichFutureRecEvent/RichRecRayTracedCKRingPoint.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      /// Type for mass hypothesis rings for each hypothesis
      using MassHypoRings = std::array<RayTracedCKRingPoint::Vector,Rich::NParticleTypes>;

      /// Container of MassHypoRings
      using MassHypoRingsVector = LHCb::STL::Vector<MassHypoRings>;

      /// photon yield TES locations
      namespace MassHypoRingsLocation
      {
        /** Location in TES for the mass hypothesis rings using the emitted photon
         *  spectra Cherenkov angles */
        static const std::string Emitted = "Rec/RichFuture/MassHypoRings/Emitted";
      }

      /// Type for two closest ring points to a given position on the ring
      using ClosestPoints = std::pair< const RayTracedCKRingPoint*, const RayTracedCKRingPoint* >;

      /// Return the points on the ring closest to the given azimuth
      ClosestPoints getPointsClosestInAzimuth( const RayTracedCKRingPoint::Vector & ring,
                                               const float angle );
      
    }
  }
}
