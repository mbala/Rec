
#pragma once

// STL
#include <string>

// Utils
#include "RichUtils/RichSIMDTypes.h"
#include "RichRecUtils/RichPhotonSpectra.h"
#include "RichFutureUtils/RichHypoData.h"

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {

      // ---------------------------------------------------------------------------------

      /// Type for photon predicted pixel signal
      using PhotonSignals = Rich::Future::HypoData<double>;

      /// photon predicted pixel signal TES locations
      namespace PhotonSignalsLocation
      {
        /// Location in TES for the predicted pixel signal
        static const std::string Default = "Rec/RichFuture/PhotonPixelSignals/Default";
      }

      // ---------------------------------------------------------------------------------

      /// SIMD Type for photon predicted pixel signal
      using SIMDPhotonSignals = Rich::Future::HypoData< Rich::SIMD::FP<Rich::SIMD::DefaultScalarFP> >;

      /// TES locations
      namespace SIMDPhotonSignalsLocation
      {
        /// Location in TES for the SIMD predicted pixel signal
        static const std::string Default = "Rec/RichFuture/SIMDPhotonPixelSignals/Default";
      }

      // ---------------------------------------------------------------------------------

      /// Type for flags to say if a photon is active or not. i.e. has any signal.
      using PhotonFlags = LHCb::STL::Vector<bool>;

      /// TES locations
      namespace PhotonActiveFlagsLocation
      {
        /// Location in TES for the active photon flags
        static const std::string Default = "Rec/RichFuture/PhotonActiveFlags/Default";
      }

      // ---------------------------------------------------------------------------------

    }
  }
}
