
#pragma once

// STL
#include <string>
#include <vector>

// Utils
#include "RichFutureUtils/RichHypoData.h"
#include "RichUtils/RichSIMDTypes.h"

// RICH Rec event
#include "RichFutureRecEvent/RichRecRelations.h"

// LHCb Event Model
#include "Event/Track.h"

// Vc
#include <Vc/common/alignedbase.h>

namespace Rich 
{
  namespace Future 
  { 
    namespace Rec 
    { 
      // ===========================================================================
      
      /** @namespace Summary
       *
       *  General namespace for RICH reconstruction summary information
       *
       *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
       *  @date   01/02/2017
       */

      namespace Summary
      {
        
        /** @class Track RichSummaryEventData.h
         *
         *  Summary of the immutable track data.
         *
         *  @author Chris Jones
         *  @date   2016-09-30
         */
        
        class Track final : public Vc::AlignedBase<Vc::VectorAlignment>
        {
          
        public:
          
          /// Type for a container of Tracks
          using Vector = Rich::SIMD::STDVector<Track>;
          
        public:

          /// Constructor from track key and index
          Track( const LHCb::Tracks::key_type key,
                 const std::size_t index ) : m_index(index), m_tkKey(key) { }
          
        public:
          
          /// Read/Write access to threshold flags
          inline HypoData<bool> & thresholds() noexcept { return m_thresholds; }
          
          /// Read/Write access to the total signal values
          inline HypoData<float> & totalSignals() noexcept { return m_totalSignals; }
          
          /// Read/Write access to the photon indices
          inline Relations::PhotonIndices & photonIndices() noexcept { return m_photonIndices; }
          
          /// Set the active flag
          inline void setActive( const bool active ) noexcept { m_active = active; }
          
          /// Set the RICH flags
          inline DetectorArray<bool>& richActive() noexcept { return m_richActive; }
          
          /// Set the radiator flags
          inline RadiatorArray<bool>& radiatorActive() noexcept { return m_radActive; }
          
        public:
          
          /// Read access to the track key
          inline LHCb::Tracks::key_type key() const noexcept { return m_tkKey; }
          
          /// Read access to the index
          inline std::size_t index() const noexcept { return m_index; }
          
          /// Read only access to threshold flags
          inline const HypoData<bool> & thresholds() const noexcept { return m_thresholds; }
          
          /// Read access to the total signal values
          inline const HypoData<float> & totalSignals() const noexcept { return m_totalSignals; }
          
          /// Read access to the photon indices
          inline const Relations::PhotonIndices & photonIndices() const noexcept { return m_photonIndices; }
          
          /// Access the active flag
          inline bool active() const noexcept { return m_active; }
          
          /// Access the RICH flags
          inline const DetectorArray<bool>& richActive() const noexcept { return m_richActive; }
          
          /// Access the radiator flags
          inline const RadiatorArray<bool>& radiatorActive() const noexcept { return m_radActive; }
          
        private:
          
          /// Is this track active
          bool m_active{false};

          /// RICH flags.
          DetectorArray<bool> m_richActive = {{false,false}};

          /// Track index
          std::size_t m_index{0};

          /// Threshold flags. True if above threshold in at least one radiator
          HypoData<bool> m_thresholds;

          /// Total signal yields
          HypoData<float> m_totalSignals;
          
          /// Photon indices
          Relations::PhotonIndices m_photonIndices;

          /// Radiator flags
          RadiatorArray<bool> m_radActive = {{false,false,false}};
          
          /// LHCb Track key
          LHCb::Tracks::key_type m_tkKey{};
          
        };
        
        // ===========================================================================
        
        /** @class Pixel RichSummaryEventData.h
         *
         *  Summary of the immutable pixell data.
         *
         *  @author Chris Jones
         *  @date   2016-09-30
         */

        class Pixel final : public Vc::AlignedBase<Vc::VectorAlignment>
        {
          
        public:
          
          /// Type for a container of Pixels
          using Vector = Rich::SIMD::STDVector<Pixel>;
          
        public:
          
          /// Default constructor
          Pixel() = default;
          
        public:
          
          /// Read/Write access to the photon indices
          inline       Relations::PhotonIndices & photonIndices()       noexcept { return m_photonIndices; }
          
          /// Read access to the photon indices
          inline const Relations::PhotonIndices & photonIndices() const noexcept { return m_photonIndices; }
          
        private:
          
          /// List of photon indices associated to this pixel
          Relations::PhotonIndices m_photonIndices; 
          
        };
        
        // ===========================================================================
        
        /// Summary information TES locations
        namespace TESLocations
        {
          /// Tracks
          static const std::string Tracks = "Rec/RichFuture/Summary/Tracks";
          /// Pixels
          static const std::string Pixels = "Rec/RichFuture/Summary/Pixels";
        }
        
        // ===========================================================================
        
      }
      
    }
  }
}
