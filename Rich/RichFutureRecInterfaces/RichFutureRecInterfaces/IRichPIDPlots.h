
//-----------------------------------------------------------------------------
/** @file IRichPIDPlots.h
 *
 *  Header file for RICH tool interface : Rich::Rec::IPIDPlots
 *
 *  @author Chris Jones       Christopher.Rob.Jones@cern.ch
 *  @date   2008-04-14
 */
//-----------------------------------------------------------------------------

#pragma once

// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/PhysicalConstants.h"

// Kernel
#include "Kernel/RichParticleIDType.h"

// Event
namespace LHCb
{
  class Track;
  class RichPID;
  class ProtoParticle;
}

/// The interface ID
static const InterfaceID IID_IRichPIDPlots ( "IRichPIDPlots", 1, 0 );

namespace Rich
{
  namespace Future
  {
    namespace Rec
    {
      
      /** @class IPIDPlots IRichPIDPlots.h RichKernel/IRichPIDPlots.h
       *
       *  Interface to tool which creates standardised PID plots for
       *  monitoring and calibration purposes.
       *
       *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
       *  @date   2008-04-14
       */
      
      class IPIDPlots : virtual public IAlgTool
      {
        
      public:
        
        /// Return the interface ID
        static const InterfaceID& interfaceID() { return IID_IRichPIDPlots; }
        
        /** Fill the plots for the given RichPID data object and PID hypothesis
         *
         *  @param[in] pid    Pointer to the RichPID data object to fill plots for
         *  @param[in] hypo   The mass hypothesis to assume for this RichPID
         *  @param[in] config (Optional) Configuration object
         */
        virtual void plots( const LHCb::RichPID * pid,
                            const Rich::ParticleIDType hypo ) const = 0;
        
        /** Fill the plots for the given track and PID hypothesis
         *
         *  @param[in] track  Pointer to the track to fill plots for
         *  @param[in] hypo   The mass hypothesis to assume for this track
         *  @param[in] config (Optional) Configuration object
         */
        virtual void plots( const LHCb::Track * track,
                            const Rich::ParticleIDType hypo ) const = 0;
        
        /** Fill the plots for the given ProtoParticle and PID hypothesis
         *
         *  @param[in] proto  Pointer to the ProtoParticle to fill plots for
         *  @param[in] hypo   The mass hypothesis to assume for this track
         *  @param[in] config (Optional) Configuration object
         */
        virtual void plots( const LHCb::ProtoParticle * proto,
                            const Rich::ParticleIDType hypo ) const = 0;
        
      };
      
    }
  }
}
