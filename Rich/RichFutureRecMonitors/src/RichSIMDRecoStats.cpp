
// local
#include "RichSIMDRecoStats.h"

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------
// Implementation file for class : SIMDRecoStats
//
// 2016-11-07 : Chris Jones
//-----------------------------------------------------------------------------

SIMDRecoStats::SIMDRecoStats( const std::string& name, ISvcLocator* pSvcLocator )
  : Consumer( name, pSvcLocator,
              { KeyValue{ "TrackSegmentsLocation",   LHCb::RichTrackSegmentLocation::Default },
                KeyValue{ "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected },
                KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default } } )
{ 
  // init
  m_photCount.fill(0);
}

//-----------------------------------------------------------------------------

void
SIMDRecoStats::operator()( const LHCb::RichTrackSegment::Vector& segments,
                           const Relations::TrackToSegments::Vector& tkToSegs,
                           const SIMDCherenkovPhoton::Vector& photons ) const 
{

  // Local stats objects for this event
  TkStats tkStats;
  RadiatorArray<unsigned long long> photCount = {{}};

  // Loop over the track data
  for ( const auto & rels : tkToSegs )
  {

    // Was this track selected. i.e. does it have at least one segment
    const bool selected = !rels.segmentIndices.empty();

    // radiator flags
    RadiatorArray<bool> radSelected = { false, false, false };
    
    // loop over segments for this track
    for ( const auto & iSeg : rels.segmentIndices )
    {
      // get the segment
      const auto & seg = segments[iSeg];

      // set the radiator flag
      radSelected[seg.radiator()] = true;
    }

    // Update the track stats object for this track
    tkStats.add( selected, radSelected );

  }

  // Loop over photons
  for ( const auto & phot : photons )
  {
    // which radiator
    const auto rad = ( Rich::Rich1 == phot.rich() ? 
                       Rich::Rich1Gas : Rich::Rich2Gas );
    // count scalar photons per radiator
    photCount[rad] += phot.validityMask().count();
  }

  // Finally update the mutable members under a mutex lock
  {
    // the lock
    std::lock_guard<std::mutex> lock(m_updateLock);

    // Update the track stats
    m_tkStats.add( tkStats );

    // update the photon stats
    for ( const auto & rad : Rich::radiators() )
    {
      m_photCount[rad] += photCount[rad];
    }

    // count events
    ++m_Nevts;
  }

}

//=============================================================================

StatusCode SIMDRecoStats::finalize()
{

  // Print the final track stats.
  if ( m_Nevts > 0 )
  {

    // Statistical tools
    const PoissonEffFunctor eff("%6.2f +-%5.2f");
    const StatDivFunctor    occ("%8.2f +-%5.2f");
    
    // Print out final track stats

    info() << "===========================================================================" << endmsg;
    info() << "                 " << name() << " Summary : " << m_Nevts << " events" << endmsg;
    info() << "---------------------------------------------------------------------------" << endmsg;

    info() << "    Selected " << occ(m_tkStats.selTracks,m_Nevts) 
           << " tracks/event : RICH eff " << eff(m_tkStats.selTracks,m_tkStats.triedTracks)
           << " % " << endmsg;

    info() << "---------------------------------------------------------------------------" << endmsg;

    for ( const auto rad : Rich::radiators() )
    {
      if ( m_tkStats.segments[rad] > 0 )
      {
        info() << "             "
               << occ(m_tkStats.segments[rad],m_Nevts)  << " " << rad << "  segments/event" << endmsg;
      }
    }

    info() << "---------------------------------------------------------------------------" << endmsg;

    for ( const auto rad : Rich::radiators() )
    {
      if ( m_photCount[rad] > 0 )
      {
        info() << "             "
               << occ(m_photCount[rad],m_Nevts) << " " << rad << "  photons/event" << endmsg;
      }
    }

    info() << "===========================================================================" << endmsg;

  }

  return Consumer::finalize();
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDRecoStats  )

//=============================================================================
