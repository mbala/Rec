
// local
#include "RichSIMDPixelBackgroundsEstiAvHPD.h"

using namespace Rich::Future::Rec;

//-----------------------------------------------------------------------------
// Implementation file for class : RichPixelbackgroundsEstiAvHPD
//
// 2016-10-25 : Chris Jones
//-----------------------------------------------------------------------------

SIMDPixelBackgroundsEstiAvHPD::
SIMDPixelBackgroundsEstiAvHPD( const std::string& name, ISvcLocator* pSvcLocator )
 : Transformer( name, pSvcLocator,
                { KeyValue{ "TrackToSegmentsLocation",        Relations::TrackToSegmentsLocation::Selected },
                  KeyValue{ "TrackPIDHyposLocation",          TrackPIDHyposLocation::Default },
                  KeyValue{ "TrackSegmentsLocation",          LHCb::RichTrackSegmentLocation::Default },
                  KeyValue{ "GeomEffsPerPDLocation",          GeomEffsPerPDLocation::Default },
                  KeyValue{ "DetectablePhotonYieldLocation",  PhotonYieldsLocation::Detectable },
                  KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default } },
                { KeyValue{ "PixelBackgroundsLocation",       SIMDPixelBackgroundsLocation::Default } } )
{
  // debug
  //setProperty( "OutputLevel", MSG::DEBUG );
}

//-----------------------------------------------------------------------------

StatusCode SIMDPixelBackgroundsEstiAvHPD::initialize()
{
  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // Rich System
  m_richSys = getDet<DeRichSystem>( DeRichLocations::RichSystem );

  // Make sure the PD data is initialised here, before things get multi-threaded
  getPDData();
  
  if ( m_ignoreExpSignal )
  { _ri_debug << "Will ignore expected signals when computing backgrounds" << endmsg; }
  
  return sc;
}

//-----------------------------------------------------------------------------

const SIMDPixelBackgroundsEstiAvHPD::RichPDData & 
SIMDPixelBackgroundsEstiAvHPD::getPDData() const
{
  // The default data
  static RichPDData pdData;
  
  // If needed initialise the PD data.
  // Data is static as does not vary across algorithm instances and
  // is heavy on memory usage, so best to only have one copy.
  // This works even when threaded as long as the initialisation is
  // triggered during the initialize(), as this is always called
  // sequentially for all instances. 
  // Once initialised, access is thread safe.
  static bool isReady = false;
  if ( UNLIKELY( !isReady ) )
  {
    // flag init as done
    isReady = true;
  
    // initialise the data for each PD
    for ( const auto rich : Rich::detectors() )
    {
      auto & richD = pdData[rich];
      for ( const auto side : Rich::sides() )
      {
        auto & panelD = richD[side];

        // Total number of PDs for this panel
        const auto nPDs = maxPdIndex( rich, side );
        
        // number of PD groups
        // NB : This is a poor mans attempt to group the PDs together, using
        //      their copy numbers. This will be correlated to grouping spatially,
        //      but is not perfect. To be addressed once a better PD numbering
        //      scheme is available from RichDet.
        const auto nPDGs = 1 + ( nPDs / m_pdGroupSize[rich] );
        
        // Initalise the PD vector
        panelD.resize( nPDGs );

        // Loop over PDs
        for ( unsigned int index = 0; index < nPDs; ++index )
        {
          // The group ID
          const auto groupID = index / m_pdGroupSize[rich];

          // get the entry in the group vector
          auto & pd = panelD[groupID];

          // Get the DePD
          // try / catch needed for MaPMTs as not all indices are valid...
          // To be improved ...
          try
          { 
            const auto PD = dePD( rich, side, Rich::DAQ::PDPanelIndex(index) ); 
            if ( PD )
            {
              // sanity check on rich...
              if ( UNLIKELY( rich != PD->rich() ) ) { Exception("RICH mis-match"); }

              // add to pixel group size
              pd.effNumPixs += PD->effectiveNumActivePixels();

              _ri_debug << "RICH Data : index " << index
                        << " Group ID " << groupID 
                        << " Eff NumPixs " << pd.effNumPixs << endmsg;
            }
          }
          catch ( const GaudiException & ) 
          {
            _ri_debug << " -> No PD found for " 
                      << rich << " " << side << " index " << index << endmsg;
          }
        }

      } // side loop
    } // RICH loop

  } // isready check

  return pdData;
}

//-----------------------------------------------------------------------------

SIMDPixelBackgrounds
SIMDPixelBackgroundsEstiAvHPD::operator()( const Relations::TrackToSegments::Vector& tkToSegs,
                                           const TrackPIDHypos& tkHypos,
                                           const LHCb::RichTrackSegment::Vector& segments,
                                           const GeomEffsPerPDVector& geomEffsPerPD,
                                           const PhotonYields::Vector& detYieldsV,
                                           const SIMDPixelSummaries& pixels ) const
{
  // the backgrounds to return. Initialize to 0
  SIMDPixelBackgrounds          backgrounds( pixels.size(), SIMDFP::Zero()           );

  // local cache of cluster indices
  SIMD::STDVector<SIMDPDCopyNumber> indices( pixels.size(), SIMDPDCopyNumber::Zero() );

  // The working data, copied from the default instance
  RichPDData pdData = getPDData();
 
  // Zip the segment data together
  const auto segRange = Ranges::ConstZip(segments,geomEffsPerPD,detYieldsV);

  // -----------------------------------------------------------
  // Fill the observed data
  // -----------------------------------------------------------
  for ( auto && pixData : Ranges::Zip(pixels,indices) )
  {
    // get the pixel and background value for this cluster
    const auto & pixel = std::get<0>(pixData);
    auto       & index = std::get<1>(pixData);

    // RICH and panel
    const auto rich = pixel.rich();
    const auto side = pixel.side();

    // Get the data vector for this panel
    auto & dataV = (pdData[rich])[side];

    // Loop over the scalar entries for this SIMD pixel
    for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i )
    {
      // PD ID
      const auto pd = (pixel.smartID()[i]).pdID();

      // Get the PD copy number index
      index[i] = pdIndex(pd,rich);

      // Sanity check
      if ( index[i] < dataV.size() )
      {
        // the working data object for this PD group
        auto & data = dataV[ index[i] ];
        // count the number of hits in each PD, in each RICH
        ++(data.obsSignal);
      }
      else
      {
        Error( "Pixel : PD index " + std::to_string(index[i]) + " out of range !!" ).ignore();
      }
      
    } // pixel scalar loop

  } // SIMD pixels

  // -----------------------------------------------------------
  // Now the expected signals, based on the track information
  // loop over tracks (via the hypo values)
  // -----------------------------------------------------------
  if ( !m_ignoreExpSignal )
  {
    for ( const auto && tk : Ranges::ConstZip(tkToSegs,tkHypos) )
    {
      // get the track data
      const auto & tkRels = std::get<0>(tk);
      const auto & tkHypo = std::get<1>(tk);

      //_ri_debug << " -> Track " << tkRels.tkKey << " " << tkHypo << endmsg;
      
      // Loop over the segments for this track
      for ( const auto & iSeg : tkRels.segmentIndices )
      {
        // the segment data tuple
        const auto && segData = segRange[iSeg];
        // extract from the tuple
        const auto & segment  = std::get<0>(segData);
        const auto & geomEffs = std::get<1>(segData);
        const auto & detYield = std::get<2>(segData);
        
        // which RICH
        const auto rich = segment.rich();

        //_ri_debug << "  -> Segment " << iSeg << " " << rich
        //          << " " << segment.radiator()
        //          << " DetPhots=" << detYield[tkHypo] << endmsg;
        
        // Loop over the per PD geom. effs. for this track hypo
        for ( const auto & PD : geomEffs[tkHypo] )
        {
          // expected signal for this PD
          const auto sig = detYield[tkHypo] * PD.second; 
          // PD ID
          const auto & pdID = PD.first;
          // index
          const auto index = pdIndex(pdID,rich);
          // panel data vector
          auto & dataV = (pdData[rich])[pdID.panel()];
          // Update the PD data map with this value
          if ( index < dataV.size() )
          {
            auto & data = dataV[index];
            data.expSignal += sig;
            //_ri_debug << "   -> " << LHCb::RichSmartID(PD.first) << " DetPhots=" << sig << endmsg;
          }
          else
          {
            Error( "Track : PD index " + std::to_string(index) + " out of range !!" ).ignore();
          }
        }        
      }
    }
  }

  // -----------------------------------------------------------
  // Now compute the background terms
  // -----------------------------------------------------------
  
  // Obtain background term PD by PD
  for ( const auto rich : Rich::detectors() )
  {
    //_ri_debug << "Computing PD backgrounds in " << rich << endmsg;
    
    unsigned int iter = 1;
    bool cont = true;
    FP rnorm = 0.0;
    while ( cont )
    {
      //_ri_debug << " -> Iteration " << iter << endmsg;
      
      unsigned int nBelow(0), nAbove(0);
      FP tBelow = 0.0;
      // loop over panels
      for ( auto& panelData : pdData[rich] )
      {
        // Loop over PD in this panel
        for ( auto& iPD : panelData )
        {
          // Only process PDs with observed hits
          if ( iPD.obsSignal > 0 )
          {
            
            // The background for this PD
            auto & bkg = iPD.expBackgrd;
            
            if ( 1 == iter )
            {
              // First iteration, just set background for this PD to the difference
              // between the observed and and expected number of hits in the PD
              //_ri_debug << "  -> PD " << pd << " obs. = " << obs << " exp. = " << exp << endmsg;
              bkg = static_cast<FP>(iPD.obsSignal) - iPD.expSignal;
            }
            else
            {
              // For additional interations apply the normalisation factor
              bkg = ( bkg > 0 ? bkg-rnorm : 0 );
            }
            
            if ( bkg < 0.0 )
            {
              // Count the number of PDs below expectation for this iteration
              ++nBelow;
              // save the total amount below expectation
              tBelow += fabs( bkg );
            }
            else if ( bkg > 0.0 )
            {
              // count the number of PDs above expectation
              ++nAbove;
            }
            
          } // with observed hits
          
        } // end loop over signal PDs
      } // end loop over panels
      
      //_ri_debug << "  -> Above = " << nAbove << " Below = " << nBelow << endmsg;
      
      if ( nBelow > 0 && nAbove > 0 )
      {
        // we have some PDs above and below expectation
        // calculate the amount of signal below per above PD
        rnorm = tBelow / ( static_cast<FP>(nAbove) );
        //_ri_debug << "   -> Correction factor per PD above = " << rnorm << endmsg;
      }
      else
      {
        //_ri_debug << "  -> Aborting iterations" << endmsg;
        cont = false;
      }
      
      // Final protection against infinite loops
      if ( ++iter > m_maxBkgIterations ) { cont = false; }

    } // while loop

  } // end rich loop

  // -----------------------------------------------------------
  // Normalise the PD backgrounds
  // -----------------------------------------------------------

  // Loop over the RICH data maps
  for ( const auto rich : Rich::detectors() )
  {
    // loop over panels
    for ( auto & panel : pdData[rich] )
    {
      // Loop over the PD data objects
      for ( auto & pd : panel )
      {
        if ( pd.obsSignal > 0 )
        {
          // normalise background for this PD
          pd.expBackgrd  = ( pd.expBackgrd > 0 ? pd.expBackgrd / pd.effNumPixs : 0 );
          // rescale by the overall weight factor for the RICH this PD is in
          pd.expBackgrd *= m_bkgWeight[rich];
          // apply min and max pixel background limits
          pd.expBackgrd  = std::max( pd.expBackgrd, m_minPixBkg[rich] );
          pd.expBackgrd  = std::min( pd.expBackgrd, m_maxPixBkg[rich] );
        }
      }
    }
  }

  // -----------------------------------------------------------
  // Fill the background values into the output data structure
  // -----------------------------------------------------------

  for ( auto && pixData : Ranges::Zip(pixels,backgrounds,indices) )
  {
    // get the cluster and background value for this cluster
    const auto & pixel = std::get<0>(pixData);
    auto       & bkg   = std::get<1>(pixData);
    const auto & index = std::get<2>(pixData);

    // RICH flags
    const auto rich = pixel.rich();
    const auto side = pixel.side();

    // get the panel data vector
    auto & dataV = (pdData[rich])[side];

    // Loop over the scalar entries for this SIMD pixel
    for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i )
    {

      // index for this PD
      const auto indx = index[i];

      // get the data object for this PD
      if ( indx < dataV.size() )
      {
        // data for this PD
        auto & data = dataV[indx];
        // update the pixel background
        bkg[i] = data.expBackgrd;
        _ri_verbo << rich << " Pix Bkg " << bkg[i] << endmsg;
      }
      else
      {
        Error( "Bkg : PD index " + std::to_string(indx) + " out of range !!" ).ignore();
        bkg[i] = 0;
      }

    } // scalar loop

  } // pixel loop

  // -----------------------------------------------------------
  // All done, so return
  // -----------------------------------------------------------

  return backgrounds;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPixelBackgroundsEstiAvHPD  )

//=============================================================================
