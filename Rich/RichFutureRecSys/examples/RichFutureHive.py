###############################################################
# Job options file
#==============================================================

from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import LHCbApp
from GaudiConfig.ControlFlow import seq

# Enable Hive !!
#==============================================================

LHCbApp(EnableHive=True)

#from Configurables import AlgResourcePool, HiveWhiteBoard, ForwardSchedulerSvc
#AlgResourcePool().OutputLevel = VERBOSE

#scheduler = ForwardSchedulerSvc()
#scheduler.OutputLevel = VERBOSE
#whiteboard = HiveWhiteBoard("EventDataSvc")

#scheduler.MaxAlgosInFlight = 20
#scheduler.ThreadPoolSize   = 30
#whiteboard.EventSlots      = 6

#==============================================================
