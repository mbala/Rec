from Gaudi.Configuration import *
from GaudiConf import IOHelper

import glob

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/Collision16/LHCb/Raw/",    # Cambridge
    "/usera/jonesc/NFS/data/Collision15/LHCb/Raw/",    # Cambridge
    "/usera/jonesc/NFS/data/Collision12/LHCb/Raw/",    # Cambridge
    "/usera/jonesc/NFS/data/Collision11_25/LHCb/Raw/", # Cambridge
    "/home/chris/LHCb/Data/Collision16/LHCb/Raw/"      # CRJ's CernVM
    ]

print "Data Files :-"
data = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*/*.raw"))
    print("\n".join(files))
    data += [ "DATAFILE='"+file+"'" for file in files ]

IOHelper('MDF').inputFiles( data, clear=True )
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

