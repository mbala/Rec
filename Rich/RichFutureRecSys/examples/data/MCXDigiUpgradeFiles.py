from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/data/MC/Upgrade/Sim09c-Up02/30000000/XDIGI/", 
    # CRJ's CernVM
    "/home/chris/LHCb/Data/MC/Upgrade/Sim09c-Up02/30000000/XDIGI/"  
    ]

print "Data Files :-"
data = [ ]
for path in searchPaths :
    files = sorted(glob.glob(path+"*.xdigi"))
    data += [ "'PFN:"+file for file in files ]
    for f in files : print f

IOHelper('ROOT').inputFiles( data, clear=True )
FileCatalog().Catalogs = [ 'xmlcatalog_file:out.xml' ]

from Configurables import Brunel, LHCbApp

Brunel().InputType  = 'DIGI'
Brunel().DataType   = "Upgrade"
Brunel().Simulation = True
Brunel().WithMC     = True 
LHCbApp().DDDBtag   = "dddb-20171010"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"
