
#pragma once

// STL
#include <tuple>
#include <array>

// Gaudi
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/PhysicalConstants.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

namespace Rich
{
  namespace Future
  {    
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;
      
      namespace
      {
        /// The output data
        using OutData = std::tuple< GeomEffs::Vector,
                                    GeomEffsPerPDVector,
                                    SegmentPhotonFlags::Vector >;
      }

      /** @class GeomEffCKMassRings RichRichGeomEffCKMassRings.h
       *
       *  Computes the geometrical efficiencies for a set of track segments.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class GeomEffCKMassRings final : 
          public MultiTransformer< OutData( const LHCb::RichTrackSegment::Vector&,
                                            const CherenkovAngles::Vector&,
                                            const MassHypoRingsVector& ), 
                                 Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        GeomEffCKMassRings( const std::string& name, 
                            ISvcLocator* pSvcLocator );
        
      public:

        /// Algorithm execution via transform
        OutData operator()( const LHCb::RichTrackSegment::Vector& segments,
                            const CherenkovAngles::Vector& ckAngles,
                            const MassHypoRingsVector& massRings ) const override;

      };
      
    }
  }
}
