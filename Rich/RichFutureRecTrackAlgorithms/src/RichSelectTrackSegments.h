
#pragma once

// Gaudi
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

// Rec event model
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"

namespace Rich
{
  namespace Future
  {    
    namespace Rec
    {

      // Use the functional framework
      using namespace Gaudi::Functional;

     namespace
      {
        /// The output data
        using OutData = Relations::TrackToSegments::Vector;
      }

      /** @class SelectTrackSegments RichSelectTrackSegments.h
       *
       *  Filters the initial track to segment relations to select only
       *  segments with RICH information.
       *
       *  @author Chris Jones
       *  @date   2016-10-31
       */

      class SelectTrackSegments final : 
        public Transformer< OutData( const Relations::TrackToSegments::Vector&,
                                     const GeomEffs::Vector& ), 
                            Traits::BaseClass_t<AlgBase> >
      {

      public:

        /// Standard constructor
        SelectTrackSegments( const std::string& name, 
                             ISvcLocator* pSvcLocator );

      public:

        /// Algorithm execution via transform
        OutData operator()( const Relations::TrackToSegments::Vector& tkToSegRels,
                            const GeomEffs::Vector& geomEffs ) const override;

      };

    }
  }
}
