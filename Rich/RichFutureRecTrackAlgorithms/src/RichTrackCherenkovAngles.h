
#pragma once

// STL
#include <cmath>
#include <algorithm>
#include <iomanip>

// Gaudi Functional
#include "GaudiAlg/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"
#include "RichUtils/FastMaths.h"

// Interfaces
#include "RichInterfaces/IRichRefractiveIndex.h"

namespace Rich
{
  namespace Future
  {

    // Use the functional framework
    using namespace Gaudi::Functional;

    namespace Rec
    {

      /** @class TrackCherenkovAnglesBase RichTrackCherenkovAngles.h
       *
       *  Computes the expected Cherenkov angles for the given track
       *  segments and photon spectra data.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class TrackCherenkovAnglesBase : public AlgBase
      {

      public:

        /// Standard constructor
        TrackCherenkovAnglesBase( const std::string& name,
                                  ISvcLocator* pSvcLocator )
          : AlgBase ( name, pSvcLocator ) { }

      protected:

        /// Algorithm execution
        CherenkovAngles::Vector run( const LHCb::RichTrackSegment::Vector& segments,
                                     const PhotonSpectra::Vector& tkSpectra,
                                     const PhotonYields::Vector& tkYields ) const;

      private:

        /// Pointer to general refractive index tool
        ToolHandle<const IRefractiveIndex> m_refIndex
        { this, "RefIndex", "Rich::Future::TabulatedRefractiveIndex/RefIndex" };

      };

      /** @class TrackEmittedCherenkovAngles RichTrackCherenkovAngles.h
       *
       *  Functional implementation using emitted photon spectra.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class TrackEmittedCherenkovAngles final :
        public Transformer< CherenkovAngles::Vector( const LHCb::RichTrackSegment::Vector&,
                                                     const PhotonSpectra::Vector&,
                                                     const PhotonYields::Vector& ),
                            Traits::BaseClass_t<TrackCherenkovAnglesBase> >
      {
      public:
        /// Constructor
        TrackEmittedCherenkovAngles( const std::string& name,
                                     ISvcLocator* pSvcLocator )
          : Transformer ( name, pSvcLocator,
                          { KeyValue{ "TrackSegmentsLocation",   LHCb::RichTrackSegmentLocation::Default },
                            KeyValue{ "EmittedPhotonSpectraLocation",   PhotonSpectraLocation::Emitted },
                            KeyValue{ "EmittedPhotonYieldLocation",     PhotonYieldsLocation::Emitted } },
                          { KeyValue{ "EmittedCherenkovAnglesLocation", CherenkovAnglesLocation::Emitted } } )
        {
          //setProperty( "OutputLevel", MSG::VERBOSE );
        }
        /// Algorithm execution via transform
        CherenkovAngles::Vector
          operator()( const LHCb::RichTrackSegment::Vector& segments,
                      const PhotonSpectra::Vector& tkSpectra,
                      const PhotonYields::Vector& tkYields ) const override
        { return run(segments,tkSpectra,tkYields); }
      };

      /** @class TrackSignalCherenkovAngles RichTrackCherenkovAngles.h
       *
       *  Functional implementation using emitted photon spectra.
       *
       *  @author Chris Jones
       *  @date   2016-09-30
       */
      class TrackSignalCherenkovAngles final :
        public Transformer< CherenkovAngles::Vector( const LHCb::RichTrackSegment::Vector&,
                                                     const PhotonSpectra::Vector&,
                                                     const PhotonYields::Vector& ),
                            Traits::BaseClass_t<TrackCherenkovAnglesBase> >
      {
      public:
        /// Constructor
        TrackSignalCherenkovAngles( const std::string& name,
                                    ISvcLocator* pSvcLocator )
          : Transformer ( name, pSvcLocator,
                          { KeyValue{ "TrackSegmentsLocation",   LHCb::RichTrackSegmentLocation::Default },
                            KeyValue{ "SignalPhotonSpectraLocation",   PhotonSpectraLocation::Signal },
                            KeyValue{ "SignalPhotonYieldLocation",     PhotonYieldsLocation::Signal } },
                          { KeyValue{ "SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal } } )
        {
          //setProperty( "OutputLevel", MSG::VERBOSE );
        }
        /// Algorithm execution via transform
        CherenkovAngles::Vector
          operator()( const LHCb::RichTrackSegment::Vector& segments,
                      const PhotonSpectra::Vector& tkSpectra,
                      const PhotonYields::Vector& tkYields ) const override
        { return run(segments,tkSpectra,tkYields); }
      };

    }
  }
}
