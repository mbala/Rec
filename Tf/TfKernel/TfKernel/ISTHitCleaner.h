#ifndef TFTOOLS_ISTHITCLEANER_H
#define TFTOOLS_ISTHITCLEANER_H 1

// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// From Tf
#include "TfKernel/STHit.h"
//#include "TfKernel/Region.h"


namespace Tf
{

  /** @class ISTHitCleaner ISTHitCleaner.h TfKernel/ISTHitCleaner.h
   *
   *  Interface to tool that 'cleans' ranges of STHits
   *
   *  @author S. Hansmann-Menzemer, W. Hulsbergen, C. Jones, K. Rinnert
   *  @date   2007-07-20
   */
  struct ISTHitCleaner : extend_interfaces<IAlgTool>
  {
    /// Interface ID
    DeclareInterfaceID( ISTHitCleaner, 2, 0 );


    /** Clean the given range of ST hits
     *  @param[in]  input The range of ST hits to clean
     *  @param[out] output The selected hits
     *  @return The number of removed hits
     */
    template<class INPUTDATA >
    inline STHits cleanHits( const INPUTDATA & input ) const
    {
      return cleanHits ( input.begin(), input.end() );
    }

    /** Clean the given range of hits
     *  @param[in] begin Iterator to the start of a range of ST hits
     *  @param[in] end   Iterator to the start of a range of ST hits
     *  @param[out] output The selected hits   
     *  @return The number of removed hits
     */
    virtual STHits cleanHits( const STHits::const_iterator begin,
                              const STHits::const_iterator end ) const = 0;

  };

}

#endif // TFTOOLS_ISTHITCLEANER_H
