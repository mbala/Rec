from Gaudi.Configuration import GaudiSequencer
from TrackSys.Configuration import *
from GaudiKernel.SystemOfUnits import mm


def PrUpgradeChecking(defTracks={}, tracksToConvert = [], defTracksConverted = {}):
    # match hits and tracks
    log.warning("Run upgrade checkers.")

    from Configurables import (UnpackMCParticle, UnpackMCVertex,
                               PrLHCbID2MCParticle)
    # Check if VP is part of the list of detectors.
    from Configurables import LHCbApp, VPCluster2MCParticleLinker, VPFullCluster2MCParticleLinker, VPClusFull
    withVP = False
    if hasattr(LHCbApp(), "Detectors"):
        if LHCbApp().isPropertySet("Detectors"):
            if 'VP' in LHCbApp().upgradeDetectors():
                withVP = True
    trackTypes = TrackSys().getProp("TrackTypes")

    if "Truth" in trackTypes:
        truthSeq = GaudiSequencer("RecoTruthSeq")
        truthSeq.Members = [UnpackMCParticle(), UnpackMCVertex()]
        if withVP:
            truthSeq.Members += [VPCluster2MCParticleLinker()]
        truthSeq.Members += [PrLHCbID2MCParticle()]
    else:
        if withVP:
            if TrackSys().getProp("VPLinkingOfflineClusters"):
                #re-run VPClustering for FULL Velo cluster [ with info of pixels to clusters] and its own linker algorithm
                GaudiSequencer("MCLinksTrSeq").Members = [ 
                    VPClusFull() , VPFullCluster2MCParticleLinker(), PrLHCbID2MCParticle() 
                ]
            else:
                GaudiSequencer("MCLinksTrSeq").Members = [
                    VPCluster2MCParticleLinker(), PrLHCbID2MCParticle()]
        else:
            GaudiSequencer("MCLinksTrSeq").Members = [PrLHCbID2MCParticle()]
        
    newDefTracks = defTracks
    #Re-create a keyed container for a sub-set of tracks
    from Configurables import PrTrackAssociator, PrTrackConverter
    for tracktype in defTracks:
        seq = GaudiSequencer(tracktype+"Checker")
        if tracktype in tracksToConvert:
            log.warning("Pre-appending "+tracktype+" track copy in TES from vector<Track> to KeyedContainer to allow truth matching, this is an hack, not a final solution")
            trconverter = PrTrackConverter(tracktype+"Converter")
            trconverter.InputTracksLocation   =  defTracks[tracktype]["Location"]
            trconverter.OutKeyedTrackLocation =  defTracksConverted[tracktype]["Location"]
            seq.Members+= [ trconverter]
            #insert in the sequence the converter for the tracks listed in UpgrateTracksToConvert
            trassociator                 = PrTrackAssociator(tracktype+"Associator")
            trassociator.SingleContainer = defTracksConverted[tracktype]["Location"]
            #update the track locations for the PrChecker
            seq.Members+= [ trassociator]
            GaudiSequencer("MCLinksTrSeq").Members+= [seq]
            newDefTracks[tracktype]["Location"] = defTracksConverted[tracktype]["Location"]
    
    from Configurables import LHCb__Converters__RecVertex__v1__fromVectorLHCbRecVertex as FromVectorLHCbRecVertex
    vertexConverter = FromVectorLHCbRecVertex("VertexConverter")
    vertexConverter.InputVerticesName = "Rec/Vertex/Vector/Primary"
    vertexConverter.InputTracksName = defTracksConverted["Velo"]["Location"]
    vertexConverter.OutputVerticesName = "Rec/Vertex/Primary"
    GaudiSequencer("MCLinksTrSeq").Members+= [ vertexConverter ]

    #PrTrackAssociator uses by default whatever it finds in RootOfContainers
    GaudiSequencer("MCLinksTrSeq").Members+= [ PrTrackAssociator("PrTrackAssociator")]
    # PrChecker2
    from Configurables import PrChecker2
    from Configurables import LoKi__Hybrid__MCTool
    MCHybridFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
    MCHybridFactory.Modules = ["LoKiMC.decorators"]
    # FAST
    checker2 = PrChecker2("PrChecker2Fast", 
                          VeloTracks="", 
                          MatchTracks="",
                          SeedTracks="", 
                          DownTracks="", 
                          UpTracks="",
                          TriggerNumbers=True, 
                          BestTracks="")
    PrChecker2("PrChecker2Fast").addTool(MCHybridFactory)
    PrChecker2("PrChecker2Fast").Upgrade = True
    PrChecker2("PrChecker2Fast").VeloTracks = newDefTracks["Velo"]["Location"]
    #PrChecker2("PrChecker2Fast").NewTracks  = defTracks["VeloFitted"]["Location"]
    PrChecker2("PrChecker2Fast").ForwardTracks = newDefTracks["ForwardFast"]["Location"]
    PrChecker2("PrChecker2Fast").UpTracks      = newDefTracks["Upstream"]["Location"]
    PrChecker2("PrChecker2Fast").Eta25Cut = True
    PrChecker2("PrChecker2Fast").GhostProbCut = 0.9
    GaudiSequencer("CheckPatSeq").Members += [checker2]

    # PrChecker2("PrChecker2Fast").WriteTexOutput = True
    # PrChecker2("PrChecker2Fast").TexOutputFolder = "texfilesFast/"
    # PrChecker2("PrChecker2Fast").TexOutputName = "PrChecker2Fast"
    # BEST
    GaudiSequencer("CheckPatSeq").Members += [PrChecker2("PrChecker2")]
    PrChecker2("PrChecker2").addTool(MCHybridFactory)
    PrChecker2("PrChecker2").Upgrade = True
    forwardTracks = newDefTracks["ForwardBest"]["Location"]
    PrChecker2("PrChecker2").ForwardTracks = forwardTracks
    #PrChecker2("PrChecker2").UpTracks      = newDefTracks["Upstream"]["Location"]
    PrChecker2("PrChecker2").Eta25Cut = True
    PrChecker2("PrChecker2").GhostProbCut = 0.9
    # PrChecker2("PrChecker2").WriteTexOutput = True
    # PrChecker2("PrChecker2").TexOutputFolder = "texfiles/"
    # PrChecker2("PrChecker2").TexOutputName = "PrChecker2"

    #Track resolution checker fast stage
    from Configurables import TrackResChecker
    GaudiSequencer("CheckPatSeq").Members  += [ TrackResChecker("TrackResCheckerFast")];
    TrackResChecker("TrackResCheckerFast").HistoPrint = False
    TrackResChecker("TrackResCheckerFast").TracksInContainer = defTracksConverted["ForwardFastFitted"]["Location"]
    
    from Configurables import PrimaryVertexChecker
    PVCheck = PrimaryVertexChecker("PVChecker")
    if not PVCheck.isPropertySet('inputVerticesName'):
        PVCheck.inputVerticesName = "Rec/Vertex/Primary"
    if not PVCheck.isPropertySet('matchByTracks'):
        PVCheck.matchByTracks = False
    if not PVCheck.isPropertySet('nTracksToBeRecble'):
        PVCheck.nTracksToBeRecble = 4
    if not PVCheck.isPropertySet('inputTracksName'):
        PVCheck.inputTracksName = defTracksConverted["VeloFitted"]["Location"]
    GaudiSequencer("CheckPatSeq").Members += [PVCheck]
    
