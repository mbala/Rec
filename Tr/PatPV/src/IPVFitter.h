#ifndef IPVFITTER_H
#define IPVFITTER_H 1

// from STL
#include <vector>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/Point3DTypes.h"

namespace LHCb {
  class Track;
  class RecVertex;
}

struct IPVFitter : extend_interfaces<IAlgTool> {
  DeclareInterfaceID(IPVFitter,2,0);
  virtual StatusCode fitVertex(const Gaudi::XYZPoint& seedPoint,
			                   const std::vector<const LHCb::Track*>& tracks,
                               LHCb::RecVertex& vtx,
                               std::vector<const LHCb::Track*>& tracks2remove) const = 0;

};
#endif // IPVFITTER_H
