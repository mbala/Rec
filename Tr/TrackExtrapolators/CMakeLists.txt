################################################################################
# Package: TrackExtrapolators
################################################################################
gaudi_subdir(TrackExtrapolators v2r42)

gaudi_depends_on_subdirs(Det/DetDesc
                         Event/TrackEvent
                         GaudiAlg
                         Kernel/PartProp
                         Tr/TrackInterfaces
                         Tr/TrackKernel)

find_package(GSL)
find_package(Eigen)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS})

gaudi_add_module(TrackExtrapolators
                 src/*.cpp
                 INCLUDE_DIRS GSL Eigen Tr/TrackInterfaces
                 LINK_LIBRARIES GSL DetDescLib TrackEvent GaudiAlgLib TrackKernel PartPropLib)
