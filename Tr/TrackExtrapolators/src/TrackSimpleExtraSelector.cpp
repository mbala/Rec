#include "TrackSimpleExtraSelector.h"

// from TrackInterfaces
#include "TrackInterfaces/ITrackExtrapolator.h"

DECLARE_COMPONENT( TrackSimpleExtraSelector )


//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackSimpleExtraSelector::initialize()
{
  // initialize
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);
  m_extrapolator= tool<ITrackExtrapolator>( m_extraName, m_extraName, this );
  return StatusCode::SUCCESS;
}

//=============================================================================
//
//=============================================================================
const ITrackExtrapolator*
TrackSimpleExtraSelector::select( double, double) const
{
  return m_extrapolator;
}

//=============================================================================
