// Include files

// from STDet
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"
#include "STDet/DeSTSensor.h"

// from Kernel
#include "TrackInterfaces/ISTClusterPosition.h"

// local
#include "Event/UTLiteMeasurement.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : UTLiteMeasurement
//
// 2005-04-07 : Jose Hernando, Eduardo Rodrigues
// Author: Rutger van der Eijk
// Created: 07-04-1999
//-----------------------------------------------------------------------------

/// Standard constructor, initializes variables
UTLiteMeasurement::UTLiteMeasurement( const UT::Hit& hit,
                                      const DeSTDetector& geom,
                                      const ISTClusterPosition& stClusPosTool,
                                      const LHCb::StateVector& /*refVector*/)
  : Measurement(Measurement::Type::UTLite, hit.lhcbID(), 0), m_hit(hit)
{
  this->init( geom, stClusPosTool );
}

void UTLiteMeasurement::init( const DeSTDetector& geom,
                              const ISTClusterPosition& stClusPosTool)
{
  // Fill the data members
  const DeSTSector* stSector = geom.findSector( m_hit.chanID() );
  m_detectorElement = stSector ;

  m_errMeasure = stClusPosTool.error(m_hit.pseudoSize())*stSector->pitch();
  m_trajectory = stSector->trajectory( m_hit.chanID(), m_hit.fracStrip()) ;
  m_z = stSector->globalCentre().z();

  // get the best sensor to and go local
  // this is the only way to ensure we get inside a
  // sensor, and not into a bondgap
  m_measure = stSector->middleSensor()->localU( m_hit.strip() )
              + ( m_hit.fracStrip() * stSector -> pitch() );

}
