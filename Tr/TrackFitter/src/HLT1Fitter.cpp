#ifdef _WIN32
#pragma warning (disable : 4355) // This used in initializer list, needed for ToolHandles
#endif

#include <tuple>
#include "HLT1Fitter.h"
#include "Kernel/ParticleID.h"
#include <range/v3/utility/any.hpp>
#include "Event/OTMeasurement.h"
#include "Event/ChiSquare.h"
#include "TrackKernel/TrackTraj.h"
#include "Event/StateParameters.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackKernel/StateZTraj.h"
#include <boost/range/adaptor/reversed.hpp>

DECLARE_COMPONENT(HLT1Fitter)

/**
 * Default Constructor
 * Only defines deafult input and output paths in TES
 */
HLT1Fitter::HLT1Fitter(const std::string& name,
                       ISvcLocator* pSvcLocator) :
Transformer(name, pSvcLocator,
            KeyValue{"TracksInContainer", LHCb::TrackLocation::Default},
            KeyValue{"TracksOutContainer", LHCb::TrackLocation::Default}) {
  declareProperty("MeasProvider", m_measProvider);
  declareProperty("Extrapolator", m_extrapolator);
  declareProperty("VeloExtrapolator", m_veloExtrapolator);
  declareProperty("MaterialLocator", m_materialLocator);
}

/**
 * operator () : main method orchestrating the work
 */
std::vector<LHCb::Track> HLT1Fitter::operator()(const std::vector<LHCb::Track>& tracksCont) const {

  std::vector<LHCb::Track> tracksNewCont;
  tracksNewCont.reserve(tracksCont.size());
  ranges::v3::any cache = m_materialLocator->createCache();
  unsigned int nFitFail = 0;
  unsigned int nBadInput = 0;

  // Loop over the tracks and fit them
  for (const auto& iTrack : tracksCont) {
    // If needed make a new track keeping the same key
    tracksNewCont.emplace_back(iTrack, iTrack.key());
    LHCb::Track& track = tracksNewCont.back();
    if (track.nStates()==0 || track.checkFlag(LHCb::Track::Invalid)) {
      track.setFlag(LHCb::Track::Invalid, true);
      // don't put failures on the output container. this is how they want it in HLT.
      ++nBadInput;
    } else {
      double qopBefore = track.firstState().qOverP();
      try {
        unsigned int nbOutliers = fit(track, cache);
        std::string prefix = Gaudi::Utils::toString(track.type());
        // Update counters
        if (track.checkFlag(LHCb::Track::Backward)) prefix += "Backward";
        prefix += '.';
        if (track.nDoF()>0) {
          double chisqprob = track.probChi2();
          counter(prefix + "chisqprobSum") += chisqprob;
          counter(prefix + "badChisq") += bool(chisqprob<0.01);
        }
        counter(prefix + "flipCharge") += bool(qopBefore * track.firstState().qOverP() <0);
        counter(prefix + "numOutliers") += nbOutliers;
        // Add the track to the new Tracks container if it passes the chi2-cut
        if (m_maxChi2DoF > track.chi2PerDoF()) {
        } else {
          tracksNewCont.pop_back();
          counter(prefix + "RejectedChisqCut") += 1;
        }
      } catch (const StatusCode &sc) {
        track.setFlag(LHCb::Track::Invalid, true); // this set a flag in a track that is about to be leaked...
        ++nFitFail;
      }
    }
  } // loop over input Tracks

  // Update counters
  unsigned int nTracks = tracksCont.size();
  counter("nTracks") += nTracks;
  counter("nFitted") += (nTracks - nFitFail - nBadInput);
  counter("nBadInput") += nBadInput;

  return tracksNewCont;
}

/// finalize : reporting performance
StatusCode HLT1Fitter::finalize() {
  if (msgLevel(MSG::INFO)) {
    float perf = 0.;
    double nTracks = counter("nTracks").flag();
    if (nTracks > 1e-3) {
      perf = float(100.0*counter("nFitted").flag() / nTracks);
    }
    info() << "  Fitting performance   : " << format( " %7.2f %%", perf ) << endmsg;
  }
  // must be called after all other actions
  return Transformer::finalize();
}

/**
 * This routine calculates the chisquare contributions from
 * different segments of the track. It uses the 'delta-chisquare'
 * contributions from the bi-directional kalman fit. Summing these
 * leads to a real chisquare only if the contributions are
 * uncorrelated. For a Velo-TT-T track you can then calculate:
 * - the chisuare of the T segment and the T-TT segment by using the
 * 'upstream' contributions
 * - the chisquare of the Velo segment and the Velo-TT segment by
 * using the 'downstream' contributions
 * Note that you cannot calculate the contribution of the TT segment
 * seperately (unless there are no T or no Velo hits). Also, if
 * there are Muon hits, you cannot calculate the T station part, so
 * for now this only works for tracks without muon hits.
 * @return a tuple with DownStream, Velo and Match chi2 in this order
 */
static inline std::array<LHCb::ChiSquare, 3>
computeChiSquares(const std::vector<LHCb::SebFitNode> &nodes) {
  if (nodes.empty()) {
    return {LHCb::ChiSquare(), LHCb::ChiSquare(), LHCb::ChiSquare()};
  } else {
    LHCb::ChiSquare chi2Downstream, chi2Velo, chi2Muon, chi2Upstream, chi2;
    auto firstVelo = nodes.end();
    auto lastVelo = nodes.end();
    auto firstTT = nodes.end();
    auto lastTT = nodes.end();
    auto firstT = nodes.end();
    auto lastT = nodes.end();
    auto firstMuon = nodes.end();
    auto lastMuon = nodes.end();
    for (auto it = nodes.cbegin(); it != nodes.cend(); it++) {
      if (it->type() == LHCb::SebFitNode::HitOnTrack) {
        switch (it->measurement().type()) {
        case LHCb::Measurement::VP:
        case LHCb::Measurement::VeloR:
        case LHCb::Measurement::VeloPhi:
        case LHCb::Measurement::VeloLiteR:
        case LHCb::Measurement::VeloLitePhi:
        case LHCb::Measurement::Origin:
          if (firstVelo==nodes.end()) {
            firstVelo = it;
          }
          lastVelo = it;
          break;
        case LHCb::Measurement::TT:
        case LHCb::Measurement::TTLite:
        case LHCb::Measurement::UT:
        case LHCb::Measurement::UTLite:
          if (firstTT==nodes.end()) {
            firstTT = it;
          }
          lastTT = it;
          break;
        case LHCb::Measurement::OT:
        case LHCb::Measurement::IT:
        case LHCb::Measurement::ITLite:
        case LHCb::Measurement::FT:
          if (firstT==nodes.end()) {
            firstT = it;
          }
          lastT = it;
          break;
        case LHCb::Measurement::Muon:
          if (firstMuon==nodes.end()) {
            firstMuon = it;
          }
          lastMuon = it;
          break;
        case LHCb::Measurement::Unknown:
        case LHCb::Measurement::Calo:
        default:
          break;
        }
      }
    }
    bool upstream = nodes.front().z() > nodes.back().z();
    if (firstMuon != nodes.end()) {
      chi2Muon = upstream ? lastMuon->totalChi2(LHCb::SebFitNode::Forward) : firstMuon->totalChi2(LHCb::SebFitNode::Backward);
    } else {
      chi2Muon = LHCb::ChiSquare();
    }
    if (firstT != nodes.end()) {
      chi2Downstream = upstream ? lastT->totalChi2(LHCb::SebFitNode::Forward) : firstT->totalChi2(LHCb::SebFitNode::Backward);
    } else {
      chi2Downstream = chi2Muon;
    }
    if (firstVelo != nodes.end()) {
      chi2Velo = upstream ? firstVelo->totalChi2(LHCb::SebFitNode::Backward) : lastVelo->totalChi2(LHCb::SebFitNode::Forward);
    } else {
      chi2Velo = LHCb::ChiSquare();
    }

    if (firstTT != nodes.end()) {
      chi2Upstream = upstream ? firstTT->totalChi2(LHCb::SebFitNode::Backward) : lastTT->totalChi2(LHCb::SebFitNode::Forward);
    } else {
      chi2Upstream = chi2Velo;
    }
    const LHCb::ChiSquare& chi2A = nodes.front().totalChi2(LHCb::SebFitNode::Backward);
    const LHCb::ChiSquare& chi2B = nodes.back().totalChi2(LHCb::SebFitNode::Forward);
    chi2 = (chi2A.chi2() > chi2B.chi2()) ? chi2A : chi2B;
    return {chi2Downstream, chi2Velo, chi2-chi2Upstream-chi2Downstream};
  }
}

static inline unsigned int countOTMesurements(const std::vector<LHCb::Measurement*> &measurements) {
  return std::count_if(measurements.begin(), measurements.end(),
                       [&](const LHCb::Measurement* m) {
                         return m->type() == LHCb::Measurement::OT;
                       });
}

static inline unsigned int computeActiveOTTime(const std::vector<LHCb::SebFitNode> &nodes) {
  return std::count_if
    (nodes.cbegin(), nodes.cend(),
     [](const LHCb::SebFitNode& node) {
      if (node.type() != LHCb::SebFitNode::HitOnTrack ||
          node.measurement().type() != LHCb::Measurement::OT) return false;
      const auto& ot = static_cast<const LHCb::OTMeasurement&>(node.measurement());
      return ot.driftTimeStrategy() == LHCb::OTMeasurement::FitDistance ||
        ot.driftTimeStrategy() == LHCb::OTMeasurement::FitTime;
    });
}

/**
 * Add info from fit as extrainfo to track
 */
static inline void fillExtraInfo(LHCb::Track& track,
                                 const std::vector<LHCb::SebFitNode>& nodes,
                                 const std::vector<LHCb::Measurement*>& measurements,
                                 const LHCb::Track::Types trackType) {
  // Clean up the track info
  track.eraseInfo(LHCb::Track::FitVeloChi2);
  track.eraseInfo(LHCb::Track::FitVeloNDoF);
  track.eraseInfo(LHCb::Track::FitTChi2);
  track.eraseInfo(LHCb::Track::FitTNDoF);
  track.eraseInfo(LHCb::Track::FitMatchChi2);
  track.eraseInfo(LHCb::Track::FitFracUsedOTTimes);

  // get chi2s in this order : downstream, Velo, Match
  std::array<LHCb::ChiSquare, 3> chi2s = computeChiSquares(nodes);

  // Case of T track
  if (trackType == LHCb::Track::Types::Ttrack ||
      trackType == LHCb::Track::Types::Downstream ||
      trackType == LHCb::Track::Types::Long) {
    // Downstream chi2
    track.addInfo(LHCb::Track::FitTChi2, chi2s[0].chi2());
    track.addInfo(LHCb::Track::FitTNDoF, chi2s[0].nDoF());
    unsigned int nOTMeas = countOTMesurements(measurements);
    if (nOTMeas > 0) {
      unsigned int actirOTTime = computeActiveOTTime(nodes);
      track.addInfo(LHCb::Track::FitFracUsedOTTimes, actirOTTime / double(nOTMeas));
    }
  }

  // Case of Velo track
  if (trackType == LHCb::Track::Types::Velo ||
      trackType == LHCb::Track::Types::VeloR ||
      trackType == LHCb::Track::Types::Upstream ||
      trackType == LHCb::Track::Types::Long) {
    // Velo chi2
    track.addInfo(LHCb::Track::FitVeloChi2, chi2s[1].chi2());
    track.addInfo(LHCb::Track::FitVeloNDoF, chi2s[1].nDoF());
    // case of Velo and T track, i.e Long track
    if (trackType == LHCb::Track::Types::Long) {
      // Match chi2
      track.addInfo(LHCb::Track::FitMatchChi2, chi2s[2].chi2());
    }
  }
}

unsigned int HLT1Fitter::fit(LHCb::Track& track, ranges::v3::any& accelCache) const {
  // any track that doesnt make it to the end is failed
  track.setFitStatus(LHCb::Track::FitFailed);

  // Store results of the Kalman fit
  std::vector<LHCb::SebFitNode> nodes;
  std::vector<LHCb::Measurement*> measurements;
  int nTrackParameters;

  // Make the nodes and measurements
  try {
    nodes = makeNodes(track, accelCache, measurements);
    // create the transport of the reference (after the noise, because it uses energy loss)
    nTrackParameters = updateTransport(nodes, track.type());
  } catch (const StatusCode& sc) {
    debug() << "unable to make nodes from the measurements" << endmsg;
    throw sc;
  }

  // create a covariance matrix to seed the Kalman fit
  Gaudi::TrackSymMatrix seedCov; // Set off-diagonal elements to zero
  seedCov(0,0) = m_errorX*m_errorX;
  seedCov(1,1) = m_errorY*m_errorY;
  seedCov(2,2) = m_errorTx*m_errorTx;
  seedCov(3,3) = m_errorTy*m_errorTy;
  seedCov(4,4) = std::pow(m_errorQoP[0] * track.firstState().qOverP(), 2) + std::pow(m_errorQoP[1],2);
  nodes.front().setSeedCovariance(seedCov);
  nodes.back().setSeedCovariance(seedCov);

  // Iterate the track fit for linearisation. Be careful with chi2
  // convergence here: The first iteration might not be using OT
  // drifttimes in which case the chi2 can actually go up in the 2nd
  // iteration.
  bool converged = false;
  LHCb::SebFitNode *firstOnTrackNode = nullptr;
  LHCb::SebFitNode *lastOnTrackNode = nullptr;
  for (int iter = 1; iter <= m_numFitIter && !converged; ++iter) {
    // update reference trajectories with smoothed states
    if (iter > 1) {
      try {
        // update the projections. need to be done every time ref is updated
        updateRefVectors(nodes, track.type(), firstOnTrackNode, lastOnTrackNode);
      } catch (const StatusCode &sc) {
        debug() << "problem updating ref vectors" << endmsg;
        throw sc;
      }
    }
    auto prevchi2 = track.chi2();
    try {
      std::tuple<double, int> chi2AndNDoF = fitIteration(nodes, nTrackParameters, seedCov, &firstOnTrackNode, &lastOnTrackNode);
      auto dchi2 = prevchi2 - std::get<0>(chi2AndNDoF);
      // require at least 3 iterations, because of the OT prefit.
      converged = ((iter>1) && std::abs(dchi2) < m_maxDeltaChi2Converged * std::get<1>(chi2AndNDoF));
      track.setChi2AndDoF(std::get<0>(chi2AndNDoF), std::get<1>(chi2AndNDoF));
    } catch (const std::string& errorMsg) {
      debug() << std::string("unable to fit the track") << errorMsg << endmsg;
      throw StatusCode::FAILURE;
    }
  }

  // Outlier removal iterations
  unsigned int nbOutliers = std::count_if(nodes.begin(), nodes.end(),
                                          [](const LHCb::SebFitNode& node) {
                                            return node.type() == LHCb::SebFitNode::Outlier;
                                          });
  while (nbOutliers < m_numOutlierIter && track.nDoF() > 1) {
    // try to remove an outlier, stop the loop if none left
    if (!tryAndRemoveOutlier(nodes, track.type(), firstOnTrackNode, lastOnTrackNode)) {
        break;
    }
    // Call the track fit
    try {
      std::tuple<double, int> chi2AndNDoF = fitIteration(nodes, nTrackParameters, seedCov, &firstOnTrackNode, &lastOnTrackNode);
      track.setChi2AndDoF(std::get<0>(chi2AndNDoF), std::get<1>(chi2AndNDoF));
    } catch (const std::string& errorMsg) {
      debug() << "unable to fit the track " << errorMsg << endmsg;
      throw StatusCode::FAILURE;
    }
    ++nbOutliers;
  }

  // determine the track states at user defined z positions
  try {
    auto states = determineStates(nodes, track.checkFlag(LHCb::Track::Backward), firstOnTrackNode, lastOnTrackNode);
    track.addToStates(states);
  } catch (const StatusCode& sc) {
    debug() << "failed in determining states" << endmsg;
    throw sc;
  }

  // fill extra info
  if (m_fillExtraInfo.value()) {
    fillExtraInfo(track, nodes, measurements, track.type());
  }

  track.setFitStatus(LHCb::Track::Fitted);
  return nbOutliers;
}

std::vector<LHCb::Measurement*>
HLT1Fitter::loadMeasurement(LHCb::span<const LHCb::State> states,
                            LHCb::span<const LHCb::LHCbID> lhcbIDs) const {
  std::vector<LHCb::LHCbID> newids{lhcbIDs.begin(), lhcbIDs.end()};

  // create a reference trajectory
  LHCb::TrackTraj reftraj(states);

  // create all measurements for selected IDs
  std::vector<LHCb::Measurement*> newmeasurements;
  newmeasurements.reserve(newids.size());
  m_measProvider->addToMeasurements(newids, newmeasurements, reftraj);

  // remove all zeros, just in case.
  auto newend = std::remove_if(newmeasurements.begin(),newmeasurements.end(),
                               [](const LHCb::Measurement* m) { return m==nullptr; });
  if (newend != newmeasurements.end()) {
    debug() << "Some measurement pointers are zero: " << int(newmeasurements.end() - newend) << endmsg;
    newmeasurements.erase(newend,newmeasurements.end());
    throw StatusCode::FAILURE;
  }
  return std::move(newmeasurements);
}

/*
 * given existing states, this function creates additional states at fixed
 * z-positions. If a state already exists sufficiently close to the
 * desired state, it will not create the new state.
 * @param states the initial existing states. Not the absence of constness,
 * as these states will have their QOverP modified
 * @return the additional states created
 */
std::vector<LHCb::State>
HLT1Fitter::createAdditionalRefStates(LHCb::span<LHCb::State> states,
                                      bool isBackward,
                                      LHCb::Track::Types trackType) const {
  // first need to make sure all states already on track have
  // reasonable momentum. still needs to check that this works for
  // velo-TT
  auto iter = std::find_if(states.cbegin(), states.cend(),
                           [](const LHCb::State& s) {
                             return s.checkLocation(LHCb::State::AtT);
                           });
  const LHCb::State& refstate = (iter != states.cend()) ? *iter :
    (isBackward ? *states.begin() : *states.rbegin());
  for (auto& state : states) {
    state.setQOverP(refstate.qOverP());
  }

  // collect the z-positions where we want the states
  boost::container::static_vector<double,4> zpositions;
  if (trackType == LHCb::Track::Ttrack ||
      trackType == LHCb::Track::Downstream ||
      trackType == LHCb::Track::Long) {
    zpositions.push_back(StateParameters::ZBegT);
    zpositions.push_back(StateParameters::ZEndT);
  }
  if (trackType == LHCb::Track::Downstream ||
      trackType == LHCb::Track::Upstream ||
      trackType == LHCb::Track::Long) {
    zpositions.push_back(StateParameters::ZEndTT);
  }
  if (trackType == LHCb::Track::Velo ||
      trackType == LHCb::Track::VeloR ||
      trackType == LHCb::Track::Upstream ||
      trackType == LHCb::Track::Long) {
    zpositions.push_back(StateParameters::ZEndVelo);
  }

  // the following container is going to hold pairs of 'desired'
  // z-positions and actual states. the reason for the gymnastics
  // is that we always want to propagate from the closest availlable
  // state, but then recursively. this will make the parabolic
  // approximation reasonably accurate.
  typedef std::pair<double, const LHCb::State*> ZPosWithState;
  std::vector<ZPosWithState> posStates;
  posStates.reserve(states.size());
  // we first add the states we already have
  std::transform(states.begin(), states.end(),
                 std::back_inserter(posStates),
                 [](const LHCb::State& s) {
                   return std::make_pair(s.z(),&s);
                 });

  // now add the other z-positions, provided nothing close exists
  const double maxDistance = 50*Gaudi::Units::cm;
  for (auto z : zpositions) {
    bool not_found = std::none_of(posStates.begin(), posStates.end(),
                                  [&](const ZPosWithState& s) {
                                    return std::abs(z - s.first) < maxDistance;
                                  });
    if (not_found) posStates.emplace_back(z, nullptr);
  }
  std::sort(posStates.begin(), posStates.end(),
            [](const ZPosWithState& s1, const ZPosWithState& s2){
              return s1.first < s2.first;
            });

  // create the states in between
  const ITrackExtrapolator* extrap = extrapolator(trackType);
  LHCb::Track::StateContainer newStates;
  newStates.reserve(posStates.size());
  for (auto it = posStates.begin(); it != posStates.end(); ++it) {
    if (it->second) continue;
    // find the nearest existing state to it
    auto best = posStates.end();
    for (auto jt = posStates.begin(); jt != posStates.end(); ++jt)
      if (it != jt && jt->second
          && (best==posStates.end() || std::abs(jt->first - it->first) < std::abs(best->first - it->first)))
        best = jt;
    assert(best != posStates.end());
    // move from that state to this iterator, using the extrapolator and filling all states in between.
    int direction = best > it ? -1 : +1;
    LHCb::StateVector statevec(best->second->stateVector(), best->second->z());
    for (auto jt = best+direction; jt != it+direction; jt += direction) {
      StatusCode thissc = extrap->propagate(statevec, jt->first, 0, LHCb::Tr::PID::Pion());
      if (!thissc.isSuccess()) {
        std::ostringstream msg;
        msg << "Problem propagating state: " << statevec << " to z= " << jt->first
            << "\ninitializeRefStates() fails in propagating state";
        throw msg.str();
      } else {
        newStates.emplace_back(statevec);
        jt->second = &newStates.back();
      }
    }
  }

  return std::move(newStates);
}

static inline double closestToBeamLine(const LHCb::State& state) {
  const Gaudi::TrackVector& vec = state.stateVector();
  auto z = state.z();
  // check on division by zero (track parallel to beam line!)
  if (vec[2] != 0 || vec[3] != 0) {
    z -= (vec[0]*vec[2] + vec[1]*vec[3]) / (vec[2]*vec[2] + vec[3]*vec[3]);
  }
  // don't go outside the sensible volume
  return std::min(std::max(z,-100*Gaudi::Units::cm), StateParameters::ZBegRich2) ;
}

namespace {
  /** small inline helper function helping in creating a list of nodes ordered
   *  by z from a list of measurements and a list of extra reference nodes to
   *  be added. The 2 lists are passed via iterators and their types are templated
   *  do that we can use it both for forward and backward tracks
   */
  template <class CompareOp, class MeasIterator, class RefIterator>
  inline void createNodesFromMeasurements(MeasIterator measBegin, MeasIterator measEnd,
                                          RefIterator refBegin, RefIterator refEnd,
                                          std::vector<LHCb::SebFitNode>& nodes) {
    auto refIt = refBegin;
    for (auto measIt = measBegin; measIt != measEnd; measIt++) {
      while (refIt != refEnd && CompareOp()(refIt->first, (*measIt)->z())) {
        nodes.emplace_back(refIt->first, refIt->second);
        refIt++;
      }
      nodes.emplace_back(**measIt);
    }
    for (; refIt != refEnd; refIt++) {
      nodes.emplace_back(refIt->first, refIt->second);
    }
  }
}

std::vector<LHCb::SebFitNode>
HLT1Fitter::makeNodes(LHCb::Track& track,
                      ranges::v3::any& accelCache,
                      std::vector<LHCb::Measurement*> &measurements) const {
  try {
    if (track.states().empty()) {
      throw Error("Track has no state! Can not fit.", StatusCode::FAILURE);
    }
    // make sure the track has sufficient reference states
    auto newStates = createAdditionalRefStates(track.states(),
                                               track.checkFlag(LHCb::Track::Backward),
                                               track.type());
    track.addToStates(newStates);
  } catch (const std::string &msg) {
    debug() << msg << endmsg;
    throw Warning("Problems setting reference info", StatusCode::FAILURE, 1);
  }

  // make measurements
  try {
    measurements = loadMeasurement(track.states(), track.lhcbIDs());
  } catch (const StatusCode &sc) {
    throw Error("Unable to load measurements!", StatusCode::FAILURE);
  }

  // check that there are sufficient measurements. in fact, one is
  // enough for the fit not to fail
  if (measurements.empty()) {
    throw Warning("No measurements on track", StatusCode::FAILURE, 0);
  }
  // order measurements in z
  bool isBackward = track.checkFlag(LHCb::Track::Backward);
  if (isBackward) {
    std::stable_sort(measurements.begin(), measurements.end(),
                     [](const LHCb::Measurement* m1, const LHCb::Measurement* m2) {return m1->z() > m2->z();});
  } else {
    std::stable_sort(measurements.begin(), measurements.end(),
                     [](const LHCb::Measurement* m1, const LHCb::Measurement* m2) {return m1->z() < m2->z();});
  }

  // Prepare a list of reference nodes to be added, ordered by z
  typedef std::pair<double, LHCb::State::Location> RefNodeData;
  std::vector<RefNodeData> extraRefNodes;
  extraRefNodes.reserve(6);
  if (m_addDefaultRefNodes.value()) {
    if (track.hasVelo() && !track.checkFlag(LHCb::Track::Backward))
      extraRefNodes.emplace_back(StateParameters::ZEndVelo, LHCb::State::EndVelo);
    if (track.hasTT()) {
      extraRefNodes.emplace_back(StateParameters::ZBegRich1, LHCb::State::BegRich1);
      extraRefNodes.emplace_back(StateParameters::ZEndRich1, LHCb::State::EndRich1);
    }
    if (track.hasT()) {
      extraRefNodes.emplace_back(StateParameters::ZBegT, LHCb::State::AtT);
      extraRefNodes.emplace_back(StateParameters::ZBegRich2, LHCb::State::BegRich2);
      extraRefNodes.emplace_back(StateParameters::ZEndRich2, LHCb::State::EndRich2);
    }
  }
  // Add a node for the position at the beamline, put it in the right place
  if (m_stateAtBeamLine.value() && (track.hasTT() || track.hasVelo())) {
    const LHCb::State& refstate = (track.checkFlag(LHCb::Track::Backward) ? track.states().back() : track.states().front());
    double closestToBeamLineZ = closestToBeamLine(refstate);
    auto it = extraRefNodes.begin();
    while (it->first < closestToBeamLineZ) it++;
    extraRefNodes.emplace(it, closestToBeamLineZ, LHCb::State::ClosestToBeam);
  }

  // We want to create the nodes in the right order (ordered by z), but the order depends
  // on the track, specifically whether it's a Forward or Backward track
  std::vector<LHCb::SebFitNode> nodes;
  nodes.reserve(measurements.size() + 6);
  if (!isBackward) {
    createNodesFromMeasurements<std::greater<double>>
      (measurements.rbegin(), measurements.rend(),
       extraRefNodes.rbegin(), extraRefNodes.rend(),
       nodes);
  } else {
    createNodesFromMeasurements<std::less<double>>
      (measurements.rbegin(), measurements.rend(),
       extraRefNodes.begin(), extraRefNodes.end(),
       nodes);
  }

  // Set the reference using a TrackTraj
  LHCb::TrackTraj tracktraj(track.states());
  std::for_each(nodes.begin(), nodes.end(),
                [&](LHCb::SebFitNode& node) { node.setRefVector(tracktraj.stateVector(node.z()));
  });

  // update the projections. need to be done every time ref is updated
  try {
    projectReference(nodes);
  } catch (const StatusCode &sc) {
    debug() << "problem updating ref vectors" << endmsg;
    throw sc;
  }

  // add all the noise, if required
  if (m_applyMaterialCorrections.value()) {
    updateMaterialCorrections(nodes, track.states(), track.type(), accelCache);
  }

  return std::move(nodes);
}

void HLT1Fitter::updateRefVectors(LHCb::span<LHCb::SebFitNode> nodes,
                                  LHCb::Track::Types trackType,
                                  LHCb::SebFitNode* firstOnTrackNode,
                                  LHCb::SebFitNode* lastOnTrackNode) const {
  // force the smoothing.
  bool hasinfoStreamForward = false;
  bool hasinfoStreamBackward = (lastOnTrackNode != nullptr);
  for(auto& node : nodes) {
    hasinfoStreamBackward = hasinfoStreamBackward && (&*nodes.begin() != lastOnTrackNode);
    node.computeBiSmoothedStateSeb(hasinfoStreamForward, hasinfoStreamBackward);
    hasinfoStreamForward = hasinfoStreamForward || (&node == firstOnTrackNode);
  }
  for (auto & node : nodes) {
    node.setRefVector(node.state().stateVector());
  }
  // update the projections. need to be done every time ref is
  // updated. we can move this code here at some point.
  projectReference(nodes);

  // update the transport using the new ref vectors
  updateTransport(nodes, trackType);
}

typedef Gaudi::Matrix1x3 DualVector;
DualVector dual(const Gaudi::XYZVector& v) {
  DualVector d;
  v.GetCoordinates( d.Array() );
  return d;
}

double HLT1Fitter::getToleranceForMeasurement(const LHCb::Measurement::Type& type) const {
  switch (type) {
  case LHCb::Measurement::VP:
    return 0.0005*Gaudi::Units::mm;
  case LHCb::Measurement::UT:
  case LHCb::Measurement::UTLite:
    return 0.002*Gaudi::Units::mm;
  case LHCb::Measurement::FT:
    return 0.002*Gaudi::Units::mm;
  case LHCb::Measurement::Muon:
    return 0.002*Gaudi::Units::mm;
  default:
    throw Warning("unknown measurement in projectReference", StatusCode::FAILURE, 0);
  }
}

HLT1Fitter::InternalProjectResult
HLT1Fitter::internal_project(const LHCb::StateVector& statevector,
                             const LHCb::Measurement& meas,
                             double tolerance) const {
  // Project onto the reference. First create the StateTraj without BField information.
  Gaudi::XYZVector bfield(0,0,0) ;
  const LHCb::StateZTraj<double> refTraj(statevector, bfield);

  // Get the measurement trajectory representing the centre of gravity
  const auto& measTraj = meas.trajectory();

  // Determine initial estimates of s1 and s2
  double sState = statevector.z(); // Assume state is already close to the minimum
  double sMeas = measTraj.muEstimate(refTraj.position(sState));

  // Determine the actual minimum with the Poca tool
  Gaudi::XYZVector dist;
  StatusCode sc = m_poca->minimize(refTraj, sState,
                                   measTraj, sMeas, dist, tolerance);
  if (sc.isFailure()) {
    throw sc;
  }

  // Set up the vector onto which we project everything. This should
  // actually be parallel to dist.
  Gaudi::XYZVector unitPocaVector = (measTraj.direction(sMeas).Cross(refTraj.direction(sState))).Unit();
  double doca = unitPocaVector.Dot(dist);

  // compute the projection matrix from parameter space onto the (signed!) unit
  Gaudi::TrackProjectionMatrix H = dual(unitPocaVector) * refTraj.derivative(sState);

  // Set the error on the measurement so that it can be used in the fit
  double errMeasure2 = meas.resolution2(refTraj.position(sState),
                                        refTraj.direction(sState));

  return {sMeas, doca, std::move(H), std::move(unitPocaVector), errMeasure2};
}

void HLT1Fitter::projectReference(LHCb::span<LHCb::SebFitNode> nodes) const {
  for (auto& node: nodes) {
    if (node.hasMeasurement()) {
      try {
        // if the reference is not set, issue an error
        auto result = internal_project(node.refVector(), node.measurement(),
                                       getToleranceForMeasurement(node.measurement().type()));
        node.updateProjection(std::move(result.H), -result.doca);
        node.setPocaVector(std::move(result.unitPocaVector));
        node.setDoca(result.doca);
        node.setErrMeasure2(result.errMeasure2);
      } catch (StatusCode scr) {
        throw Warning("unable to project statevector", scr, 0);
      }
    }
  }
}

namespace {
  enum HitType {VeloR, VeloPhi, TT, T, Muon, Unknown};
  inline int hittypemap(LHCb::Measurement::Type type) {
    HitType rc = Unknown;
    switch(type) {
    case LHCb::Measurement::Unknown:   rc = Unknown; break;
    case LHCb::Measurement::Muon: rc = Muon; break;
    case LHCb::Measurement::VP: rc = VeloR; break;
    case LHCb::Measurement::FT : rc = T; break;
    case LHCb::Measurement::UT : rc = TT; break;
    case LHCb::Measurement::UTLite : rc = TT; break;
    default : rc = Unknown; break;
    }
    return rc;
  }
}

bool HLT1Fitter::tryAndRemoveOutlier(LHCb::span<LHCb::SebFitNode> nodes,
                                     LHCb::Track::Types trackType,
                                     LHCb::SebFitNode* firstOnTrackNode,
                                     LHCb::SebFitNode* lastOnTrackNode) const {
  // return false if outlier chi2 cut < 0
  if (m_chi2Outliers < 0.0) return false;

  // Count the number of hits of each type
  const size_t minNumHits[5] = { m_minNumVeloRHits, m_minNumVeloPhiHits, m_minNumTTHits, m_minNumTHits, m_minNumMuonHits };
  size_t numHits[5]          = {0,0,0,0,0};
  for (const auto& node : nodes) {
    if (node.type() == LHCb::SebFitNode::HitOnTrack){
      ++numHits[hittypemap(node.measurement().type())];
    }
  }

  // loop over the nodes and find the one with the highest chi2 >
  // m_chi2Outliers, provided there is enough hits of this type left.

  // Computing the chi2 will trigger the smoothing. Especially in the
  // trigger, where we don't update the reference, this makes a big
  // difference. One way to save some time is to first test a number
  // of which we know that it is either equal to or bigger than the
  // chi2 contribution of the hit, namely the chi2 sum of the match
  // and the hit at this node. We then sort the hits in this number,
  // and only compute chi2 if it can be bigger than the current worst
  // one.
  const double totalchi2  = std::max(nodes.begin()->totalChi2(LHCb::SebFitNode::Backward).chi2(),
                                     nodes.rbegin()->totalChi2(LHCb::SebFitNode::Forward).chi2());
  using NodeWithChi2 = std::tuple<LHCb::SebFitNode*, double, bool, bool>;
  std::vector<NodeWithChi2> nodesWithChi2UL;
  nodesWithChi2UL.reserve(nodes.size());
  int numtried(0), numcalled(0);
  bool infoForward = false;
  bool infoBackward = (lastOnTrackNode != nullptr);
  LHCb::SebFitNode *prevNode = nullptr;
  for (auto it = nodes.begin(); it != nodes.end(); it++) {
    infoBackward = infoBackward && (!(&(*it) == lastOnTrackNode));
    if (it->hasMeasurement() &&
        it->type() == LHCb::SebFitNode::HitOnTrack) {
      int hittype = hittypemap(it->measurement().type());
      if (numHits[hittype] > minNumHits[hittype]) {
        ++numtried;
        auto chi2MatchAndHit = totalchi2;
        if (prevNode) {chi2MatchAndHit -= prevNode->totalChi2(0).chi2();
        }
        auto nextIt = it + 1;
        if (nextIt != nodes.end()) {
          chi2MatchAndHit -= nextIt->totalChi2(1).chi2();
        }
        if (chi2MatchAndHit > m_chi2Outliers) {
          nodesWithChi2UL.emplace_back(&(*it), chi2MatchAndHit, infoForward, infoBackward);
        }
      }
    }
    prevNode = &(*it);
    infoForward = infoForward || (prevNode == firstOnTrackNode);
  }
  // now sort them
  auto worstChi2 = m_chi2Outliers;
  std::sort(nodesWithChi2UL.begin(), nodesWithChi2UL.end(),
            [](const NodeWithChi2 &n1, const NodeWithChi2 &n2){
              return std::get<1>(n1) > std::get<1>(n2);
            });

  // -- if the first is smaller than worstChi2, and it is sorted in decreasing order
  // -- the 'if' will never be true and we can return here (M. De Cian)
  if (!nodesWithChi2UL.empty() && std::get<1>(nodesWithChi2UL.front()) < worstChi2) {
    return false;
  }
  LHCb::SebFitNode* outlier{nullptr};
  for (auto& node : nodesWithChi2UL) {
    if (std::get<1>(node) > worstChi2) {
      std::get<0>(node)->computeBiSmoothedStateSeb(std::get<2>(node), std::get<3>(node));
      const auto chi2 = std::get<0>(node)->chi2Seb();
      ++numcalled;
      if (chi2 > worstChi2) {
        worstChi2 = chi2;
        outlier = std::get<0>(node);
      }
    }
  }

  if (outlier) {
    // update reference trajectories with smoothed states
    if (m_updateReferenceInOutlierIters.value()) {
      try {
        updateRefVectors(nodes, trackType, firstOnTrackNode, lastOnTrackNode);
      } catch (const StatusCode &sc) {
        debug() << "problem updating ref vectors" << endmsg;
        throw sc;
      }
    }

    // deactivate the outlier and return
    outlier->setType(LHCb::SebFitNode::Outlier);
    return true;
  }
  return false;
}

std::vector<LHCb::State> HLT1Fitter::determineStates(LHCb::span<const LHCb::SebFitNode> nodes,
                                                     bool isBackward,
                                                     LHCb::SebFitNode* firstOnTrackNode,
                                                     LHCb::SebFitNode* lastOnTrackNode) const {
  // Add the state at the first and last measurement position
  if (firstOnTrackNode == nullptr) {
    debug() << "HLT1Fitter::determineStates : unable to find first measurement" << endmsg;
    throw StatusCode::FAILURE;
  }
  // force the smoothing
  firstOnTrackNode->computeBiSmoothedStateSeb(false, lastOnTrackNode != nullptr);
  if (lastOnTrackNode == nullptr) {
    debug() << "HLT1Fitter::determineStates : unable to find last measurement" << endmsg;
    throw StatusCode::FAILURE;
  }
  // force the smoothing
  lastOnTrackNode->computeBiSmoothedStateSeb(firstOnTrackNode != nullptr, false);
  const LHCb::State& lastState = lastOnTrackNode->state();

  bool upstream = nodes.begin()->z() > nodes.rbegin()->z();
  bool reversed = (upstream && !isBackward) || (!upstream && isBackward);

  // This state is not filtered for a forward only fit.
  LHCb::Track::StateContainer states;
  if (m_addDefaultRefNodes.value()) {
    states.emplace_back(firstOnTrackNode->state());
    states.back().setLocation(reversed ? LHCb::State::LastMeasurement : LHCb::State::FirstMeasurement);
  }
  // This state is always filtered
  states.emplace_back(lastState);
  states.back().setLocation(reversed ? LHCb::State::FirstMeasurement : LHCb::State::LastMeasurement);

  // Add the states at the reference positions
  for (const auto& node : nodes)
    if (node.type() == LHCb::SebFitNode::Reference)
      states.emplace_back(node.state());

  return std::move(states);
}

void HLT1Fitter::updateMaterialCorrections(LHCb::span<LHCb::SebFitNode> nodes,
                                           LHCb::span<const LHCb::State> states,
                                           LHCb::Track::Types trackType,
                                           ranges::v3::any& accelCache) const {
  if (nodes.size() < 1) return;

  // the noise in each node is the noise in the propagation between
  // the previous node and this node.
  // first collect all volumes on the track. The advantages of collecting them all at once
  // is that it is much faster. (Less call to TransportSvc.)

  // if m_scatteringP is set, use it
  auto scatteringMomentum = m_scatteringP.value();
  if (m_scatteringP.value() <= 0) {
    // if only velo, or magnet off, use a fixed momentum based on pt.
    scatteringMomentum = states[0].p();
    if (m_scatteringPt.value() > 0 &&
        // exclude Long, Upstream, DownStream, Ttrack, so check !hasT && !hasTT
        (trackType < LHCb::Track::Long || trackType > LHCb::Track::Ttrack)) {
      auto tx     = states[0].tx();
      auto ty     = states[0].ty();
      auto slope2 = tx*tx+ty*ty;
      auto tanth  = std::max(std::sqrt(slope2/(1+slope2)),1e-4);
      scatteringMomentum = m_scatteringPt/tanth;
    }
    // set some limits for the momentum used for scattering
    scatteringMomentum = std::min(scatteringMomentum, m_maxMomentumForScattering.value());
    scatteringMomentum = std::max(scatteringMomentum, m_minMomentumForScattering.value());
  }

  // this is fairly tricky now: we want to use TracjTraj, but we
  // cannot create it directly from the nodes, because that would
  // trigger the filter!
  LHCb::TrackTraj tracktraj(states);
  auto zmin = nodes.begin()->z();
  auto zmax = nodes.rbegin()->z();
  if (zmin>zmax) std::swap(zmin,zmax);
  tracktraj.setRange(zmin,zmax);

  // make sure we have the space we need in intersections so we don't need to
  // reallocate (offline, I've seen tracks with more than 670 intersections
  // in 100 events; we stay a bit above that to be on the safe side - and we
  // don't mind the occasional reallocate if it's a rare track that has even
  // more intersections)
  IMaterialLocator::Intersections intersections;
  intersections.reserve(1024);
  m_materialLocator->intersect_r(tracktraj, intersections, accelCache);

  // now we need to redistribute the result between the nodes. the first node cannot have any noise.
  auto node = nodes.begin();
  auto zorigin = node->z();
  for (++node; node != nodes.end(); ++node) {
    auto ztarget = node->z();
    LHCb::State state(node->refVector());
    state.covariance() = Gaudi::TrackSymMatrix();
    state.setQOverP(1/scatteringMomentum);
    // only apply energyloss correction for tracks that traverse magnet
    bool applyenergyloss = m_applyEnergyLossCorrections.value() &&
      (trackType == LHCb::Track::Downstream || trackType == LHCb::Track::Long);
    m_materialLocator->applyMaterialCorrections(state, intersections, zorigin,
                                                LHCb::Tr::PID::Pion(),
                                                true, applyenergyloss);
    auto deltaE = 1/state.qOverP() - scatteringMomentum;
    node->setNoiseMatrix(state.covariance());
    node->setDeltaEnergy(deltaE);
    zorigin = ztarget;
  }
}

int HLT1Fitter::updateTransport(LHCb::span<LHCb::SebFitNode> nodes,
                                LHCb::Track::Types trackType) const {
  bool hasMomentum = false;
  // sets the propagation between the previous node and this. node that the reference
  // of the previous node is used.
  if (nodes.size()>1) {
    const ITrackExtrapolator* extrap = extrapolator(trackType);
    auto node = nodes.begin();
    const LHCb::StateVector* refvector = &(node->refVector());
    Gaudi::TrackMatrix F =  ROOT::Math::SMatrixIdentity();
    for (++node; node!=nodes.end(); ++node) {
      LHCb::StateVector statevector = *refvector;
      StatusCode sc = extrap->propagate(statevector, node->z(), &F);
      if (sc.isFailure()) {
        debug() << "unable to propagate reference vector from z=" << refvector->z()
                << " to " << node->z()
                << "; track type = " << trackType
                << ": vec = " << refvector->parameters() << endmsg;
        throw sc;
      }

      // correct for energy loss
      auto dE = node->deltaEnergy();
      if (std::abs(statevector.qOverP()) > LHCb::Math::lowTolerance) {
        auto charge = statevector.qOverP() > 0 ? 1. :  -1.;
        auto momnew = std::max(m_minMomentumForELossCorr.value(),
                               std::abs(1/statevector.qOverP()) + dE);
        if (std::abs(momnew) > m_minMomentumForELossCorr.value())
          statevector.setQOverP(charge/momnew);
      }

      // calculate the 'transport vector' (need to replace that)
      Gaudi::TrackVector tranportvec = statevector.parameters() - F * refvector->parameters();
      node->setTransportMatrix(F);
      node->setTransportVector(tranportvec);

      // update the reference
      refvector = &(node->refVector());

      // test dtx/dqop to see if the momentum affects this track.
      if (std::abs(F(2,4)) != 0) hasMomentum = true;
    }
  }

  if (m_useSeedStateErrors.value()) {
    // we need to do this until we can properly deal with the seed state
    return 0;
  } else {
    return hasMomentum ? 5 : 4;
  }
}

std::tuple<double, int> HLT1Fitter::fitIteration(LHCb::span<LHCb::SebFitNode> nodes,
                                                 int& nTrackParameters,
                                                 const Gaudi::TrackSymMatrix& seedCov,
                                                 LHCb::SebFitNode** prevFirstOnTrackNode,
                                                 LHCb::SebFitNode** prevLastOnTrackNode) const {
  // This is set up with the aim to trigger the cache such that there
  // will be no nested calls. That makes it easier to profile the
  // fit. Eventually, we could do without all of this, since
  // everything is anyway computed on demand.
  LHCb::SebFitNode *prevNode = nullptr;
  LHCb::SebFitNode *firstOnTrackNode = nullptr;
  bool hasInfoUpstream = false;
  for (auto& node : nodes) {
    node.computePredictedStateSeb(LHCb::SebFitNode::Forward, prevNode, hasInfoUpstream);
    node.computeFilteredStateSeb(LHCb::SebFitNode::Forward, prevNode, nTrackParameters);
    prevNode = &node;
    if (!hasInfoUpstream) {
      hasInfoUpstream = (node.type() == LHCb::SebFitNode::HitOnTrack);
      if (hasInfoUpstream) {
        firstOnTrackNode = &node;
      }
    }
  }
  LHCb::SebFitNode *lastOnTrackNode = nullptr;
  prevNode = nullptr;
  hasInfoUpstream = false;
  for (auto& node : boost::adaptors::reverse(nodes)) {
    node.computePredictedStateSeb(LHCb::SebFitNode::Backward, prevNode, hasInfoUpstream);
    node.computeFilteredStateSeb(LHCb::SebFitNode::Backward, prevNode, nTrackParameters);
    prevNode = &(node);
    if (!hasInfoUpstream) {
      hasInfoUpstream = (node.type() == LHCb::SebFitNode::HitOnTrack);
      if (hasInfoUpstream) {
        lastOnTrackNode = &node;
      }
    }
  }
  *prevFirstOnTrackNode = firstOnTrackNode;
  *prevLastOnTrackNode = lastOnTrackNode;
  // force the smoothing.
  /*bool hasinfoStreamForward = false;
  bool hasinfoStreamBackward = (lastOnTrackNode != nullptr);
  for(auto& node : nodes) {
    hasinfoStreamBackward = hasinfoStreamBackward && (&nodes.front() != lastOnTrackNode);
    node.computeBiSmoothedStateSeb(hasinfoStreamForward, hasinfoStreamBackward);
    hasinfoStreamForward = hasinfoStreamForward || (&node == firstOnTrackNode);
    }*/

  // set the total chisquare of the track
  // Count the number of active track parameters. For now, just look at the momentum.
  size_t npar = m_DoF;
  if (npar == 5u) {
    const LHCb::State* laststate(nullptr);
    for (auto inode = nodes.begin(); inode != nodes.end(); ++inode) {
      if (inode->type() == LHCb::SebFitNode::HitOnTrack) {
        laststate = &(inode->filteredState(LHCb::SebFitNode::Forward));
      }
    }
    if (laststate) {
      const double threshold = 0.1;
      nTrackParameters = (laststate->covariance()(4,4) / seedCov(4,4) < threshold ? 5 : 4);
    }
  }

  LHCb::ChiSquare chisq = nodes.rbegin()->totalChi2(LHCb::SebFitNode::Forward);
  int ndof = chisq.nDoF() - (npar - nTrackParameters);
  // FIXME: why don't we take the maximum, like in KalmanFitResult?
  LHCb::ChiSquare chisqbw = nodes.begin()->totalChi2(LHCb::SebFitNode::Backward);
  if (chisqbw.chi2() < chisq.chi2()) chisq = chisqbw;
  return std::make_tuple(chisq.chi2(), ndof);
}
