#pragma once

// Include files
#include "GaudiAlg/Transformer.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/IMeasurementProvider.h"
#include "TrackInterfaces/IMaterialLocator.h"
#include "Kernel/ITrajPoca.h"
#include "Kernel/STLExtensions.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Event/Measurement.h"
#include "Event/StateVector.h"
#include "Event/SebFitNode.h"
#include "Event/Track.h"

/**
 * Class dedicated to fitting HLT1 tracks
 * @author Sebastien Ponce
 */
class HLT1Fitter : public Gaudi::Functional::Transformer<std::vector<LHCb::Track>(const std::vector<LHCb::Track>&)>{

public:

  /// Standard constructor
  HLT1Fitter( const std::string& name, ISvcLocator* pSvcLocator );

  /// main execution
  std::vector<LHCb::Track> operator()(const std::vector<LHCb::Track>&) const override;

  /// finalization (pure reporting)
  StatusCode finalize() override;

private:

  struct InternalProjectResult final {
    double sMeas;
    double doca;
    Gaudi::TrackProjectionMatrix H;
    Gaudi::XYZVector unitPocaVector;
    double errMeasure2;
  };

  /**
   * fits on track, returns number of outliers found
   */
  unsigned int fit(LHCb::Track& track, ranges::v3::any& accelCache) const;

  // helper function that should be removed once MeasurementProvider is refactored
  std::vector<LHCb::Measurement*> loadMeasurement(LHCb::span<const LHCb::State> states,
                                                  LHCb::span<const LHCb::LHCbID>  lhcbIDs) const;

  /// node creation
  std::vector<LHCb::SebFitNode> makeNodes(LHCb::Track& track,
                                       ranges::v3::any& accelCache,
                                       std::vector<LHCb::Measurement*> &measurements) const;

  void updateRefVectors(LHCb::span<LHCb::SebFitNode> nodes,
                        LHCb::Track::Types trackType,
                        LHCb::SebFitNode* firstOnTrackNode,
                        LHCb::SebFitNode* lastOnTrackNode) const;

  double getToleranceForMeasurement(const LHCb::Measurement::Type& type) const;

  InternalProjectResult internal_project(const LHCb::StateVector& statevector,
                                         const LHCb::Measurement& meas,
                                         double tolerance) const;

  void projectReference(LHCb::span<LHCb::SebFitNode> nodes) const;

  /*
   * Tries to find and remove an outlier node
   * If one was found and remove returns true, else returns false
   */
  bool tryAndRemoveOutlier(LHCb::span<LHCb::SebFitNode> nodes,
                           LHCb::Track::Types trackType,
                           LHCb::SebFitNode* firstOnTrackNode,
                           LHCb::SebFitNode* lastOnTrackNode) const;

  /// Build states from node and measurements
  std::vector<LHCb::State> determineStates(LHCb::span<const LHCb::SebFitNode> nodes,
                                           bool isBackward,
                                           LHCb::SebFitNode* firstOnTrackNode,
                                           LHCb::SebFitNode* lastOnTrackNode) const;

  /*
   * given existing states, this function creates additional states at fixed
   * z-positions. If a state already exists sufficiently close to the
   * desired state, it will not create the new state.
   * @param states the initial existing states. Not the absence of constness,
   * as these states will have their QOverP modified
   * @return the additional states created
   */
  std::vector<LHCb::State> createAdditionalRefStates(LHCb::span<LHCb::State> states,
                                                     bool isBackward,
                                                     LHCb::Track::Types trackType) const;

  /**
   * Performs one iteration of the Kalmanfit
   * @returns chi2 and nDoF of the track after the fit
   * Also sets nTrackParameters, firstOnTrackNode and lastOnTrackNode
   */
  std::tuple<double, int> fitIteration(LHCb::span<LHCb::SebFitNode> nodes,
                                       int& nTrackParameters,
                                       const Gaudi::TrackSymMatrix& seedCov,
                                       LHCb::SebFitNode** firstOnTrackNode,
                                       LHCb::SebFitNode** lastOnTrackNode) const;

  int updateTransport(LHCb::span<LHCb::SebFitNode> nodes,
                      LHCb::Track::Types trackType) const;

  void updateMaterialCorrections(LHCb::span<LHCb::SebFitNode> nodes,
                                 LHCb::span<const LHCb::State> states,
                                 LHCb::Track::Types trackType,
                                 ranges::v3::any& accelCache) const;

  const ITrackExtrapolator* extrapolator(LHCb::Track::Types tracktype) const {
    if (tracktype == LHCb::Track::Velo || tracktype == LHCb::Track::VeloR) return &(*m_veloExtrapolator);
    return &(*m_extrapolator);
  }

private:

  ToolHandle<ITrackExtrapolator> m_extrapolator{"TrackMasterExtrapolator", this};     ///< extrapolator
  ToolHandle<ITrackExtrapolator> m_veloExtrapolator{"TrackLinearExtrapolator",this}; ///< extrapolator for Velo-only tracks
  ToolHandle<IMeasurementProvider> m_measProvider{"MeasurementProvider", this };
  ToolHandle<IMaterialLocator> m_materialLocator{"DetailedMaterialLocator",this};
  ToolHandle<ITrajPoca> m_poca{"TrajPoca",this};

  Gaudi::Property<bool> m_forceSmooth {this, "ForceSmooth", false, "Flag for force the smoothing (for debug reason)"};
  Gaudi::Property<unsigned int> m_DoF {this, "DoF", 5u, "Degrees of freedom"};
  Gaudi::Property<double> m_maxChi2DoF{this, "MaxChi2DoF", 999999, "Max chi2 per track when output is a new container"};
  Gaudi::Property<double> m_maxDeltaChi2Converged{this, "MaxDeltaChiSqConverged", 0.01, "Maximum change in chisquare for converged fit"};
  Gaudi::Property<unsigned int> m_numOutlierIter{this, "MaxNumberOutliers", 2, "max number of outliers to be removed"};
  Gaudi::Property<bool> m_fillExtraInfo{this, "FillExtraInfo", true, "Fill the extra info"};
  Gaudi::Property<bool> m_addDefaultRefNodes{this, "AddDefaultReferenceNodes", true, "add default reference nodes"};
  Gaudi::Property<bool> m_stateAtBeamLine{this, "StateAtBeamLine", true, "add state closest to the beam-line"};
  Gaudi::Property<bool> m_applyMaterialCorrections{this, "ApplyMaterialCorrections", true, "Apply material corrections"};
  Gaudi::Property<bool> m_applyEnergyLossCorrections{this, "ApplyEnergyLossCorr", true, "Apply energy loss corrections"};
  Gaudi::Property<double> m_chi2Outliers{this, "Chi2Outliers", 9.0, "chi2 of outliers to be removed"};
  Gaudi::Property<bool> m_useSeedStateErrors{this, "UseSeedStateErrors", false, "use errors of the seed state"};
  Gaudi::Property<double> m_minMomentumForScattering{this, "MinMomentumForScattering", 100.*Gaudi::Units::MeV, "Minimum momentum used for scattering"};
  Gaudi::Property<double> m_maxMomentumForScattering{this, "MaxMomentumForScattering", 500.*Gaudi::Units::GeV, "Maximum momentum used for scattering"};
  Gaudi::Property<double> m_errorX{this, "ErrorX", 20.0*Gaudi::Units::mm, "Seed error on x"};
  Gaudi::Property<double> m_errorY{this, "ErrorY", 20.0*Gaudi::Units::mm, "Seed error on y"};
  Gaudi::Property<double> m_errorTx{this, "ErrorTx", 0.1, "Seed error on slope x"};
  Gaudi::Property<double> m_errorTy{this, "ErrorTy", 0.1, "Seed error on slope y"};
  Gaudi::Property<std::vector<double>> m_errorQoP{this, "ErrorQoP", {0.0, 0.01}, "Seed error on QoP"};
  Gaudi::Property<int> m_numFitIter{this, "NumberFitIterations", 10, "number of fit iterations to perform"};
  Gaudi::Property<size_t> m_minNumVeloRHits{this, "MinNumVeloRHitsForOutlierRemoval", 3, "Minimum number of VeloR hits"};
  Gaudi::Property<size_t> m_minNumVeloPhiHits{this, "MinNumVeloPhiHitsForOutlierRemoval", 3, "Minimum number of VeloPhi hits"};
  Gaudi::Property<size_t> m_minNumTTHits{this, "MinNumTTHitsForOutlierRemoval", 3, "Minimum number of TT hits"};
  Gaudi::Property<size_t> m_minNumTHits{this, "MinNumTHitsForOutlierRemoval", 6, "Minimum number of T hits"};
  Gaudi::Property<size_t> m_minNumMuonHits{this, "MinNumMuonHitsForOutlierRemoval", 4, "Minimum number of Muon hits"};
  Gaudi::Property<double> m_minMomentumForELossCorr{this, "MinMomentumELossCorr", 10.*Gaudi::Units::MeV, "Minimum momentum used in correction for energy loss"};
  Gaudi::Property<double> m_scatteringPt{this, "TransverseMomentumForScattering", 400.*Gaudi::Units::MeV, "transverse momentum used for scattering if track has no good momentum estimate"};
  Gaudi::Property<double> m_scatteringP{this, "MomentumForScattering", -1, "momentum used for scattering in e.g. magnet off data"};
  Gaudi::Property<bool> m_updateReferenceInOutlierIters{this, "UpdateReferenceInOutlierIterations", true, "Update projection in iterations in which outliers are removed"};

};
