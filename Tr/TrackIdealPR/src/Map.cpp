#include "Map.h"

const TrackIdealPR::CatMap& TrackIdealPR::catToType()
{
   static const TrackIdealPR::CatMap s_map = { { IMCReconstructible::ChargedVelo,LHCb::Track::Types::Velo },
                                               { IMCReconstructible::ChargedLong,LHCb::Track::Types::Long },
                                               { IMCReconstructible::ChargedUpstream,LHCb::Track::Types::Upstream },
                                               { IMCReconstructible::ChargedDownstream,LHCb::Track::Types::Downstream },
                                               { IMCReconstructible::ChargedTtrack,LHCb::Track::Types::Ttrack } };
   return s_map;
}
