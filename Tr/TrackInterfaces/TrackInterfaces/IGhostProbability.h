#ifndef _IGhostProbability_H
#define _IGhostProbability_H

#include "GaudiKernel/IAlgTool.h"


/** @class IGhostProbability
 *
 *  interface for the ghost probability calculation
 *
 *  @author P.Seyfert
 *  @date   27/01/2015
 */

namespace LHCb{
 class Track;
}

struct IGhostProbability: extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IGhostProbability, 1, 0 );

  /** Add the reference information */
  virtual StatusCode execute(LHCb::Track& aTrack) const = 0;

  /** consider this the beginning of a new event */
  virtual StatusCode beginEvent() = 0;

  /** reveal the variable names for a track type */
  virtual std::vector<std::string> variableNames(LHCb::Track::Types type) const = 0;

  /** reveal the variable values for a track */
  virtual std::vector<float> netInputs(LHCb::Track& aTrack) const = 0;
};

#endif
