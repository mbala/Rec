#ifndef TRACKINTERFACES_ISTATECORRECTIONTOOL_H
#define TRACKINTERFACES_ISTATECORRECTIONTOOL_H 1

// Include files
// -------------
// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "range/v3/utility/any.hpp"

// Forward declarations
class Material;
namespace LHCb {
  class State;
}


/** @class IStateCorrectionTool IStateCorrectionTool.h TrackInterfaces/IStateCorrectionTool.h
 *
 *  Interface for state correction tools
 *
 *  @author Eduardo Rodrigues
 *  @date   2006-08-18
 */
struct IStateCorrectionTool : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( IStateCorrectionTool, 2, 0 );
  /// Correct a State
  virtual void correctState( LHCb::State& state, const Material* material, ranges::v3::any& cache,
                             double wallThickness = 0, bool upstream = true, double mass = 0 ) const = 0;

  virtual ranges::v3::any createBuffer() const { return ranges::v3::any(); }
};
#endif // TRACKINTERFACES_ISTATECORRECTIONTOOL_H
