#ifndef TRACKKERNEL_ZTRAJECTORY_H
#define TRACKKERNEL_ZTRAJECTORY_H 1

// Include files
#include <vector>
#include "Kernel/VectorConfiguration.h"
#include "Kernel/Trajectory.h"
#include "Event/StateVector.h"
#include "Event/State.h"

/** @class ZTrajectory ZTrajectory.h TrackKernel/ZTrajectory.h
 *  Interface for trajectories parameterized along Z. Gives access to statevectors.
 *
 *  @author Wouter HULSBERGEN
 *  @date   2007-11-29
 */

namespace LHCb
{
  // TODO : Would be nice to be able to template the floating point type
  //        for State and StateVector
  template <typename FTYPE>
  struct StateVectorT {
    using Vector5 = typename ROOT::Math::SVector<FTYPE, 5>;

    Vector5 parameters;
    FTYPE z;
  };

  // Get number of elements inside FTYPE (1 if scalar, N for VecNd of VCL)
  template<typename FTYPE>
  struct nbelmt { static constexpr auto value = VectorConfiguration::width<double>(); };

  template<> struct nbelmt<double> { static constexpr size_t value = 1; };
  template<> struct nbelmt<float> { static constexpr size_t value = 1; };

  template<typename FTYPE = double>
  class ZTrajectory : public Trajectory<FTYPE>
  {
  public:
    static constexpr auto NBELMT = nbelmt<FTYPE>::value;

    using Trajectory<FTYPE>::Trajectory;
    using Vector = typename Trajectory<FTYPE>::Vector;
    using Point  = typename Trajectory<FTYPE>::Point;
    using Range  = typename Trajectory<FTYPE>::Range;
    using StateVector = typename std::conditional<std::is_floating_point<FTYPE>::value,
                                                  LHCb::StateVector,
                                                  LHCb::StateVectorT<FTYPE>>::type;
    using AdaptState = typename std::conditional<NBELMT == 1,
                                                 LHCb::State,
                                                 std::array<State, NBELMT>>::type;


    Gaudi::TrackVector params(typename LHCb::StateVectorT<FTYPE>::Vector5& p,
                              size_t idx) const {
      return { p[0][idx], p[1][idx], p[2][idx], p[3][idx], p[4][idx] };
    }

    template <size_t... Is>
    constexpr AdaptState toStates ( StateVector sv, std::index_sequence<Is...> ) const
    {
      static_assert(sizeof...(Is)>0);
      if constexpr ( sizeof...(Is) > 1 ) {
        return { State(LHCb::StateVector(params(sv.parameters, Is), sv.z[Is]))... };
      } else if constexpr (sizeof...(Is) == 1 ) {
        return { State(sv) };
      }
    }

    /// Default constructor
    ZTrajectory() : Trajectory<FTYPE>(0., 0.) {}
    /// Constructor taking the values of mu that defined the valid range of the trajectory
    ZTrajectory( FTYPE begin, FTYPE end ): Trajectory<FTYPE>(begin, end) {}
    /// Constructor taking a range
    ZTrajectory( const Range& range ): Trajectory<FTYPE>(range) {}
    /// return stateVector at position mu
    virtual StateVector stateVector( FTYPE mu ) const = 0 ;
    /// return a state at position mu
    virtual AdaptState state( FTYPE mu ) const {
      return toStates(stateVector(mu), std::make_index_sequence<NBELMT>());
    }
    /// return the set of reference statevectors for this parameterization (if any)
    virtual std::vector<StateVector> refStateVectors() const { return { }; }
  };

}

#endif // EVENT_ZTRAJECTORY_H
