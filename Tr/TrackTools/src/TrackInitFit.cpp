// Include files

// from Gaudi
#include "TrackInitFit.h"

// from TrackEvent
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/State.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TrackInitFit
//
// 2009-11-14 : Kostyantyn Holubyev
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackInitFit )

//=============================================================================
// Initialization
//=============================================================================
StatusCode TrackInitFit::initialize() {

  info() << "==> Initialize " << endmsg;

  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  m_initTrack = tool<ITrackStateInit>( m_initToolName, "Init", this);
  m_fitTrack  = tool<ITrackFitter>(m_fitToolName, "Fit", this);

  return StatusCode::SUCCESS;
}

//=============================================================================
