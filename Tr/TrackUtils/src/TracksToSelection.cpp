/** @class TracksToSelection TracksToSelection.h
 *
 *  Convert LHCb::Tracks to LHCb::Track::Selection
 */

#include "GaudiAlg/Transformer.h"
#include <string>
#include "Event/Track.h"

struct TracksToSelection final
     : Gaudi::Functional::Transformer<LHCb::Track::Selection(const LHCb::Tracks&)>
{
  TracksToSelection(const std::string& name, ISvcLocator* pSvcLocator)
  : Transformer( name, pSvcLocator,
                   { "InputLocation", {} },
                   { "OutputLocation", {} } )
  { }

  LHCb::Track::Selection operator()(const LHCb::Tracks& tracks) const override
  {
    LHCb::Track::Selection out;
    for (const auto& track : tracks ) {
        out.insert( track );
    }
    return out;
  }
};

DECLARE_COMPONENT( TracksToSelection )
