################################################################################
# Package: TrackVectorFit
################################################################################
gaudi_subdir(TrackVectorFit)

gaudi_depends_on_subdirs(GaudiKernel
                         Kernel/VectorClass
                         Kernel/LHCbKernel
                         Tr/TrackInterfaces)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(TrackVectorFit src/TrackVectorFit.cpp
                  PUBLIC_HEADERS TrackVectorFit
                  LINK_LIBRARIES TrackFitEvent LHCbKernel)

if(LCG_COMP STREQUAL gcc )
  set_property(SOURCE src/TrackVectorFit.cpp APPEND_STRING PROPERTY COMPILE_FLAGS " -fabi-version=0 " )
endif()
