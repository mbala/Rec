#pragma once

#include "Types.h"

namespace Tr {

namespace TrackVectorFit {

/**
 * @brief Container for reference nodes
 * @details Reference Nodes are sequentially processed nodes
 *          in TrackVectorFitter. As such, they require specific
 *          containers stored in AOS and reserved at creation time.
 *          
 *          Inasmuch as possible, the Node container is used, which
 *          is also AOS.
 */
struct ReferenceNode : public FitNode {
  Gaudi::TrackMatrix m_forwardTransportMatrix;
  Gaudi::TrackMatrix m_backwardTransportMatrix;
  Gaudi::TrackVector m_transportVector;
  Gaudi::TrackSymMatrix m_noiseMatrix;
  TRACKVECTORFIT_PRECISION m_deltaEnergy = ((TRACKVECTORFIT_PRECISION) 0.0);

  ReferenceNode() = default;
  ReferenceNode(const ReferenceNode& copy) = default;
  ReferenceNode(const double& z, const LHCb::State::Location& location = LHCb::State::Location::LocationUnknown)
    : FitNode(z, location) {}

  void setNoiseMatrix (const Gaudi::TrackSymMatrix& noiseMatrix) {
    m_noiseMatrix = noiseMatrix;
  }

  void setDeltaEnergy (const TRACKVECTORFIT_PRECISION& e) {
    m_deltaEnergy = e;
  }

  void setTransportMatrix (const Gaudi::TrackMatrix& tm) {
    m_forwardTransportMatrix = tm;
  }

  void calculateAndSetInverseTransportMatrix (const Gaudi::TrackMatrix& tm) {
    Scalar::Math::invertMatrix(tm, m_backwardTransportMatrix);
  }

  void setTransportVector (const Gaudi::TrackVector& tv) {
    m_transportVector = tv;
  }

  TRACKVECTORFIT_PRECISION deltaEnergy () {
    return m_deltaEnergy;
  }

  ///////////////////////
  // Reduced fit logic //
  ///////////////////////

  template<class T>
  void initialize();

  template<class T>
  void predict(const FitNode& previous_node);

  template<bool F, bool B, class U = typename std::conditional<F == true and B == true, bool, void>::type>
  U smoother();

  /**
   * @brief Update residuals: In a reference node, this is 0.0
   */
  void updateResiduals() {
    setResidual(0.0);
    setErrResidual(0.0);
  }
};

/**
 * @brief Forward initialize
 */
template<>
void ReferenceNode::initialize<Op::Forward>() {
  m_forwardLHCbState = LHCb::State {
    refVector().parameters(),
    ArrayGen::InitialCovariance_SymMatrix(),
    z(),
    state().location()
  };
}

/**
 * @brief Backward initialize
 */
template<>
void ReferenceNode::initialize<Op::Backward>() {
  m_backwardLHCbState = LHCb::State {
    refVector().parameters(),
    ArrayGen::InitialCovariance_SymMatrix(),
    z(),
    state().location()
  };
}

/**
 * @brief Forward predict
 */
template<>
void ReferenceNode::predict<Op::Forward>(
  const FitNode& previous_node
) {
  Gaudi::TrackVector state_vec = m_forwardTransportMatrix * previous_node.forwardState().stateVector() + m_transportVector;
  Gaudi::TrackSymMatrix state_cov;

  Scalar::Math::transportCovariance(
    m_forwardTransportMatrix,
    previous_node.forwardState().covariance(),
    state_cov
  );

  state_cov += m_noiseMatrix;

  m_forwardLHCbState = LHCb::State {
    state_vec,
    state_cov,
    z(),
    state().location()
  };
}

/**
 * @brief Backward predict
 */
template<>
void ReferenceNode::predict<Op::Backward>(
  const FitNode& previous_node
) {
  Gaudi::TrackSymMatrix state_cov;
  Gaudi::TrackSymMatrix temp_cov = previous_node.backwardState().covariance() + m_noiseMatrix;
  Gaudi::TrackVector state_vec = m_backwardTransportMatrix * (previous_node.backwardState().stateVector() - m_transportVector);

  Scalar::Math::transportCovariance(
    m_backwardTransportMatrix,
    temp_cov,
    state_cov
  );

  m_backwardLHCbState = LHCb::State {
    state_vec,
    state_cov,
    z(),
    state().location()
  };
}

/**
 * @brief Smoother with no previous signal downstream
 */
template<>
void ReferenceNode::smoother<false, true>() {
  m_state = m_backwardLHCbState;
}

/**
 * @brief Smoother with no previous signal upstream
 */
template<>
void ReferenceNode::smoother<true, false>() {
  m_state = m_forwardLHCbState;
}

/**
 * @brief Smoother with signal on both directions
 */
template<>
bool ReferenceNode::smoother<true, true>() {
  m_state.setZ(z());
  return LHCb::Math::Average(
    m_forwardLHCbState.stateVector(),
    m_forwardLHCbState.covariance(),
    m_backwardLHCbState.stateVector(),
    m_backwardLHCbState.covariance(),
    m_state.stateVector(),
    m_state.covariance()
  );
}

}

}
