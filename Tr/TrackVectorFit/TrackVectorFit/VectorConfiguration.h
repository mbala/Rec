#pragma once

#include "Kernel/VectorConfiguration.h"

// Algorithm-dependent precision configuration
#ifdef TRACKVECTORFIT_SINGLE_PRECISION
#define TRACKVECTORFIT_PRECISION float
#else
#define TRACKVECTORFIT_PRECISION double
#endif

namespace Tr {
namespace TrackVectorFit {

/**
 * @brief      Returns the defined vector width
 */
constexpr std::size_t vector_width () {
  return VectorConfiguration::width<TRACKVECTORFIT_PRECISION>();
}

/**
 * @brief      Cast for values into TRACKVECTORFIT_PRECISION
 *             mostly to avoid warnings with certain libraries
 *             
 *             It should be resolved in compile time to the proper
 *             type and not produce any code
 */
template<class T>
static constexpr TRACKVECTORFIT_PRECISION cast (const T& value) {
  return ((TRACKVECTORFIT_PRECISION) value);
}

template<size_t width>
using Vectype = LHCb::Vector::Vectype<TRACKVECTORFIT_PRECISION, width>;

}
}

// Alignment definitions
#define ALIGNMENT sizeof(TRACKVECTORFIT_PRECISION) * Tr::TrackVectorFit::vector_width()
#define _aligned alignas(ALIGNMENT)
#ifdef __INTEL_COMPILER
#define _type_aligned __attribute__((align_value(ALIGNMENT)))
#else
#define _type_aligned __attribute__((aligned(ALIGNMENT)))
#endif

typedef _type_aligned TRACKVECTORFIT_PRECISION * const __restrict__ fp_ptr_64;
typedef _type_aligned const TRACKVECTORFIT_PRECISION * const __restrict__ fp_ptr_64_const;
